package com.bwj.aage;

import junit.framework.Assert;
import org.junit.Test;

public class TestMessageManager {


    @Test
    public void testMessageManager() throws Exception {
        MessageManager.addMessage("Message 1", 2);
        MessageManager.addMessage("Message 2", 1);
        MessageManager.addMessage("Message 3", 10, 10);

        Assert.assertEquals(3, MessageManager.getMessages(9, 9, 3).size());
        Assert.assertEquals(3, MessageManager.getMessages(10, 9, 1).size());
        Assert.assertEquals(2, MessageManager.getMessages(10, 8, 1).size());
        MessageManager.clearMessages();
        Assert.assertEquals(1, MessageManager.getMessages(10, 10, 3).size());
    }
}
