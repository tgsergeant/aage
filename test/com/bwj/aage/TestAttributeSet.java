package com.bwj.aage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestAttributeSet {

    private AttributeSet set1;
    private AttributeSet set2;


    @Before
    public void setUp() throws Exception {
        set1 = new AttributeSet(30);
        set2 = new AttributeSet(new int[]{20, 25, 30, 35, 40, 45});
    }

    @Test
    public void testConstructor() throws Exception {
        Assert.assertNotNull(set1);
        Assert.assertNotNull(set2);

    }

    @Test
    public void testGetAttributeValue() throws Exception {
        for (int i = 0; i < 6; i++) {
            Assert.assertEquals(30, set1.getAttributeValue(i));
        }
        Assert.assertEquals(20, set2.getAttributeValue(0));
        Assert.assertEquals(45, set2.getAttributeValue(5));
    }

    @Test
    public void testMergeWith() throws Exception {
        set1.mergeWith(set2);
        Assert.assertEquals(50, set1.getAttribute(0).getValue());
        Assert.assertEquals(75, set1.getAttribute(5).getValue());
        Assert.assertEquals(60, set1.getAttribute(2).getValue());
    }

    @Test
    public void testGetAttributeIndex() throws Exception {
        Assert.assertEquals(1, AttributeSet.getAttributeIndex("Charisma"));
        Assert.assertEquals(5, AttributeSet.getAttributeIndex("strength"));
        Assert.assertEquals(-1, AttributeSet.getAttributeIndex("blah"));
    }
}
