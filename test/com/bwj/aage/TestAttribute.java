package com.bwj.aage;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestAttribute {

    private Attribute agility;
    private Attribute charisma;
    private Attribute strength;
    private Attribute maxed;

    @Before
    public void setUp() {
        agility = new Attribute(0, "Agility");
        charisma = new Attribute(30, "Charisma");
        strength = new Attribute(36, "Strength");
        maxed = new Attribute(100, "Maxed");
    }

    @Test
    public void testConstructor() {
        assertNotNull(agility);
        assertNotNull(charisma);
        assertNotNull(strength);
    }

    @Test
    public void testGetModifier() {
        assertEquals(-6, agility.getModifier());
        assertEquals(0, charisma.getModifier());
        assertEquals(1, strength.getModifier());
        assertEquals(14, maxed.getModifier());

    }
}
