package com.bwj.aage;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestPoint {

    Point origin;
    Point east;
    Point north;
    Point southwest;

    @Before
    public void setUp() throws Exception {
        origin = new Point();
        east = new Point(10, 0);
        north = new Point(0, 10);
        southwest = new Point(-10, -9);
    }

    @Test
    public void testConstructor() throws Exception {
        Assert.assertNotNull(origin);
        Assert.assertNotNull(east);
    }

    @Test
    public void testGetRelativeDirection() throws Exception {
        Assert.assertEquals(Direction.EAST, origin.getRelativeDirection(east));
        Assert.assertEquals(Direction.NORTH, origin.getRelativeDirection(north));
        Assert.assertEquals(Direction.SOUTHWEST, origin.getRelativeDirection(southwest));

        Assert.assertEquals(Direction.WEST, east.getRelativeDirection(origin));
        Assert.assertEquals(Direction.SOUTH, north.getRelativeDirection(origin));
        Assert.assertEquals(Direction.NORTHEAST, southwest.getRelativeDirection(origin));
    }
}
