package com.bwj.aage;

import net.slashie.libjcsi.CSIColor;

import java.io.Serializable;

/**
 * Represents anything that is drawn to the screen as part of the map. All objects and
 * entities reference a Tile, defining how they should be drawn.
 * @author Tim
 *
 */
public class Tile implements Serializable {

    public static Tile GroundTile = new Tile('.', CSIColor.DARK_GRAY,  false, "Dirt Floor");

    public static Tile WallTile = new Tile('#', CSIColor.GRAY, true, "Wall");

    public static Tile BlankTile = new Tile(' ', CSIColor.BLACK, true, "Nothing");

    public static Tile GrassTile = new Tile('.', CSIColor.GREEN, false, "Grass");

	public static Tile HillTile = new Tile('~', CSIColor.BROWN, false, "Hills");

    public static Tile OceanTile = new Tile('~', CSIColor.BLUE, true, false, "Deep ocean");

	public static Tile ShallowOcean = new Tile('~', new CSIColor(0x118cff), true, false, "Shallow ocean");

	public static Tile ForestTile = new Tile('&', CSIColor.FOREST_GREEN, false, "Forest");

    public static Tile MountainTile = new Tile('^', CSIColor.GRAY, true, "Impassable mountains");
    
    public static Tile SandTile = new Tile('.', CSIColor.CHARTREUSE, false, "Sand");

	public static Tile WoodenWallTile = new Tile('#', new CSIColor(0x964B00), true, "Wooden wall");
	
	public static Tile IndoorFloorTile = new Tile('.', CSIColor.DARK_BROWN, false, "Packed earth floor");

	public static Tile TreeTile = new Tile('T', CSIColor.GREEN, false, "A Tree");

	public static Tile RoadTile = new Tile('.', CSIColor.SLATE_GRAY, false, "Dirt road");

    private char character;

    private CSIColor color;
	


	public CSIColor getVisitedColor() {
        return Tile.darkenColor(color);
    }

    private boolean blocksMovement = false;
	
	private boolean blocksSight = false;

	private String description;
    /**
     * Constructs a new tile. Assumes that visibility and passability are the same
     * @param c The character to be drawn for this tile
     * @param color The color of the tile
     * @param blocks True if the tile blocks movement
     */
    public Tile(char c, CSIColor color, boolean blocks, String description) {
        character = c;
        this.color = color;
        blocksMovement = blocks;
		blocksSight = blocks;
		this.description = description;
    }

	public Tile(char c, CSIColor color, boolean blocks) {
		this(c, color, blocks, null);
	}

	/**
	 * Creates a new tile, with different values for visibility and passability.
	 * @param c The character to be drawn for this tile
	 * @param color The color of the tile
	 * @param movement True if the tile blocks movement
	 * @param sight True if the tile blocks sight
	 */
	public Tile(char c, CSIColor color, boolean movement, boolean sight, String description) {
		character = c;
		this.color = color;
		blocksMovement = movement;
		blocksSight = sight;
		this.description = description;
	}

    public Tile(char c, CSIColor color) {
        character = c;
        this.color = color;
    }

    public void setCharacter(char character) {
        this.character = character;
    }

    public char getCharacter() {
        return character;
    }

    public void setColor(CSIColor color) {
        this.color = color;
    }

    public CSIColor getColor() {
        return color;
    }


    public void setBlocksMovement(boolean blocksMovement) {
        this.blocksMovement = blocksMovement;
    }

    public boolean blocksMovement() {
        return blocksMovement;
    }

	public boolean blocksSight() {
		return blocksSight;
	}
    /**
     * Darkens the given colour
     * @param old The colour to darken
     * @return A darker version of the given colour
     */
    public static CSIColor darkenColor(CSIColor old) {
        double darkfactor = 0.45;
        int newA = (int) (old.getA() * darkfactor);
        int newR = (int) (old.getR() * darkfactor);
        int newG = (int) (old.getG() * darkfactor);
        int newB = (int) (old.getB() * darkfactor);
        return new CSIColor(newR, newG, newB, newA);
    }

	public String getDescription() {
		return description;
	}
}
