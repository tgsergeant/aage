package com.bwj.aage;

/**
 * Singleton class to list the experience values required to advance a level.
 * @author Tim
 *
 */
public class ExperienceTable {
    
    private static ExperienceTable table = null;
    
    /** Experiences values required for each level */
    private int[] experienceValues;
    public static final int MAXLEVEL = 100;
    
    /**
     * Creates a new Experience table using the formula I devised.
     */
    protected ExperienceTable() {
        experienceValues = new int[MAXLEVEL + 1];
        experienceValues[0] = 0;
        for(int i = 1; i <= MAXLEVEL; i++) {
            experienceValues[i] = experienceValues[i-1] + (int)(Math.pow(1.23, (i + 50)/2));
        }
    }
    
    /**
     * Returns the experience required for a given level.
     * @param level Level to check
     * @return The total experience required for this level
     */
    public static int getExperience(int level) {
        if(table == null) {
            table = new ExperienceTable();
        }
        return table.experienceValues[level - 1];
    }

}
