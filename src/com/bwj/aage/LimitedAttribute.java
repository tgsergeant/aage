package com.bwj.aage;

/**
 * Represents an attribute which can never go above its default value.
 */
public class LimitedAttribute extends Attribute {

    public LimitedAttribute(int value, String name) {
        super(value, name);
    }

    @Override
    public void modifyValue(int toAdd) {
        super.modifyValue(toAdd);
        limitValue();
    }

    private void limitValue() {
        if (getValue() > getDefaultValue()) {
            setValue(getDefaultValue());
        }
    }

    @Override
    public void setValue(int value) {
        super.setValue(value);
        limitValue();
    }
}
