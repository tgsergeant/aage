package com.bwj.aage;

import java.io.Serializable;

/**
 * An attribute which describes a characteristic of an entity, for example,
 * intelligence or strength. These are used in a variety of ways throughout
 * the game.
 */
public class Attribute implements Serializable {

	/** Current value of the attribute */
	private int value;

	/** Default (unmodified) value of the attribute */
	private int defaultValue;

	/** Name of the attribute */
	private String name;

	// Array indexes for attributes
	public static final int AGILITY = 0,
							CHARISMA = 1,
							ENDURANCE = 2,
							INTELLIGENCE = 3,
							PERCEPTION = 4,
							STRENGTH = 5;

	public Attribute(int value, String name) {
		this.value = value;
		this.defaultValue = value;
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(int defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getName() {
		return name;
	}
	
	public void modifyValue(int toAdd) {
		value += toAdd;
	}
	
	public void modifyDefault(int toAdd) {
		defaultValue += toAdd;
		value += toAdd;
	}

	/**
	 * A modifier is a 'normalised' value for an attribute between -6 and +14
	 * which describes how that value relates to an average score.
	 * @return The modifier for this ability
	 */
	public int getModifier() {
		return (value - 30) / 5;
	}

    @Override
    public String toString() {
        return value + "/" + defaultValue;
    }

	public String toStringColor() {
		String attString = "";
		if(value < defaultValue * 0.2) {
			attString += "`1";
		} else if(value > defaultValue) {
			attString += "`8";
		}
		attString += value + "`0/" + defaultValue;

		return attString;
	}

    public String toTableString() {
        //Carefully position the strings with whitespace
        String attString = Util.addWhitespace(getName(), 17)
                + Util.addWhitespace(toStringColor(), 15);
        int modifier = getModifier();
		if(modifier > 5) {
			attString += "`8";
		} else if (modifier <= -2) {
			attString += "`1";
		}
        attString += (modifier > 0 ? "+" : "") + modifier + "`0";
        return attString;
    }
    
    public static String getAttributeTableHeader() {
        return "    Attribute    Current Value   Modifier";
    }
}
