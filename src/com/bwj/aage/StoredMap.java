package com.bwj.aage;

import java.io.Serializable;


/**
 * Used to store the data for a map in memory while the player is on another map.
 * @author Tim
 *
 */
public class StoredMap implements Serializable {
    
    private static final long serialVersionUID = 6340939741856459947L;

    private Map map;
    
    private VisibilityMap visibility;
    
    private int x, y;
    
    /**
     * Generates a new map for storage
     * @param m The map to be stored
     * @param vis The player's Visibilitymap for this map
     * @param x The x-coordinate of the player
     * @param y The y-coordinate of the player.
     */
    public StoredMap(Map m, VisibilityMap vis, int x, int y) {
	map = m;
	visibility = vis;
	this.x = x;
	this.y = y;
    }

    public Map getMap() {
        return map;
    }

    public VisibilityMap getVisibility() {
        return visibility;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public void setVisibility(VisibilityMap visibility) {
        this.visibility = visibility;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

}
