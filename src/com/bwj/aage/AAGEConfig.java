package com.bwj.aage;

import java.io.*;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * A singleton class which holds the config for the game. Loads its data from the "config.config" class.
 * @author Tim
 *
 */
// TODO Make an options screen to allow ingame configuration
public class AAGEConfig {
    
    private static Properties prop = null;
    
    /** Used to prevent rogue instances */
    protected AAGEConfig() {
        
    }

    /**
     * @return The width of the game window
     */
    public static int getWidth() {
		return getIntegerProperty("xDim");
	}

    /**
     * @return The height of the game window
     */
    public static int getHeight() {
		return getIntegerProperty("yDim");
	}
    
    /**
     * @return The properties object which has been loaded.
     */
    public static Properties getProperties() {
        if(prop == null) {
            loadConfig();
        }
        return prop;
    }

    /**
     * Loads the config.config file and initialises the properties.
     */
    private static void loadConfig() {
        File f = new File("config.xml");
        if(!f.exists()) {
            makeDefaultPropertiesFile();
        }
        prop = new Properties();
        try {
            FileInputStream fis = new FileInputStream(f);
            prop = new Properties();
            prop.loadFromXML(fis);
        } catch (FileNotFoundException e) {
            Util.reportError(e);
        } catch (InvalidPropertiesFormatException e) {
            Util.reportError(e);
        } catch (IOException e) {
            Util.reportError(e);
        }
	}
    
    /**
     * Makes a default properties file.
     */
    private static void makeDefaultPropertiesFile() {
        Properties prop = new Properties();
        prop.put("fontSize", "12");
        prop.put("xDim", "100");
        prop.put("yDim", "30");
		prop.put("verbose", "false");
        try {
            FileOutputStream fos = new FileOutputStream(new File("config.xml"));
            prop.storeToXML(fos, "Default settings file for AAGE");
            fos.close();
        } catch (FileNotFoundException e) {
            Util.reportError(e);
        } catch (IOException e) {
            Util.reportError(e);
        }
    }

	public static String getStringProperty(String key) {
		return getProperties().getProperty(key);
	}

	public static int getIntegerProperty(String key) {
		return Integer.parseInt(getProperties().getProperty(key));
	}

	public static void toggleBooleanProperty(String key) {
		boolean current = Boolean.parseBoolean(getProperties().getProperty(key));
		getProperties().setProperty(key, String.valueOf(!current));
	}

	/**
	 * Saves the properties to the config.xml file
	 */
	public static void saveProperties() {
		try {
			FileOutputStream fos = new FileOutputStream(new File("config.xml"));
			prop.storeToXML(fos, "AAGE Settings");
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
