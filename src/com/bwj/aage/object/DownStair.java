package com.bwj.aage.object;

import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import net.slashie.libjcsi.CSIColor;

public class DownStair extends Stair {

	private long toSeed;

	public long getToSeed() {
		return toSeed;
	}

	public void setToSeed(long toSeed) {
		this.toSeed = toSeed;
	}

	public DownStair(int i, int j, int toSeed) {
		super(i, j, new Tile('>', CSIColor.GRAY, true));
		this.toSeed = toSeed;
	}

	public void transport(Player player) {
		// TODO Check to see if the cave has been generated before. If not, generate it
		player.requestMapChange(toSeed);
	}

	@Override
	public String getDescription(boolean showFull, Player p) {
		return "A staircase leading down";
	}
}
