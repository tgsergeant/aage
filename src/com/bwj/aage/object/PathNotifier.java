package com.bwj.aage.object;

import com.bwj.aage.MapObject;
import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import net.slashie.libjcsi.CSIColor;

public class PathNotifier extends MapObject {

    /**
     * 
     */
    private static final long serialVersionUID = 3683449174487315142L;

    public PathNotifier(int x, int y) {
        super(x, y, new Tile('x', CSIColor.GRAY, true));
        
    }

	@Override
	public String getDescription(boolean showFull, Player p) {
		return null;
	}
}
