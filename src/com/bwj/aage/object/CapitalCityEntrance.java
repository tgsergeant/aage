package com.bwj.aage.object;

import com.bwj.aage.Map;
import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import com.bwj.aage.map.CapitalCityMap;
import com.bwj.aage.map.Kingdom;
import net.slashie.libjcsi.CSIColor;

import java.util.Random;

/**
 * An entrance to the capital city of a kingdom.
 */
public class CapitalCityEntrance extends SettlementEntrance {

	Kingdom kingdom;
	
	public CapitalCityEntrance(Kingdom kingdom, long parentSeed, Random random) {
		super(kingdom.getCapitalPosition().getX(), kingdom.getCapitalPosition().getY(), new Tile('O', CSIColor.CRIMSON), parentSeed, random);
		this.kingdom = kingdom;
	}

	@Override
	protected Map createMap(long seed, long parentSeed) {
		return new CapitalCityMap(seed, parentSeed);
	}

	@Override
	public String getDescription(boolean showFull, Player p) {
		return getName() + ", the capital city of " + kingdom.getKingdomDescription();
	}
}
