package com.bwj.aage.object;

import com.bwj.aage.Map;
import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import com.bwj.aage.map.TownMap;
import net.slashie.libjcsi.CSIColor;

import java.util.Random;

/**
 * An entrance to a town map.
 */
public class TownEntrance extends SettlementEntrance {

	public TownEntrance(int x, int y, long parentSeed, Random random) {
		super(x, y, new Tile('O', CSIColor.RED_VIOLET), parentSeed, random);
	}

	@Override
	protected Map createMap(long seed, long parentSeed) {
		return new TownMap(seed, parentSeed);
	}

	@Override
	public String getDescription(boolean showFull, Player p) {
		return "An entrance to " + getName();
	}
}
