package com.bwj.aage.object;

import com.bwj.aage.Map;
import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import com.bwj.aage.util.MarkovNameGenerator;

import java.util.Random;

/**
 * Generic entrance to a settlement on the map - such as a town or village.
 * Implementors need to provide their own map creation method.
 */
public abstract class SettlementEntrance extends Stair {
	private long seed;
	private long parentSeed;
	private String name;

	public SettlementEntrance(int x, int y, Tile t, long parentSeed, Random random) {
		super(x, y, t);
		this.parentSeed = parentSeed;
		this.seed = random.nextLong();
		name = MarkovNameGenerator.placeGen.getName(random);
	}

	@Override
	public void transport(Player player) {
		if (!player.hasMap(seed)) {
			Map genMap = createMap(seed, parentSeed);
			
			genMap.generate(null);
			genMap.setName(name);
			player.storeMap(genMap);
		}
		player.requestMapChange(seed);
	}

	/**
	 * Creates the map which this settlement transports the player to. Implementation is left
	 * up to the subclass. generate() should not be called on the map.
	 * @param seed Seed of the map to generate
	 * @param parentSeed Seed of the world map. When the player steps off the edge of the
	 *                   settlement, they will be returned to this map.
	 * @return The map that this entrance should redirect to.
	 */
	protected abstract Map createMap(long seed, long parentSeed);

	public String getName() {
		return name;
	}
}
