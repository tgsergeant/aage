package com.bwj.aage.object;

import com.bwj.aage.MapType;
import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import com.bwj.aage.map.CaveNameGenerator;
import net.slashie.libjcsi.CSIColor;

import java.util.Random;

public class DungeonEntrance extends UndergroundEntrance {

    /**
     * 
     */
    private static final long serialVersionUID = -8670980747569024969L;

    public DungeonEntrance(int x, int y, Random r, long parent) {
		super(x, y, new Tile('*', CSIColor.LIGHT_GRAY, true), MapType.DUNGEON, parent, r);

        int depth = (int) (r.nextGaussian() * 2 + 5);
		if(depth < 1)
			depth = 1;
        seeds = new long[depth];
		name = CaveNameGenerator.getName(r);
		difficulty = (int) r.nextGaussian();

        for(int i = 0; i < depth; i++) {
            seeds[i] = r.nextLong();
        }
    }

	@Override
	public String getDescription(boolean showFull, Player p) {
		return "A staircase leading into " + name;
	}
}
