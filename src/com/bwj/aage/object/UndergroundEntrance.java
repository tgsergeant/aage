package com.bwj.aage.object;

import com.bwj.aage.*;
import com.bwj.aage.config.ConfigManager;
import com.bwj.aage.config.Race;
import com.bwj.aage.map.UndergroundMap;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 */
public abstract class UndergroundEntrance extends Stair {

	long parentSeed;
	protected long[] seeds;
	String name;
	int difficulty = 1;
	
	MapType type;

	boolean raceDungeon = false;
	Race race = null;

	public UndergroundEntrance(int x, int y, Tile t, MapType type, long parentSeed, Random r) {
		super(x, y, t);
		this.parentSeed = parentSeed;
		this.type = type;
		if (r.nextInt(10) == 1) {
			//This is a race dungeon! Select a race to use
			raceDungeon = true;
			ArrayList<Race> raceList = ConfigManager.getRaceByTag("racedungeon");
			race = raceList.get(Util.rand(0, raceList.size() - 1));
		}
	}

	/**
	 * Transports the player to the cave system, generating the caves if they have not been visited
	 * before. The difficulty of the cave system is dependent on the level of the player when it is
	 * first visited.
	 * @param player  The current player
	 */
	public void transport(Player player) {
	//Check to see if the cave has been generated before. If not, generate it.
		if(!player.hasMap(seeds[0])) {
			for(int i = 0; i < seeds.length; i++) {
				UndergroundMap map = (UndergroundMap) Map.create(type, seeds[i], player);

				if(i == 0) {
					map.setParent(parentSeed);
				}
				else {
					map.setParent(seeds[i - 1]);
				}
				if(i == seeds.length - 1) {
					map.removeDownStair();
				} else {
					map.setChild(seeds[i + 1]);
				}
				map.setName(name);
				map.setDepth(i + 1);
				int caveDifficulty = player.getLevel() + difficulty;

				map.setDifficulty((caveDifficulty > 0) ? caveDifficulty : 1);

				if(raceDungeon) {
					map.setRaceFilter(race);
				}
				map.populateMap();
				player.storeMap(map);
			}
			if (difficulty >= 2) {
				MessageManager.addMessage("You feel a deep sense of forboding as you enter the cave.");
			}
			MessageManager.addMessage("You discovered the " + name + "!");
			player.addExperience(20);
		}
		player.requestMapChange(seeds[0]);
	}
}
