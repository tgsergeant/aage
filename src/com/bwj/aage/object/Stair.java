package com.bwj.aage.object;

import com.bwj.aage.MapObject;
import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import net.slashie.libjcsi.CharKey;

/**
 *
 */
public abstract class Stair extends MapObject {

	protected Stair(int x, int y, Tile t) {
		super(x, y, t);
	}

	public abstract void transport(Player player);

	@Override
	public boolean canInteract(int keyCode, Player player) {
		return keyCode == CharKey.LESSTHAN || keyCode == CharKey.MORETHAN;
	}

	@Override
	public void interact(int keyCode, Player player) {
		transport(player);
	}
}
