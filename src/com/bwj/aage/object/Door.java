package com.bwj.aage.object;

import com.bwj.aage.MapObject;
import com.bwj.aage.MessageManager;
import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import net.slashie.libjcsi.CSIColor;
import net.slashie.libjcsi.CharKey;

/**
 * Controls a door and how it is displayed on the map. References two tiles,
 * representing an open and closed door, and keeps track of which one should be
 * displayed.
 * @author Tim
 *
 */
//TODO: Locked doors, kicking down doors - A Strength DC check?
public class Door extends MapObject {

	public static final Tile OPENTILE = new Tile('\\', CSIColor.RUST, false);
	public static final Tile CLOSEDTILE = new Tile('+', CSIColor.RUST, true);

	private boolean closed = true;

	public Door(int i, int j, boolean closed) {
		super(i, j);
		setClosed(closed);
	}

	public void setClosed(boolean close) {
		closed = close;
		tile = closed ? CLOSEDTILE : OPENTILE;
	}

	public void toggleClosed() {
		setClosed(!closed);
	}

	@Override
	public boolean blocksMovement() {
		return closed;
	}

	@Override
	public boolean blocksSight() {
		return closed;
	}

	@Override
	public boolean canInteract(int keyCode, Player player) {
		switch (keyCode) {
			case CharKey.o:
			case -1:
				return closed;
			case CharKey.c:
				return !closed;
		}
		return false;
	}

	@Override
	public void interact(int keyCode, Player player) {
		switch (keyCode) {
			case CharKey.o:
			case -1:
				setClosed(false);
				MessageManager.addMessage("You open the door.");
				break;
			case CharKey.c :
				if (player.getMap().isTileBlocked(x, y)) {
					MessageManager.addMessage("That door is blocked.");
				} else {
					setClosed(true);
					MessageManager.addMessage("You close the door");
				}
				break;
		}
	}

	@Override
	public String getDescription(boolean showFull, Player p) {
		return "A " + (closed ? "closed" : "open") + " door";
	}
}
