package com.bwj.aage.object;

import com.bwj.aage.Map;
import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import com.bwj.aage.map.VillageMap;
import net.slashie.libjcsi.CSIColor;

import java.util.Random;

/**
 * An entrance to a village map
 */
public class VillageEntrance extends SettlementEntrance {

	public VillageEntrance(int x, int y, Random random, long parentSeed) {
		super(x, y, new Tile('O', CSIColor.ORANGE), parentSeed, random);

	}

	@Override
	public String getDescription(boolean showFull, Player p) {
		return "An entrance to the village of " + getName();
	}

	@Override
	protected Map createMap(long seed, long parentSeed) {
		return new VillageMap(seed, parentSeed);
	}
}
