package com.bwj.aage.object;

import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import net.slashie.libjcsi.CSIColor;

public class UpStair extends Stair {

	private long toSeed;

	public long getToSeed() {
		return toSeed;
	}

	public void setToSeed(long toSeed) {
		this.toSeed = toSeed;
	}

	public UpStair(int i, int j, int toSeed) {
		super(i, j, new Tile('<', CSIColor.GRAY, true));
		this.toSeed = toSeed;
	}

	public void transport(Player player) {
		player.requestMapChange(toSeed);
		// TODO Maps could have multiple entrances. Maybe each stair should specify an exact position to go to
	}

	@Override
	public String getDescription(boolean showFull, Player p) {
		return "A staircase leading up";
	}
}
