package com.bwj.aage.object;

import com.bwj.aage.MapType;
import com.bwj.aage.Player;
import com.bwj.aage.Tile;
import com.bwj.aage.map.CaveNameGenerator;
import net.slashie.libjcsi.CSIColor;

import java.util.Random;


public class CaveEntrance extends UndergroundEntrance {
    
    //int caveSeed;

	/**
     * Creates a new cave entrance at the given position
     * @param x The x position of the entrance
     * @param y y position of the entrance
     * @param r RNG used to create the cave system
     * @param parentSeed Seed of the world map which is parent to this cave
     */
    public CaveEntrance(int x, int y, Random r, long parentSeed) {
        super(x,y,new Tile('O', CSIColor.GRAY, true), MapType.CAVE, parentSeed, r);

		int depth = (int) (r.nextGaussian() + 5);
		if(depth < 1)
			depth = 1;
        seeds = new long[depth];
		name = CaveNameGenerator.getName(r);
		difficulty = (int) r.nextGaussian();

        for(int i = 0; i < depth; i++) {
            seeds[i] = r.nextLong();
        }
	}

	@Override
	public String getDescription(boolean showFull, Player p) {
		return "A rough entrance to " + name;
	}
}
