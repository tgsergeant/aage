package com.bwj.aage.map;

import java.util.Random;

public class CaveNameGenerator {
    
    public static String getName(Random r) {
        String word1, word2, word3;
        int index1 = r.nextInt(list1.length);
        word1 = list1[index1];
        int index2 = r.nextInt(list2.length);
        word2 = list2[index2];
        int index3 = r.nextInt(list3.length);
        word3 = list3[index3];
        return word1 + " " + word2 + " of " + word3;
    }
    
    private static String[] list1 = {
             "Accursed",
             "Ancient",
             "Baneful",
             "Black",
             "Bloodstained",
             "Cold",
             "Dark",
             "Devouring",
             "Diabolical",
             "Ebon",
             "Eldritch",
             "Forbidden",
             "Forgotten",
             "Haunted",
             "Hidden",
             "Lonely",
             "Lost",
             "Malevolent",
             "Misplaced",
             "Nameless",
             "Scarlet",
             "Secret",
             "Shrouded",
             "Squamous",
             "Strange",
             "Tenebrous",
             "Uncanny",
             "Unspeakable",
             "Corrupt",
             "Vanishing",
             "Weird",
    };
    private static String[] list2  = {
     "Abyss",
     "Catacombs",
     "Caverns",
     "Cyst",
     "Depths",
     "Dungeons",
     "Haunts",
     "Labyrinth",
     "Maze",
     "Milieu",
     "Mines",
     "Pits",
     "Ruins",
     "Sanctum",
     "Shambles",
     "Temple",
     "Tower",
     "Vault"
    };
    
    
    private static String[] list3  = {
         "Blood",
         "Bones",
         "Chaos",
         "Curses",
         "the Dead",
         "Death",
         "Demons",
         "Despair",
         "Deviltry",
         "Doom",
         "Evil",
         "Fire",
         "Frost",
         "Gloom",
         "Hells",
         "Horrors",
         "Ichor",
         "Iron",
         "Madness",
         "Mists",
         "Monsters",
         "Mystery",
         "Necromancy",
         "Oblivion",
         "Peril",
         "Phantasms",
         "Secrets",
         "Shadows",
         "Sigils",
         "Skulls",
         "Slaughter",
         "Sorcery",
         "Terror",
         "Torment",
         "Treasure",
         "the Underworld",
         "the Unknown",
    };
    
}
