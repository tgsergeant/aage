package com.bwj.aage.map;

import com.bwj.aage.Direction;
import com.bwj.aage.Point;
import com.bwj.aage.Tile;
import com.bwj.aage.Util;
import com.bwj.aage.object.Door;
import com.bwj.aage.object.DownStair;
import com.bwj.aage.object.UpStair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


public class DungeonMap extends UndergroundMap {
    
    private static final int MINROOMSIZE = 4;
    private static final int MAXROOMSIZE = 11;
    private static final int MINDENSITY = 25;
    private static final int MAXDENSITY = 50;
    private static final int CORRIDORCHANCE = 25;
    private static final int MAKEDOORCHANCE = 10;

    private ArrayList<Room> rooms = new ArrayList<Room>();


    public DungeonMap(long seed) {
        super(seed, 80, 21);
    }


    @Override
    public void generate(HashMap<String, Integer> param) {
		System.out.println("---");

        //Decide on a final density
        Random r = new Random(getSeed());
        int density = r.nextInt(MAXDENSITY - MINDENSITY) + MINDENSITY;
        fill(Tile.BlankTile); // TODO Consider using a rock tile to allow mining?

        //Make a starting room
        boolean success = false;
        while(!success) {
            int width = Util.randomInRange(MINROOMSIZE, MAXROOMSIZE, r);
            int height = Util.randomInRange(MINROOMSIZE, MAXROOMSIZE, r);
            int x = Util.randomInRange(1, getWidth() - width - 1, r);
            int y = Util.randomInRange(1, getHeight() - height - 1, r);

            Room tempRoom = new Room(x, y, width, height);
            if(roomValid(tempRoom)) {
                fillRoom(tempRoom);
                rooms.add(tempRoom);
                UpStair stair = new UpStair(x + (width/2), y + (height / 2), 0);
                objects.add(stair);
                success = true;
            }
        }
        while(getDensity() < density) {
            int x = Util.randomInRange(1, getWidth() - MINROOMSIZE - 1, r);
            int y = Util.randomInRange(1, getHeight() - MINROOMSIZE - 1, r);
            if(getTile(x, y) == Tile.WallTile) {

				//Find a direction to build a room
				Direction dir = getDirection(x, y, r);
				if (dir != null) {
					int width = Util.randomInRange(MINROOMSIZE, MAXROOMSIZE, r);
					int height = Util.randomInRange(MINROOMSIZE, MAXROOMSIZE, r);

					//Decide whether this is a corridor
					if (r.nextInt(100) < CORRIDORCHANCE) {
						switch (dir) {
							case NORTH:
							case SOUTH:
								width = 1;
								break;
							case EAST:
							case WEST:
								height = 1;
								break;
						}
					}
					Room tempRoom = makeRoom(x, y, width, height, dir, r);
					if (roomValid(tempRoom)) {
						fillRoom(tempRoom);
						rooms.add(tempRoom);
						//Add a door
						boolean closed = r.nextInt(100) < 50;
						makeDoor(x, y, closed);
					}
				}
			}
        }
		//Place a downstair
		while(true) {
			int roomNo = r.nextInt(rooms.size());
			Room selectedRoom = rooms.get(roomNo);
			if (!selectedRoom.isCorridor()) {
				int stairX = selectedRoom.x + selectedRoom.width/2;
				int stairY = selectedRoom.y + selectedRoom.height/2;
				Point startingPosition = getStartingPosition();

				//Make sure the upstair and downstair aren't on top of each other
				if (stairX != startingPosition.getX() && stairY != startingPosition.getY()) {
					DownStair stair = new DownStair(stairX, stairY, 0);
					objects.add(stair);
					break;
				}
			}

		}

    }

	private void makeDoor(int x, int y, boolean closed) {
		terrain[x][y] = Tile.GroundTile;
		Door d = new Door(x, y, closed);
		objects.add(d);
	}

	/**
	 * Finds the top corner of a room, and makes a room object, given the door position,
	 * direction and size of the room.
	 * Depending on the direction, this method will shift the room out of the road on one
	 * axis, and randomly shift on the other axis.
	 * @param x Coordinate of the door
	 * @param y Coordinate of the door
	 * @param width Width of the room
	 * @param height Height of the room
	 * @param dir Direction the room is facing
	 * @param r Random number generator for the map
	 * @return An appropriately positioned room for the given coordinates.
	 */
    private Room makeRoom(int x, int y, int width, int height, Direction dir, Random r) {
        int roomX = x;
        int roomY = y;
        switch(dir) {
        case NORTH:
            roomY -= height;
            roomX -= r.nextInt(width);
			break;
        case SOUTH:
            roomY += 1;
            roomX -= r.nextInt(width);
			break;
        case WEST:
            roomX -= width;
            roomY -= r.nextInt(height);
			break;
        case EAST:
            roomX += 1;
            roomY -= r.nextInt(height);
			break;
        }
        return new Room(roomX, roomY, width, height);
    }


    private int getDensity() {
        int density;
        //Calculate total tiles
        double total = getWidth() * getHeight();
        double filled = 0;
        for(int i = 0; i < getWidth(); i++) {
                for(int j = 0; j < getHeight(); j++) {
                        if(terrain[i][j] != Tile.BlankTile) {
                            
                                filled++;
                        }
                }
        }
        density = (int) (filled / total * 100);
//        System.out.println("Density Claculation: " + density + "% (" +
//                      (int)filled + "/" + (int)total + ")");
        return density;
    }


    /**
     * Fills all of the tiles covered by this room with ground tiles and adds walls
	 * to the edge of the room.
	 * We have already checked to ensure that the space we are filling is valid, so we can do
	 * with it whatever we want, really.
     * @param room The room to fill
     */
    private void fillRoom(Room room) {
		for(int i = -1; i <= room.width; i++) {
			for (int j = -1; j <= room.height; j++) {
				terrain[room.x + i][room.y + j] = Tile.WallTile;
			}
		}
        for(int i = 0; i < room.width; i++) {
            for(int j = 0; j < room.height; j++) {
                terrain[room.x + i][room.y + j] = Tile.GroundTile;
            }
        }
    }

	/**
	 * Given a position on the map, observes the tiles around it to determine which direction a
	 * room should be built in
	 * @param x Coordinate to examine
	 * @param y Coordinate to examine
	 * @return The direction where a room is valid, or null if one could not be found.
	 */
    public Direction getDirection(int x, int y, Random r) {
		//Check for walls to left and right
		final Tile below = getTile(x, y + 1);
		final Tile above = getTile(x, y - 1);
		final Tile left = getTile(x - 1, y);
		final Tile right = getTile(x + 1, y);
		if (left == Tile.WallTile && right == Tile.WallTile) {
			//Check for floor above or below
			if (above == Tile.GroundTile && below == Tile.GroundTile) {
				if (r.nextInt(100) < MAKEDOORCHANCE) {
					makeDoor((int) x, (int) y, r.nextInt(100) < 50);
					System.out.println("Made a wall into a door");
					return null;
				}
			}
			else if (below == Tile.GroundTile) {
				return Direction.NORTH;
			} else if (above == Tile.GroundTile) {
				return Direction.SOUTH;
			}
		}
		//Check for walls to above and below
		if (above == Tile.WallTile && below == Tile.WallTile) {
			//Check for floor left and right
			if (left == Tile.GroundTile && right == Tile.GroundTile) {
				if (r.nextInt(100) < MAKEDOORCHANCE) {
					makeDoor((int) x, (int) y, r.nextInt(100) < 50);
					System.out.println("Made a wall into a door");
					return null;
				}
			}
			if (left == Tile.GroundTile) {
				return Direction.EAST;
			} else if (right == Tile.GroundTile) {
				return Direction.WEST;
			}
		}
		return null; //No appropriate direction was found =(
	}

	/**
     * Checks to see whether the given room can be added without overlap to this map.
     * Should also return false if the room leaves the bounds of the map.
     * @param room
     * @return
     */
    private boolean roomValid(Room room) {
        for(int i = 0; i < room.width; i++) {
            for(int j = 0; j < room.height; j++) {
                if(getTile(room.x + i, room.y + j) != Tile.BlankTile) {
                    return false;
                }
            }
        }
		//Check borders: Edges of the room must either be walls or blank
		
		//First check top and bottom - this includes both corner walls
		for (int i = -1; i <= room.width; i++) {
			Tile top = getTile(room.x + i, room.y - 1);
			if (!tileBlankOrWall(top)) {
				return false;
			}
			Tile bottom = getTile(room.x + i, room.y + room.height);
			if (!tileBlankOrWall(bottom)) {
				return false;
			}
		}
		//Next check left and right, not including corners
		for (int i = 0; i < room.height; i++) {
			Tile left = getTile(room.x - 1, room.y + i);
			if(!(tileBlankOrWall(left))) {
				return false;
			}
			Tile right = getTile(room.x + room.width, room.y + i);
			if(!(tileBlankOrWall(right))) {
				return false;
			}
		}
        return true;
    }

	private boolean tileBlankOrWall(Tile tile) {
		return (tile == Tile.BlankTile || tile == Tile.WallTile);
	}


	@Override
    public Point getStartingPosition() {
        ArrayList<UpStair> s = getObjects(UpStair.class);
        if(s.size() > 0) {
            int x = s.get(0).getX();
            int y = s.get(0).getY();

            return new Point(x,y);
        } else {
            return new Point(0,0);
        }
    }
}