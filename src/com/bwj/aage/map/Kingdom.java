package com.bwj.aage.map;

import com.bwj.aage.Point;
import com.bwj.aage.config.Race;

import java.io.Serializable;

/**
 * Contains the data representing a single kingdom on the world map. Kingdoms
 * are represented as a 2D array of bytes, where different byte values represent
 * different kingdoms. Each kingdom has a name and a capital city.
 * @author Tim Sergeant
 */
public class Kingdom implements Serializable {
	private byte id;
	private String name;
	private Point capitalPosition;
	private Race kingdomRace;

	public Kingdom(byte id, String name, Point capitalPosition, Race kingdomRace) {
		this.id = id;
		this.name = name;
		this.capitalPosition = capitalPosition;
		this.kingdomRace = kingdomRace;
	}


	public byte getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Point getCapitalPosition() {
		return capitalPosition;
	}

	public Race getKingdomRace() {
		return kingdomRace;
	}

	public String getKingdomDescription() {
		return "The " + kingdomRace.getName() + " Kingdom of " + name;
	}
}
