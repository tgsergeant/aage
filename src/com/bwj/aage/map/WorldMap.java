package com.bwj.aage.map;

import com.bwj.aage.*;
import com.bwj.aage.config.ConfigManager;
import com.bwj.aage.config.Race;
import com.bwj.aage.object.*;
import com.bwj.aage.util.MarkovNameGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class WorldMap extends Map {

	/**
	 *
	 */
	private static final long serialVersionUID = 4109711304467405399L;
	Random r;

	public WorldMap(long seed) {
		super(seed, 150, 150);
		sizeMod = getWidth() * getHeight() / 1600;
		setName("The Overworld");

		r = new Random(getSeed());
		kingdomData = new Byte[getWidth()][getHeight()];
	}


	@Override
	public void setSize(int w, int h) {
		super.setSize(w, h);
		sizeMod = getWidth() * getHeight() / 1600;
		setName("The Overworld");
	}


	protected int sizeMod;
	private int CLOSEPROB;
	private int REPETITIONS;
	private int REQUIREDNEIGHBOURS;

	private Point startingPosition;

	private Byte[][] kingdomData;
	private Kingdom[] kingdoms;

	protected void generateFeatures(Random r) {
		int featureCount = 0;
		//Generate entrances
		while(featureCount < 5*sizeMod) {
			int x = r.nextInt(getWidth());
			int y = r.nextInt(getHeight());
			// TODO It would be nice to have entrances more likely in certain areas.
			// TODO Also, how about modifying villages based on what terrain they're in?
			if(!terrain[x][y].blocksMovement() && terrain[x][y] != Tile.OceanTile) {
				int roll = r.nextInt(100);
				MapObject newobj;
				if (getKingdomId(x, y) == null) {
					//Make dungeons more common outside of civilised areas
					if (roll < 35) {
						newobj = new CaveEntrance(x, y, r, getSeed());
					}
					else if(roll < 70){
						newobj = new DungeonEntrance(x,y,r, getSeed());
					} else if(roll < 87) {
						newobj = new VillageEntrance(x, y, r, getSeed());
					} else {
						newobj = new TownEntrance(x, y, getSeed(), r);
					}
				} else {
					if (roll < 40) {
						newobj = new VillageEntrance(x, y, r, getSeed());
					} else if(roll < 75) {
						newobj = new TownEntrance(x, y, getSeed(), r);
					} else if(roll < 85) {
						newobj = new CaveEntrance(x, y, r, getSeed());
					} else {
						newobj = new DungeonEntrance(x, y, r, getSeed());
					}
				}
				objects.add(newobj);
				featureCount++;
			}
		}
	}


	/**
	 * Counts the number of a certain tile surrounding the tile at a given position.
	 * @param x Position to search
	 * @param y Position to search
	 * @param t The tile to search for
	 * @return The number of grass tiles around this tile
	 */
	private int countClose(int x, int y, Tile t) {
		int closed = 0;
		for(int i = x - 1; i <= x + 1; i++) {
			for(int j = y - 1; j <= y + 1; j++) {
				if(getTile(i, j) == t) {
					closed++;
				}
			}
		}
		return closed;
	}

	@Override
	public Point getStartingPosition() {
		return startingPosition;

	}

	public void generateStartingPosition( Random r) {
		while(true) {
			int x = r.nextInt(getWidth());
			int y = r.nextInt(getHeight());
			if(terrain[x][y] == Tile.GrassTile) {
				startingPosition = new Point(x,y);
				return;
			}

		}
	}
	/**
	 * Populates the map with feature tiles to provide visual variety
	 * @param t The tile to fill with
	 * @param r Allows the seed to be carried over
	 * @param initialChance Weighting of the number of neighbours relative to chanceRange
	 * @param reps The number of times to repeat the algorithm. A few thousand works well
	 * @param guaranteed The number of features to guarantee at the start (as 'seeds')
	 * @param desiredNeighbour Tile to clump the new feature around (eg, place mountains near the ocean)
	 * @param chanceRange Somewhere around 7*initialChance is a good value
	 * @param prune The minimum number of neighbours needed to survive at the end.
	 */
	protected void featureCA(Tile t, Random r, int initialChance, int reps, int guaranteed, Tile desiredNeighbour, int chanceRange, int prune) {
		//Generate a number of random features
		for(int i = 0; i < reps; i++) {
			int x = r.nextInt(getWidth());
			int y = r.nextInt(getHeight());
			if(terrain[x][y] == t || terrain[x][y] == Tile.GrassTile) {
				//A tile has a greater chance of being a feature if tiles around it are too.
				int derivedChance = (countClose(x, y, t) + 1) * initialChance;
				//A number at the start are guaranteed to be generated
				if(i < guaranteed) {
					derivedChance = chanceRange;
				}
				if(countClose(x, y, desiredNeighbour) >= 2) {
					derivedChance += 2 * initialChance;
				}
				int chance = r.nextInt(chanceRange);
				if(chance < derivedChance) {
					terrain[x][y] = t;
				}
				else {
					terrain[x][y] = Tile.GrassTile;
				}
			}
		}

		//Prune features with few neighbours
		for(int k = 0; k < 2; k++) {
			WorldMap m = new WorldMap(0);
			m.setSize(getWidth(), getHeight());
			for(int i = 0; i < this.getWidth(); i++) {
				for(int j = 0; j < this.getHeight(); j++) {
					if(terrain[i][j] == t && countClose(i, j, t) <= prune) {
						m.terrain[i][j] = Tile.GrassTile;
					}
					else {
						m.terrain[i][j] = terrain[i][j];
					}

				}
			}
			terrain = m.terrain;
		}
	}

	/**
	 * Creates an array of floats and fills it with completely random noise
	 *
	 * @param width  Width of the array
	 * @param height Height of the array
	 * @return White noise with the given width and height
	 */
	private float[][] generateWhiteNoise(int width, int height) {
		float[][] noise = new float[width][height];
		for (int i = 0; i < noise.length; i++) {
			for (int j = 0; j < noise[0].length; j++) {
				noise[i][j] = (float) r.nextDouble();
			}
		}
		return noise;
	}

	/**
	 * Generates the kth octave of the perlin noise, by sampling the white noise
	 * at even intervals and interpolating between these points
	 * @param baseNoise The white noise to sample
	 * @param depth The depth of the octave
	 * @return Smooth noise for this octave
	 */
	private float[][] generateSmoothNoise(float[][] baseNoise, int depth) {
		int width = baseNoise.length;
		int height = baseNoise[0].length;

		float[][] smoothNoise = new float[width][height];

		int samplePeriod = (int) Math.pow(2, depth);
		float sampleFrequency = (float) (1.0 / samplePeriod);

		for (int i = 0; i < width; i++) {
			int horSampleStart = (i / samplePeriod) * samplePeriod;
			int horSampleEnd = (horSampleStart + samplePeriod) % width;

			float horBlend = (i - horSampleStart) * sampleFrequency;

			for (int j = 0; j < height; j++) {
				int verSampleStart = (j / samplePeriod) * samplePeriod;
				int verSampleEnd = (verSampleStart + samplePeriod) % height;
				float verBlend = (j - verSampleStart) * sampleFrequency;

				//Blend top two corners
				float blend1 = interpolate(baseNoise[horSampleStart][verSampleStart], baseNoise[horSampleEnd][verSampleStart], horBlend);

				//Blend top two corners
				float blend2 = interpolate(baseNoise[horSampleStart][verSampleEnd], baseNoise[horSampleEnd][verSampleEnd], horBlend);

				//Blend together
				smoothNoise[i][j] = interpolate(blend1, blend2, verBlend);

			}
		}
		return smoothNoise;
	}

	/**
	 * Creates Perlin Noise for the given width and height by creating white noise,
	 * creating n different octaves of smooth noise, and then adding them together
	 * and normalising.
	 * @param width Width of the noise to create
	 * @param height Height of the noise
	 * @param count Nunber of octaves to create
	 * @return Perlin noise.
	 */
	protected float[][] generatePerlinNoise(int width, int height, int count) {
		float[][] baseNoise = generateWhiteNoise(width, height);

		float[][][] layers = new float[count][width][height];
		for (int i = 0; i < count; i++) {
			layers[i] = generateSmoothNoise(baseNoise, i);
		}

		float[][] mask = createRollingMask(width,  height);
		//printMask(mask);
		float[][] perlinNoise = new float[width][height];

		float amplitude = 1.0f;
		float totalAmplitude = 0.0f;
		final float persistence = 0.5f;

		for (int depth = count - 1; depth >= 0; depth--) {
			amplitude *= persistence;
			totalAmplitude += amplitude;
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					float multiplier = layers[depth][i][j] * amplitude * mask[i][j];
//					if(i < 10 || j < 10 || i > width - 10 || j > height - 10) {
//						multiplier *= mask[i][j];
//					}
					perlinNoise[i][j] += multiplier;
				}
			}
		}
		//Normalisation
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				perlinNoise[i][j] *= 255 / totalAmplitude;
			}
		}
		return perlinNoise;
	}

	/**
	 * Gets the waterline for  the given noise
	 *
	 * @param noise Noise function to analyse
	 * @param depth How deep the waterline should be
	 * @return Level at which the waterliune should be placed
	 */
	protected float getWaterline(float[][] noise, float depth) {
		ArrayList<Float> heights = new ArrayList<Float>(getWidth() * getHeight());
		int sizeMod = this.sizeMod / 2;
		for (int i = 0; i < getWidth(); i+= sizeMod) {
			for (int j = 0; j < getHeight(); j+= sizeMod) {
				heights.add(noise[i][j]);
			}
		}
		Collections.sort(heights);
		System.out.println("Sorted " + heights.size() + " heights.");
		return heights.get((int) (heights.size() * depth));
	}

	/**
	 * Linearly interpolates between two values
	 * @param x0 First value
	 * @param x1 Second value
	 * @param alpha Distance from the first value
	 * @return Linear interpolation
	 */
	private float interpolate(float x0, float x1, float alpha) {
		return x1 * alpha + x0 * (1 - alpha);
	}

	private float[][] createRollingMask(int width, int height) {
		float[][] mask = new float[width][height];
		float maxHeight = 0;
		//Repeat for 3000 particles
		for(int count = 0; count < 3000 * sizeMod; count++) {
			int ballx, bally;
			ballx = r.nextInt(9 * width / 10)  + width / 20;
			bally = r.nextInt(9 * height / 10) + height / 20;

			life: for(int life = 0; life < 40 * sizeMod; life++) {
				mask[ballx][bally] ++;
				if(maxHeight < mask[ballx][bally]) {
					maxHeight = mask[ballx][bally];
				}
				int rollattempts = 0;
				while(true) {
					int newx = ballx, newy = bally;
					int dir = r.nextInt(4);
					switch(dir) {
						case 0: newy--;break;
						case 1: newx++; break;
						case 2: newy++; break;
						case 3: newx--; break;
					}
					if(newx < 0 || newy < 0 || newx >= width || newy >= height) { //Ignore out of bounds
					}
					else if(mask[newx][newy] < mask[ballx][bally]) {
						ballx = newx;
						bally = newy;
						break;
					}
					rollattempts++;
					if(rollattempts > 8) {
						break life;
					}
				}
			}
		}
		//Normalise to between 0 and 1
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				mask[i][j] /= maxHeight;
				mask[i][j] += 0.25;
				if(mask[i][j] > 1) {
					mask[i][j] = 1;
				}
			}
		}
		return mask;
	}

	private <T> void printMask(T[][] mask) {
		for(int i = 0; i < mask.length; i++) {
			String line = "";
			for(int j = 0; j < mask[0].length; j++) {

				line += (mask[i][j]==null) ? " " : mask[i][j] + " ";
			}
			System.out.println(line);
		}
	}

	@Override
	public void generate(HashMap<String, Integer> param) {
		long start = System.currentTimeMillis();
		float[][] perlinNoise = generatePerlinNoise(getWidth(), getHeight(), 6);
		long end = System.currentTimeMillis();
		System.out.println("Created perlin noise in " + (end - start));

		float waterLine = getWaterline(perlinNoise, 0.5f);
		long end2 = System.currentTimeMillis();
		System.out.println("Found water level: " + (end2 - end) + ", " + waterLine);

		float mountainLine = getWaterline(perlinNoise,  0.985f);
		float hillLine = getWaterline(perlinNoise, 0.94f);
		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				if (perlinNoise[i][j] < waterLine - 20) {
					terrain[i][j] = Tile.OceanTile;
				} else if (perlinNoise[i][j] < waterLine) {
					terrain[i][j] = Tile.ShallowOcean;
				} else if (perlinNoise[i][j] < waterLine + 8) {
					terrain[i][j] = Tile.SandTile;
				} else if (perlinNoise[i][j] > mountainLine + 1) {
					terrain[i][j] = Tile.MountainTile;
				} else if (perlinNoise[i][j] > hillLine) {
					terrain[i][j] = Tile.HillTile;
				} else if (perlinNoise[i][j] > waterLine + 60) {
					terrain[i][j] = Tile.ForestTile;
				} else {
					terrain[i][j] = Tile.GrassTile;
				}
			}
		}
		featureCA(Tile.ForestTile, r, 7, 1200 * sizeMod, 100 * sizeMod, null, 60, 1);
		generateFeatures(r);
		long startTime = System.currentTimeMillis();
		generateKingdoms(r);
		System.out.println("Kingdoms: " + (System.currentTimeMillis() - start));
		generateStartingPosition(r);
	}
	public static final int NUMBEROFKINGDOMS = 5;


	/**
	 * Uses an algorithm I pulled from mid air to generate kingdomData all over the map. A number
	 * of starting positions are generated (which will later be used to generate capital cities),
	 * and then each kingdom searches randomly for a position which:
	 * a) is unoccupied
	 * b) Is not water
	 * c) Is adjacent to another tile owned by their kingdom
	 * Kingdoms continue taking turns until they start having trouble looking for somewhere to
	 * expand to.
	 * @param r Random number generator for the map.
	 */
	private void generateKingdoms(Random r) {
		Point[] startingPositions = new Point[NUMBEROFKINGDOMS];
		boolean[] openKingdoms = new boolean[NUMBEROFKINGDOMS];
		for (byte i = 0; i < NUMBEROFKINGDOMS; i++) {
			openKingdoms[i] = true;
			while (true) {
				int x = r.nextInt(getWidth());
				int y = r.nextInt(getHeight());
				if (!terrain[x][y].blocksMovement()) {
					startingPositions[i] = new Point(x, y);
					kingdomData[x][y] = i;
					System.out.println("Starting position for kingdom " + i + ": " + x + ", " + y);
					break;
				}
			}
		}

		while (containsTrue(openKingdoms)) {
			//Take it in turns
			for (byte i = 0; i < NUMBEROFKINGDOMS; i++) {
				if (!openKingdoms[i]) {
					continue; //Skip any kingdomData that are closed
				}
				int attempts = 0;
				while (true) {
					int x = r.nextInt(getWidth());
					int y = r.nextInt(getHeight());
					if (terrain[x][y] == Tile.OceanTile || terrain[x][y] == Tile.ShallowOcean) {
						continue;
					}
						
					if (kingdomData[x][y] == null) {
						if(isAdjacentToKingdom(x, y, i)) {
							kingdomData[x][y] = i;
							break;
						}
					} else {
						attempts++;
						if (attempts > 500) {
							openKingdoms[i] = false;
							//System.out.println("Gave up on kingdom " + i);
							//printMask(kingdomData);
							break;
						}
					}
				}
			}
		}
		//Now that generation has finished, we need to go through and filter out any small gaps near the borders
		Byte[][] filtered = new Byte[getWidth()][getHeight()];
		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				if (kingdomData[i][j] == null) {
					for (byte k = 0; k < NUMBEROFKINGDOMS; k++) { //Iterate through kingdomData
						int count = countKingdomAdjacent(i, j, k);
						if (count > 6) {
							kingdomData[i][j] = k;
							break;
						}
					}
					
				}
			}
		}

		kingdoms = new Kingdom[NUMBEROFKINGDOMS];
		ArrayList<Race> kingdomRaces = ConfigManager.getRaceByTag("kingdoms");

		for (byte i = 0; i < NUMBEROFKINGDOMS; i++) {
			Point p = startingPositions[i];
			Race kingdomRace = Util.selectFromList(kingdomRaces, r);

			Kingdom k = new Kingdom(i, MarkovNameGenerator.kingdomGen.getName(r, 10), p, kingdomRace);

			CapitalCityEntrance city = new CapitalCityEntrance(k, getSeed(), r);
			objects.add(city);
			kingdoms[i] = k;
		}
	}

	/**
	 * Counts the number of tiles that the specified kingdom controls adjacent to
	 * the given tile
	 * @param x Tile to inspect
	 * @param y Tile to inspect
	 * @param kingdom Kingdom to count
	 * @return The number of kingdom tiles adjacent to this tile
	 */
	private int countKingdomAdjacent(int x, int y, Byte kingdom) {
		int count = 0;
		for (int i = x - 1; i <= x + 1; i++) {
			for (int j = y - 1; j <= y + 1; j++) {
				if (kingdom.equals(getKingdomId(i, j))) {
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * Determines if the given coordinate is adjacent to a tile of the given kingdom.
	 * Does not count diagonals
	 * @param x Position to consider
	 * @param y Position to consider
	 * @param kingdom Kingdom to search for
	 * @return True if that kingdom has a position adjancent to the given tile
	 */
	private boolean isAdjacentToKingdom(int x, int y, Byte kingdom) {
		return kingdom.equals(getKingdomId(x - 1, y)) ||
				kingdom.equals(getKingdomId(x + 1, y)) ||
				kingdom.equals(getKingdomId(x, y + 1)) ||
				kingdom.equals(getKingdomId(x, y - 1));
	}

	/**
	 * Returns the kingdom at the given coordinate, or null if there is
	 * no kingdom or the coordinates are out of bounds.
	 * @param x Coordinate to look up
	 * @param y Coordinate to look up
	 * @return The kingdom at that position, or null
	 */
	public Byte getKingdomId(int x, int y) {
		try {
			return kingdomData[x][y];
		} catch (Exception e) {
			return null;
		}
	}

	public Kingdom getKingdom(byte id) {
		return kingdoms[id];
	}

	public Kingdom getKingdom(int x, int y) {

		final Byte kingdomId = getKingdomId(x, y);
		if (kingdomId == null) {
			return null;
		}
		return kingdoms[kingdomId];
	}

	/**
	 * Determines whether the list of booleans contains a true element
	 * @param booleans Booleans to check
	 * @return True if one of the bools in the array is true.
	 */
	private boolean containsTrue(boolean[] booleans) {
		for (boolean b : booleans) {
			if (b) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Overrides the default tile descriptor to provide a kingdom name, if
	 * the current location is part of a kingdom.
	 * @param p Player
	 * @param x Location of pointer
	 * @param y Location of pointer
	 * @param showFull Verbosity. Will only show kingdom name if true
	 * @return Description of the given tile.
	 */
	@Override
	public String getTileDescriptor(Player p, int x, int y, boolean showFull) {
		String preprend = "";
		if (showFull) {
			final Kingdom kingdom = getKingdom(x, y);
			if(kingdom != null) {
				preprend = "Kingdom of " + kingdom.getName() + ". ";
			}
		}

		return preprend + super.getTileDescriptor(p, x, y, showFull);
	}
}