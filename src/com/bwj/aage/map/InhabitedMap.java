package com.bwj.aage.map;

import com.bwj.aage.Map;
import com.bwj.aage.Point;
import com.bwj.aage.Tile;
import com.bwj.aage.config.CharacterClass;
import com.bwj.aage.config.ConfigManager;
import com.bwj.aage.config.Race;
import com.bwj.aage.entity.TownNPC;
import com.bwj.aage.object.Door;
import com.bwj.aage.util.MarkovNameGenerator;
import net.slashie.util.Util;

import java.util.ArrayList;
import java.util.Random;

/**
 * Base class for maps which contain houses and people.
 */
public abstract class InhabitedMap extends Map {
	protected long parentSeed;
	protected ArrayList<House> houses = new ArrayList<House>();
	protected int population = 0;

	public InhabitedMap(long seed, int width, int height, long parentSeed) {
		super(seed, width, height);
		this.parentSeed = parentSeed;
	}

	protected void fillHouse(House house, int direction) {
		for (int i = -1; i < house.getWidth() +1; i++) {
			for (int j = -1; j < house.getHeight() + 1; j++) {
				terrain[i + house.getX()][j + house.getY()] = Tile.WoodenWallTile;
			}
		}

		//Fill floor.
		//With dirt. Poor people
		for (int i = 0; i < house.getWidth(); i++) {
			for (int j = 0; j < house.getHeight(); j++) {
				terrain[i + house.getX()][j + house.getY()] = Tile.IndoorFloorTile;
			}

		}

		int doorx = house.getX() - 1;
		int doory = house.getY() - 1;
		switch (direction) {
			case 0: //North
				doorx += house.getWidth() / 2;
				break;
			case 1: //East
				doory += house.getHeight() / 2;
				doorx += house.getWidth() + 1;
				break;
			case 2:
				doorx += house.getWidth() / 2;
				doory += house.getHeight() + 1;
				break;
			case 3: //West
				doory += house.getHeight() / 2;
				break;
		}
		terrain[doorx][doory] = Tile.GroundTile;
		Door houseDoor = new Door(doorx, doory, false);
		objects.add(houseDoor);

	}
	public long getParentSeed() {
		return parentSeed;
	}

	public void addHouse(House toAdd) {
		houses.add(toAdd);
		population += toAdd.getPopulation();
	}
	/**
	 * Fills map with all the NPCs it can fit.
	 * @param r
	 */
	public void populateMap(Random r) {
		System.out.println("Making " + population + " npcs");
		ArrayList<Race> townRaces = ConfigManager.getRaceByTag("townnpc");
		ArrayList<CharacterClass> townClasses = ConfigManager.getClassesByTag("townnpc");

		for (House h : houses) {
			String surname = MarkovNameGenerator.lastNameGen.getName(r);

			Race npcRace = townRaces.get(Util.rand(0, townRaces.size()-1));

			for (int i = 0; i < h.getPopulation(); i++) {
				Point spawnPoint = h.getRandomPointInside();

				String firstName;
				if (r.nextBoolean()) {
					firstName = MarkovNameGenerator.maleNameGen.getName(r);
				} else {
					firstName = MarkovNameGenerator.femaleNameGen.getName(r);
				}


				
				TownNPC npc = new TownNPC(spawnPoint.getX(), spawnPoint.getY(), h, this,
										npcRace, townClasses.get(Util.rand(0, townClasses.size()-1)),
										firstName + " " + surname);
				objects.add(npc);
			}
		}
	}
}
