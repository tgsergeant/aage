package com.bwj.aage.map;

import com.bwj.aage.Map;
import com.bwj.aage.Tile;
import com.bwj.aage.config.ConfigManager;
import com.bwj.aage.config.MonsterTemplate;
import com.bwj.aage.config.Race;
import com.bwj.aage.entity.Monster;
import com.bwj.aage.entity.UniqueMonsterTable;

import java.util.Random;

/**
 * A map with a monster spawner which is able to spawn hostile monsters
 */
public abstract class HostileMap extends Map {


	Random spawnerRandom = new Random();

	private Race filterRace = null;

	/** An integer representing the difficulty of this map. Details are yet to be finalised */
	private int difficultyLevel = 0;

	public HostileMap(long seed, int width, int height) {
		super(seed, width, height);

	}

	public HostileMap(int width, int height) {
		super(width, height);
	}

	@Override
	 public void update() {
		super.update();

		//Update the spawner. Monsters will only fill 65% of the initial quota after
		//the initial filling of the map.
		double desiredChallenge = getDesiredChallenge() * 0.65;
		if(getChallengeLevel() < desiredChallenge) {
			int chance = spawnerRandom.nextInt(100);
			if(chance < 3) {
				spawnMonster(getSpawnLevel());
			}
		}
	}

	/**
	 * Difficulty level should be around the same level as the character
	 * @return The difficulty level of the map
	 */
	// TODO Think about how this should work again
	public int getDifficulty() {
		return difficultyLevel;
	}

	public void setDifficulty(int difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}

	/**
	 * Spawns a random monster at a random point on the map.
	 */
	public void spawnMonster(int level) {
		while(true) {
			int x = spawnerRandom.nextInt(getWidth());
			int y = spawnerRandom.nextInt(getHeight());
			//System.out.println("Monster at: " + x + ", " + y);
			if (!getTile(x, y).blocksMovement()) {
				MonsterTemplate chosenMonster = ConfigManager.findMonster(level, filterRace);
				if(chosenMonster == null) {
					return;
				}
				if (chosenMonster.hasAttribute("unique")) {
					final UniqueMonsterTable table = UniqueMonsterTable.getTable();
					if (table.hasId(chosenMonster.getId())) {
						return;
					} else {
						table.addId(chosenMonster.getId());
					}
				}
				Tile t = new Tile(chosenMonster.getRace().getTileChar(), chosenMonster.getColor());
				Monster m = new Monster(x, y, t, this, chosenMonster.getRace(), chosenMonster.getCharacterClass(), chosenMonster.getName());
				m.setLevel(level);
				System.out.println("Spawning level " + m.getLevel() + " " + m.getName());
				objects.add(m);
				return;
			}
		}
	}

	/**
	 * Calculates a random level for a monster to be spawned. This is determined by
	 * a gaussian distribution centered around the difficulty of the map.
	 * Other map classes can override this to customise the spawning algorithm.
	 * @return A random level for the monster spawner
	 */
	protected int getSpawnLevel() {
		int level = (int) (spawnerRandom.nextGaussian() * 2 + getDifficulty() - 1);
		return (level > 0) ? level : 1;
	}

	/**
	 * Calculates the number of challenge points which should be used by the map.
	 * Monsters use up this quota depending on their difficulty and level.
	 * @return Challenge quota for the map.
	 */
	protected int getDesiredChallenge() {
		return getDifficulty() * 5;
	}

	/**
	 * Populates the map with as many monsters as it can hold.
	 */
	public void populateMap() {
		int desiredChallenge = getDesiredChallenge();

		while (getChallengeLevel() < desiredChallenge) {
			spawnMonster(getSpawnLevel());
		}
	}

	public void setRaceFilter(Race race) {
		filterRace = race;
	}
}
