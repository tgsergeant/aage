package com.bwj.aage.map;

import com.bwj.aage.Direction;
import com.bwj.aage.Point;
import com.bwj.aage.Tile;

import java.util.HashMap;
import java.util.Random;

/**
 * Generates a town - a map with a large number of houses arranged in an orderly 
 * fashion around roads.
 */
public class TownMap extends InhabitedMap {
	private Point startingPosition = null;

	public TownMap(long seed, int width, int height, long parentSeed) {
		super(seed, width, height, parentSeed);
	}

	public TownMap(long seed, long parentSeed) {
		super(seed, 75, 75, parentSeed);
	}

	@Override
	public void generate(HashMap<String, Integer> param) {
		Random r = new Random(getSeed());
		fill(Tile.GrassTile);
		generate(1, 1, getWidth() - 2, getHeight() - 2, 0, r, null);
		populateMap(r);
	}

	/**
	 * Recursive generation method for the map. It is supplied with a 'block' of land, which is subdivided into
	 * up to 4 more blocks of smaller land. Once the blocks are small enough, houses can be made in each block.
	 * We pass through a direction in order to allow doors to be placed correctly.
	 * @param startx Position of the block of land
	 * @param starty Position of the block of land
	 * @param width Width of the block
	 * @param height Height of the block
	 * @param depth Depth of the recursion (used for street width)
	 * @param r RNG for the map
	 * @param dir Represents the quadrant that this method was entered from
	 */
	private void generate(int startx, int starty, int width, int height, int depth, Random r, Direction dir) {
		//Determine street width
		int streetWidth = 1;
		if (depth == 0) {
			streetWidth = 4;
		}
		if (width < 15 && height < 15) {
			streetWidth = 0;
		}
		//If we're too small for a reasonable house, make a grassy area
		if (width < 4 || height < 4) {
			fillArea(startx, starty, width, height, Tile.GrassTile);
			return;
		}
		//Generate a house
		if (width <= 10 && height <= 10) {
			final House house = new House(startx + 1, starty + 1, width - 2, height - 2); //Needs to account for walls

			fillHouse(house, getDoorDirection(dir, r));
			addHouse(house);
			return;
		}
		int deltax = 0;
		int deltay = 0;
		if(width > 10) {

			deltax = width / 4 + r.nextInt(width / 2) - streetWidth/2;
			//Vertical road
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < streetWidth; j++) {
					terrain[startx + deltax + j][starty + i] = Tile.RoadTile;
				}
			}
		}
		if (height > 10) {
			deltay = height / 4 + r.nextInt(height / 2) - streetWidth/2; //The height at which the road starts
			//Horizontal road
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < streetWidth; j++) {
					terrain[startx + i][starty + deltay + j] = Tile.RoadTile;
				}
			}

		}


		//Recursion? Yes please
		if (deltax != 0 && deltax != 0) {
			//Generate all 4 corners
			generate(startx, starty, deltax, deltay, depth + 1, r, Direction.NORTHWEST); //Top left
			generate(startx + deltax + streetWidth, starty, width - deltax - streetWidth, deltay, depth + 1, r, Direction.NORTHEAST); //Top right
			generate(startx, starty + deltay + streetWidth, deltax, height - deltay - streetWidth, depth + 1, r, Direction.SOUTHWEST); //Bottom left
			generate(startx + deltax + streetWidth, starty + deltay + streetWidth,
					width - deltax - streetWidth, height - deltay - streetWidth, depth + 1, r, Direction.SOUTHEAST);
		}
		else if (deltay != 0) {
			//Split horizontally only
			generate(startx, starty, width, deltay, depth + 1, r, Direction.NORTH); //Top
			generate(startx, starty + deltay + streetWidth, width, height - deltay - streetWidth, depth + 1, r, Direction.SOUTH); //Bottom
		}
		else if (deltax != 0) {
			//Split vertically only
			generate(startx, starty, deltax, height, depth + 1, r, Direction.EAST);
			generate(startx + deltax + streetWidth, starty, width - deltax - streetWidth, height, depth + 1, r, Direction.WEST);

		}

		//Generate starting position
		if (depth == 0) {
			startingPosition = new Point(1, deltay + 2);
		}
	}

	@Override
	public Point getStartingPosition() {
		return startingPosition;
	}

	/**
	 * Given the direction of a block of land, determines which directions are guaranteed to be valid
	 * for door placement. This avoids issues with blocks of land which border each other with no road
	 * between them. Only a single valid direction is selected, and is returned as an int, where 0 is
	 * north and 3 is west.
	 * @param dir Direction the block of land was allocated from
	 * @param r Random number generator the map
	 * @return Direction the door should be placed in.
	 */
	public int getDoorDirection(Direction dir, Random r) {
		int[] allowedDirs = new int[]{0};
		switch(dir) {
			case EAST:
				allowedDirs = new int[]{0, 1, 2};
				break;
			case NORTH:
				allowedDirs = new int[]{3, 0, 1};
				break;
			case WEST:
				allowedDirs = new int[]{0, 2, 3};
				break;
			case SOUTH:
				allowedDirs = new int[]{1, 2, 3};
				break;
			case NORTHEAST:
				allowedDirs = new int[]{0, 1};
				break;
			case SOUTHEAST:
				allowedDirs = new int[]{1, 2};
				break;
			case SOUTHWEST:
				allowedDirs = new int[]{2, 3};
				break;
			case NORTHWEST:
				allowedDirs = new int[]{3, 0};
		}		
		return allowedDirs[r.nextInt(allowedDirs.length)];
	}
	
}
