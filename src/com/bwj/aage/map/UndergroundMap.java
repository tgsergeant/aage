package com.bwj.aage.map;

import com.bwj.aage.object.DownStair;
import com.bwj.aage.object.UpStair;

import java.util.ArrayList;

/**
 *
 */
public abstract class UndergroundMap extends HostileMap {

	private int depth = 0;

	public UndergroundMap(long seed, int width, int height) {
		super(seed, width, height);
	}

	public void setParent(long parentSeed) {
		ArrayList<UpStair> s = getObjects(UpStair.class);
		if(s.size() > 0) {
			s.get(0).setToSeed(parentSeed);
		}
	}

	public void setChild(long childSeed) {
		ArrayList<DownStair> s = getObjects(DownStair.class);
		if(s.size() > 0) {
			s.get(0).setToSeed(childSeed);
		}
	}

	public void removeDownStair() {
		ArrayList<DownStair> s = getObjects(DownStair.class);
		for (DownStair stair : s) {
			objects.remove(stair);
		}
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	@Override
	protected int getSpawnLevel() {
		int level = (int) (spawnerRandom.nextGaussian() * 2 + getDifficulty() + 2 * (double)getDepth() / 3 - 1.5);
		return (level > 0) ? level : 1;
	}

	@Override
	protected int getDesiredChallenge() {
		return getDifficulty() * depth * 5;
	}

}
