package com.bwj.aage.map;

import java.util.HashMap;

/**
 *
 */
public class DwarvenVillageMap extends VillageMap {

	public DwarvenVillageMap(long seed, long parentSeed) {
		super(seed, parentSeed);
	}

	@Override
	public void generate(HashMap<String, Integer> param) {
		CaveMap base = new CaveMap(getSeed());
		base.generate(null);
		terrain = base.getTerrain();
		super.generate(param);

	}
}
