package com.bwj.aage.map;

import com.bwj.aage.Point;
import com.bwj.aage.Util;

import java.io.Serializable;

/**
 * A house, represented on the map as a rectangle with a door.
 */
public class House implements Serializable {

	private int x, y, width, height;
	
	public House(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	/**
	 * Determines whether this house collides with another house. We give a couple
	 * of tiles worth of space between the houses for aesthetic purposes.
	 * @param other
	 * @return
	 */
	public boolean collidesWith(House other) {
		return x + width + 2 >= other.x &&
				y + height + 2 >= other.y &&
				x <= other.x + other.width + 2 &&
				y <= other.y + other.height + 2;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Determines whether p is INSIDE THE HOUSE.
	 * @param p Point to test
	 * @return True if p is inside the house.
	 */
	public boolean contains(Point p) {
		return (p.getX() >= x && p.getY() >= y && p.getX() < x + width && p.getY() < y + height);
	}

	/**
	 * Finds a point within the house for AI navigation.
	 * @return A random point inside the house.
	 */
	public Point getRandomPointInside() {
		return new Point(Util.rand(x, x + width - 1), Util.rand(y, y + height - 1));
	}

	/**
	 * @return The number of people this house is capable of holding.
	 */
	public int getPopulation() {
		final int i = width * height / 16;
		return i > 0 ? i : 1;
	}
}
