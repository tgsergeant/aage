package com.bwj.aage.map;

import com.bwj.aage.Point;
import com.bwj.aage.Tile;
import com.bwj.aage.Util;

import java.util.HashMap;
import java.util.Random;

/**
 * A map for a country village. Contains enough houses to hold around 10 people, arranged randomly on the map.
 */
public class VillageMap extends InhabitedMap {

	private long seed;

	public VillageMap(long seed, long parentSeed) {
		super(seed, 80, 21, parentSeed);
		this.seed = seed;
		fill(Tile.GrassTile);
	}

	@Override
	public void generate(HashMap<String, Integer> param) {
		Random r = new Random(seed);
		for (int i = 0; i < getWidth(); i++) {
			for (int j = 0; j < getHeight(); j++) {
				if(r.nextInt(100) < 3) {
					terrain[i][j]  = Tile.TreeTile;
				}
			}
		}
		int desiredPopulation = Util.randomInRange(6, 13, r);
		while (population < desiredPopulation) {
			//Create a new house

			int width = Util.randomInRange(4, 9, r);
			int height = Util.randomInRange(4, 9, r);
			int x = Util.randomInRange(3, getWidth() - width - 3, r);
			int y = Util.randomInRange(3, getHeight() - height - 3, r);
			//System.out.println(x + ", " + y + ", " + width + ", " + height);
			House house = new House(x, y, width, height);
			int occupants = house.getPopulation();
			boolean collision = false;

			for (House h : houses) {
				if (house.collidesWith(h)) {
					collision = true;
					break;
				}
			}
			if(!collision) {
				// Go ahead and create the house
				//First choose a direction for the door
				int direction = r.nextInt(4);
				//Then fill in the walls and floor
				fillHouse(house, direction);

				//Finally, add the house to the list
				addHouse(house);
			}
		}

		populateMap(r);
	}

	@Override
	public Point getStartingPosition() {
		return new Point(1,1);
	}

}
