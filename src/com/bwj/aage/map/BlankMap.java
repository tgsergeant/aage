package com.bwj.aage.map;

import java.util.ArrayList;
import java.util.HashMap;

import com.bwj.aage.Map;
import com.bwj.aage.Point;
import com.bwj.aage.Tile;
import com.bwj.aage.object.UpStair;

public class BlankMap extends Map {

    public BlankMap(int seed) {
        super(seed, 80, 21);
    }

    @Override
    public void generate(HashMap<String, Integer> param) {
        fill(Tile.GroundTile);
        //objects.add(new UpStair(getWidth() / 2, getHeight() / 2, parent));
    }

    @Override
    /**
     * The player begins by default in the location of the first UpStair element.
     */
    public Point getStartingPosition() {
        ArrayList<UpStair> s = getObjects(UpStair.class);
        if(s.size() > 0) {
            int x = s.get(0).getX();
            int y = s.get(0).getY();

            return new Point(x,y);
        } else {
            return new Point(0,0);
        }
    }

}
