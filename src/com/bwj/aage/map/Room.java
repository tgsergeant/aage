package com.bwj.aage.map;


import java.io.Serializable;

public class Room implements Serializable {

    public int x, y, width, height;

    private boolean startingRoom = false;

    private boolean corridor = false;

    boolean connected = false;

    public Room(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

		if (width == 1 || height == 1) {
			corridor = true;
		}
    }

    public void setStartingRoom(boolean startingRoom) {
        this.startingRoom = startingRoom;
    }

    public boolean isStartingRoom() {
        return startingRoom;
    }

    public void setCorridor(boolean corridor) {
        this.corridor = corridor;
    }

    public boolean isCorridor() {
        return corridor;
    }

}
