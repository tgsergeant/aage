package com.bwj.aage;

/**
 * Represents the different visibility states. Invisible tiles have never been
 * visited and hence cannot be seen. Visited tiles have been seen in the past.
 * The player thus knows the layout and location of items in these tiles.
 * Visible tiles are currently visible, and hence the player can see all
 * mapobjects at this location.
 * @author Tim
 *
 */
public enum Visibility {
INVISBLE, VISITED, VISIBLE
}
