package com.bwj.aage;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class ErrorReportDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField emailField;
    private JTextArea descriptionField;
    private JTextArea stackTraceArea;
	
	private String errorName = null;

    public ErrorReportDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

		showing = true;
    }

    private void onOK() {
        final String email = emailField.getText();
        final String description = descriptionField.getText();
        final String error = stackTraceArea.getText();
		final String subject = errorName;

        //Run in background
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://aage.room342.net/reportError");
                    //Set up data
                    String data = "";
                    if (!"".equals(email)) {
                        data += "email=" + URLEncoder.encode(email, "UTF-8") + "&";
                    }
                    if (!"".equals(description)) {
                        data += "description=" + URLEncoder.encode(description, "UTF-8") + "&";
                    }
                    data += "error=" + URLEncoder.encode(error, "UTF-8") + "&";
					data += "subject=" + URLEncoder.encode(subject, "UTF-8");
                    URLConnection connection = url.openConnection();
                    connection.setDoOutput(true);

                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(data);
                    writer.flush();

                    // Get the response
                    BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line = rd.readLine();
                    if ("1".equals(line)) {
                        MessageManager.addMessage("Error report sent successfully. Thanks!");
                    }
                    writer.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        //Dispose of the window
        dispose();
		showing = false;
    }

    private void onCancel() {
        dispose();
		showing = false;
    }

    public void setError(Throwable e) {

		Writer result = new StringWriter();
		PrintWriter writer = new PrintWriter(result);
		e.printStackTrace(writer);
		stackTraceArea.setText(result.toString());
		errorName = e.toString();
	}

	private boolean showing = false;
	/**
	 * Determines whether the error dialog is currently open
	 * @return True if the dialog is currently visible
	 */
	public boolean isShowing() {
		return showing;
	}
}
