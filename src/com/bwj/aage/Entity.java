package com.bwj.aage;

import com.bwj.aage.config.CharacterClass;
import com.bwj.aage.config.Race;
import com.bwj.aage.entity.StatusCondition;
import com.bwj.aage.entity.StatusConditionList;

import java.util.ArrayList;


/**
 * Represents any object drawn to the map which has intelligence. All entities are drawn
 * above other layers of the map. All entities are given the opportunity to update while
 * input is being processed.
 *
 * @author Tim
 */
public abstract class Entity extends MapObject {

    public int getExperience() {
        return experience;
    }

    private int experience = 0;

    private int level = 1;

    private transient VisibilityMap visibilityMap = null;

    private Race race;


    private CharacterClass characterClass;
    private AttributeSet attributeSet = new AttributeSet(0);
    protected String name;

	public StatusConditionList getConditionList() {
		return conditionList;
	}

	private StatusConditionList conditionList = new StatusConditionList();

    public AttributeSet getAttributeSet() {
        return attributeSet;
    }

    public void setAttributeSet(AttributeSet attributeSet) {
        this.attributeSet = attributeSet;
    }

	public void addStatusCondition(StatusCondition condition) {
		attributeSet.getAttribute(condition.getAttribute()).modifyValue(condition.getDelta());
		conditionList.addCondition(condition);
	}

    /**
     * Updates the entity be recalculating the visibility map. Subclasses will want to add
     * additional actions.
     */
    public void update() {
        int radius = getVisibilityRadius();
        getVisibilityMap().update(x, y, radius);
		conditionList.update(this); //Updates status conditions so they can wear off
    }

    public int getVisibilityRadius() {
        return 7 + attributeSet.getAttribute(Attribute.PERCEPTION).getModifier() / 2;
    }

    /**
     * The map which this entity is located on
     */
    protected Map currentMap = null;

    private boolean isDead = false;

    public Map getMap() {
        return currentMap;
    }

    public void changeHealth(int delta) {
        health.modifyValue(delta);
        if (getHealthValue() <= 0) {
            isDead = true;
        }
    }

    private LimitedAttribute health;

    public LimitedAttribute getHealth() {
        return health;
    }

    public int getHealthValue() {
        return health.getValue();
    }

    /**
     * Constructs a new entity on the given map with a specific position.
     *
     * @param x Position of the new entity
     * @param y Position of the new entity
     * @param t Tile which represents the entity
     * @param m Map on which the entity is located
     * @param r Race of the new entity
     */
    public Entity(int x, int y, Tile t, Map m, Race r, CharacterClass cl, String name) {
        super(x, y, t);
        this.currentMap = m;
        setVisibilityMap(new VisibilityMap(m));

        // Race related
        this.race = r;
        characterClass = cl;
        attributeSet.mergeWith(r.getAttributeSet());
        attributeSet.mergeWith(cl.getAttributeModifiers()); //Add class bonuses

        //Calculate derived attributes
        health = new LimitedAttribute(15 + attributeSet.getAttributeValue(Attribute.ENDURANCE) / 2, "Health");
        // TODO setMana(20 + intelligence);

        //Update visibility
        visibilityMap.update(x, y, getVisibilityRadius());
        this.name = name;
    }

    /**
     * Moves the entity one step in the specified direction.
     * Performs a passability check to prevent illegal moves.
     *
     * @param dir Direction in which to move
     */
    // TODO Rewrite this method to be more thorough, use deltax and deltay, check for other entities, etc.
    public boolean move(Direction dir) {
        int deltax = 0;
        int deltay = 0;

        switch (dir) {
            case NORTH:
                deltay = -1;
                break;
            case SOUTH:
                deltay = 1;
                break;
            case EAST:
                deltax = 1;
                break;
            case WEST:
                deltax = -1;
                break;
            case NORTHEAST:
                deltay = -1;
                deltax = 1;
                break;
            case NORTHWEST:
                deltay = -1;
                deltax = -1;
                break;
            case SOUTHEAST:
                deltay = 1;
                deltax = 1;
                break;
            case SOUTHWEST:
                deltay = 1;
                deltax = -1;
                break;
        }
        return move(deltax, deltay);
    }

    /**
     * Moves the entity by the specified amount in the x and y directions
     *
     * @param deltax Amount to move in x direction
     * @param deltay Amount to move in y direction
     * @return True if the move was blocked by terrain or another entity
     */
    public boolean move(int deltax, int deltay) {
        Tile tileTo = currentMap.getTile(x + deltax, y + deltay);
        if (tileTo == null) return false;
        //Check that the desired tile is passable to
        boolean blocking = tileTo.blocksMovement();
        if (!blocking) {
            //See if the tile is occupied.
            int desiredX = x + deltax;
            int desiredY = y + deltay;
            ArrayList<MapObject> objsAt = currentMap.getObjects(desiredX, desiredY);
            for (MapObject obj : objsAt) {
				if (obj.blocksMovement()) {
					return false;
				}
			}
            x += deltax;
            y += deltay;
            return true;
        }
        return false;
    }

    /**
     * Adds to the entity's experience level. Will also prompt a level-up, if experience
     * breaches the required threshold.
     *
     * @param toAdd Amount of experience to add
     */
    public void addExperience(int toAdd) {
        experience += toAdd;

        if (experience >= ExperienceTable.getExperience((level + 1))) {
            notifyLevelUp(level + 1);
        }
    }

    /**
     * Tells the object that it is ready to be levelled up. The level up can either be performed immediately
     * or at a later time.
     */
    protected abstract void notifyLevelUp(int level);


    public void levelUp() {
		if (getExperience() < ExperienceTable.getExperience(level + 1)) {
			return;
		}
		level += 1;
		int primaryToLevel = selectAttributeToIncrease(true);
		if (getCharacterClass().isPrimaryAttribute(primaryToLevel)) {
			attributeSet.getAttribute(primaryToLevel).modifyDefault(1);
		} else {
			System.err.println("Selected an attribute to level up which is not a primary attribute");
		}
		int attToLevel = selectAttributeToIncrease(false);
		attributeSet.getAttribute(attToLevel).modifyDefault(1);
		health.modifyDefault(attributeSet.getAttribute(Attribute.ENDURANCE).getModifier() + 5);
	}

    /**
     * Selects the attribute which should be increased due to level up
     *
     * @param primary Whether a primary attribute needs to be selected
     * @return The attribute to increase.
     */
    protected abstract int selectAttributeToIncrease(boolean primary);

    public int getLevel() {
        return level;
    }

    /**
     * @return The percentage (out of 100) of progress to the next level.
     */
    public int getLevelProgress() {
        return (int) ((((double) experience - ExperienceTable.getExperience(level)) / (ExperienceTable.getExperience(level + 1) - ExperienceTable.getExperience(level))) * 100);
    }

    public Race getRace() {
        return race;
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public VisibilityMap getVisibilityMap() {
		if(visibilityMap == null) {
			visibilityMap = new VisibilityMap(currentMap);
			visibilityMap.update(x, y, getVisibilityRadius());
		}
        return visibilityMap;
    }

    public void setVisibilityMap(VisibilityMap visibilityMap) {
        this.visibilityMap = visibilityMap;
    }

	/**
	 * @return The current position of the entity as an instance of Point.
	 */
	public Point getPositionAsPoint() {
		return new Point(x, y);
	}

    @Override
    public boolean blocksMovement() {
        return true;
    }

    public void setLevel(int levelTo) {
        addExperience(ExperienceTable.getExperience(levelTo) - experience);
    }

    /**
     * Launches an attack on the other entity, attempting to damage it.
     *
     * @param other The entity to attack
     */
    public void attack(Entity other) {
        // TODO Make entities attack when they are being attacked.
        int attackModifier = 2 + attributeSet.getAttribute(Attribute.STRENGTH).getModifier();
        int attackRoll = (int) (Math.random() * 20);

        int defenseModifier = other.getDefenseModifier();

        if (attackModifier + attackRoll >= defenseModifier) {
            int damage = (int) (Math.random() * attributeSet.getAttribute(Attribute.STRENGTH).getModifier() + 4) + 1; // TODO Better damage
            other.changeHealth(-damage);
            if (other.isDead()) {
                MessageManager.addMessage(toString() + " killed " + other.toString() + ". ", getX(), getY());
                addExperience((int) ((Util.log2(other.getLevel()) + 1) * other.getCharacterClass().getDifficultyModifier() * 12));
            } else {
                MessageManager.addMessage(toString() + " hit " + other.toString() + " for " + damage + " damage. ", getX(), getY());
            }
        } else {
            MessageManager.addMessage(toString() + " attacked " + other.toString() + " but missed. ", getX(), getY());
        }
        other.notifyAttackedBy(this);
    }

    /**
     * Called when this entity is attacked by another entity, allowing the AI to retaliate.
     * @param entity Entity which has attacked us
     */
    protected abstract void notifyAttackedBy(Entity entity);

    public int getDefenseModifier() {
        return 10 + attributeSet.getAttribute(Attribute.AGILITY).getModifier(); // TODO Armor?
    }

    public boolean isDead() {
        return isDead;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
		this.name = Util.capitalize(name);
	}

	@Override
	public String getDescription(boolean showFull, Player p) {
		return name + ", a " + race.getName() + " " + characterClass.getName();
	}

	/**
	 * Called when a status condition is removed from the entity. Used to notify the player of improvements
	 * to their health
	 * @param status The condition that was removed.
	 */
	public void notifyConditionRemoved(StatusCondition status) {
		//Do nothing
	}


}
