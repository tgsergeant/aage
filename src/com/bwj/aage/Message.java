package com.bwj.aage;

/**
 * Represents a message which is presented to the player. Each message records its source,
 * so that the player will only see it if it is in range.
 * @author Tim
 *
 */
public class Message {
    
    public Message(String message, int xsource, int ysource, int duration) {
        this.xsource = xsource;
        this.ysource = ysource;
        this.message = message.substring(0, 1).toUpperCase() + message.substring(1);
		this.duration = duration;
    }

    private int xsource, ysource;
    
    private String message;
	
	private int duration;

    public int getXsource() {
        return xsource;
    }

    public int getYsource() {
        return ysource;
    }

    public String getMessage() {
        return message;
    }

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
}
