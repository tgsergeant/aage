package com.bwj.aage;

/**
 * Specifies a direction for movement.
 * @author Tim
 *
 */
public enum Direction {

    NORTH, NORTHEAST, EAST, SOUTHEAST, SOUTH, SOUTHWEST, WEST, NORTHWEST
}
