package com.bwj.aage;

/**
 * A single node visited by the pathfinding algorithm.
 * @author Tim
 *
 */
public class PathfindingNode {

    int x = 0;

    int y = 0;

    /** The gcost is the cost of visiting all nodes leading up to, and including this node */
    int gcost = 0;
    /** The hcost is the estimated cost of reaching the exit after this node */
    int hcost = 0;
    
    private Point endingPoint;
    private PathfindingNode parent;

    public PathfindingNode getParent() {
        return parent;
    }

    /**
     * Creates a new node in the path
     * @param thisPoint The location of this point on the map
     * @param endingPoint The desired ending point
     * @param parent The parent node to this point.
     */
    public PathfindingNode(Point thisPoint,  Point endingPoint, PathfindingNode parent) {
        x = thisPoint.getX();
        y = thisPoint.getY();
        
        this.endingPoint = endingPoint;
        setParent(parent);

        
        hcost = Math.abs(x - endingPoint.getX()) + Math.abs(y - endingPoint.getY()) * 10;
    }

    /**
     * @return The total cost of this node.
     */
    public int getCost() {
        return gcost + hcost;
    }
    
    /**
     * Sets the parent and updates the g-cost accordingly.
     * @param parent The new parent of this node
     */
    public void setParent(PathfindingNode parent) {
        this.parent = parent;
        if(parent != null) {
            gcost = parent.gcost;
            if(Math.abs(x - endingPoint.getX()) == 1 && 
                    Math.abs(y - endingPoint.getY()) == 1) {
                gcost += 10;
            }
            else {
                gcost += 10;
            }
        }
    }
}
