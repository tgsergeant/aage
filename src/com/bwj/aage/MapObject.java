package com.bwj.aage;

import java.io.Serializable;

/**
 * The second level of characters which are drawn to the screen, a MapObject represents
 * anything which can be interacted with by the player, but which does not have an
 * intelligence of its own. For example, doors and items.
 * @author Tim
 *
 */
// TODO: Are traps a part of this? Are we even going to have traps?

public abstract class MapObject implements Serializable {

    private static final long serialVersionUID = 7227376691878967318L;

    protected Tile tile;

    protected int x = 0;

    protected int y = 0;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public MapObject() {

    }

    /**
     * Creates a new MapObject at the given position.
     * @param x Position of the new object
     * @param y Position of the new object
     */
    public MapObject(int x, int y) {
        this.x = x;
        this.y = y;
        tile = Tile.BlankTile;
    }

    /**
     * Creates a new object at the given position, represented by the
     * given tile.
     * @param x Position of the new object
     * @param y Position of the new object
     * @param t Tile which represents the object
     */
    public MapObject(int x, int y, Tile t) {
        this.x = x;
        this.y = y;
        tile = t;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public Tile getTile() {
        return tile;
    }

    public boolean blocksMovement() {
        return false;
    }

	public boolean blocksSight() {
		return false;
	}

	/**
	 * Determines whether the player can act upon this object
	 * by pressing the given key. Used to search for targets
	 * for interactions
	 *
	 * @param keyCode CharKey code for the pressed key, or -1 if walked into
	 * @param player
	 * @return True if that key is a valid interaction
	 */
	public boolean canInteract(int keyCode, Player player) {
		return false;
	}

	/**
	 * Performs the interaction specified by the given keycode. This will only
	 * be called with codes for which canInteract returned true.
	 * @param keyCode
	 * @param player
	 */
	public void interact(int keyCode, Player player) {
		return;
	}

	/**
	 * Gives a detailed description of the object, used when the player is hovering over it
	 * @return
	 * @param showFull
	 * @param p
	 */
	public abstract String getDescription(boolean showFull, Player p);
}
