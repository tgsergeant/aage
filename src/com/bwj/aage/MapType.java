package com.bwj.aage;

/**
 * Represents the type of map a map object represents.
 * @author Tim
 *
 */
public enum MapType {
    CAVE, DUNGEON, WORLD
}
