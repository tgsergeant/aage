package com.bwj.aage;

import com.bwj.aage.entity.Monster;
import com.bwj.aage.map.CaveMap;
import com.bwj.aage.map.DungeonMap;
import com.bwj.aage.map.WorldMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Represents a space which is explorable by the player. It contains a collection of tiles
 * representing the floor of the space, and lists of MapObjects and Entities.
 * @author Tim
 *
 */
public abstract class Map implements Serializable {


    private static final long serialVersionUID = 2595103337063343813L;
    public long getSeed() {
        return seed;
    }

    private String name;

    private long seed;

    /** The type of the map */
    public MapType type = null;

    /** A list of all the terrain within the map which is filled out at generation */
    protected Tile[][] terrain;

    protected ArrayList<MapObject> objects = new ArrayList<MapObject>();


    public ArrayList<MapObject> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<MapObject> objects) {
        this.objects = objects;
    }

    private int height;

    private int width;
    
    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
    
    /**
     * Changes the size of the map and fills it with blank tiles.
     * @param w New width
     * @param h New height
     */
    public void setSize(int w, int h) {
        this.width = w;
        this.height = h;
        terrain = new Tile[width][height];
        fill(Tile.BlankTile);
    }

    /**
     * Creates a blank map with the given dimensions
	 * @param seed Seed from which the map is generated
	 * @param width Width of the map
	 * @param height Height of the map
	 */
    public Map(long seed, int width, int height) {
        setSize(width, height);
        this.seed = seed;
    }

    /**
     * Creates a map with the specified dimensions.
     * @param width The width of the desired map.
     * @param height The height of the desired map.
     */
    public Map(int width, int height) {
        setSize(width, height);
    }

    /**
     * Generates a new map using the seed specified in the constructor.
	 * @param param Dictionary of parameters used to generate the map
	 */
    public abstract void generate(HashMap<String, Integer> param);

    /**
     * Fills the map with blank tiles in order to avoid NullPointerExceptions.
     * @param fillTile TODO
     */
    protected void fill(Tile fillTile) {
        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                terrain[x][y] = fillTile;
            }
        }
    }

    /**
     * Updates the map by updating each entity in the map.
     */
    public void update() {

        for(MapObject obj : objects) {
            if (obj instanceof Entity) {
				if(!((Entity) obj).isDead()) {
                	((Entity)obj).update();
				}
            }
        }
        ArrayList<MapObject> dead = new ArrayList<MapObject>();
        for(MapObject obj : objects) {
            if (obj instanceof Entity) {
                if(((Entity)obj).isDead()) {
                    dead.add(obj);
                    //System.out.println(obj+ " is dead");
                }
            }
        }
        objects.removeAll(dead);
    }

    /**
     * Returns a list of all the MapObjects at a given x and y position.
     * @param x Position to search
     * @param y Position to search
     * @return All objects at that position
     */
    public ArrayList<MapObject> getObjects(int x, int y) {
        ArrayList<MapObject> foundObjects = new ArrayList<MapObject>();
        for(MapObject obj : objects) {
            if(obj.getX() == x && obj.getY() == y) {
                foundObjects.add(obj);
            }
        }
        return foundObjects;
    }

    // TODO This isn't working
    /**
     * Filters through the list of objects to return those which are of the
     * given class
     * @param t Class to search
     * @return A list of all objects which are a subclass of t
     */
    public <T extends MapObject> ArrayList<T> getObjects(Class<T> t) {
        ArrayList<T> foundObjects = new ArrayList<T>();
        for(MapObject obj : objects) {
            if(subclassOf(obj.getClass(), t)) {
                foundObjects.add((T) obj);
            }
        }
        return foundObjects;
    }
    
    private <T extends MapObject> boolean subclassOf(Class obj, Class<T> t) {
        if(obj.equals(t)) {
            return true;
        }
        Class<T> superClass = (Class<T>) obj.getSuperclass();
        if(!superClass.equals(MapObject.class)) {
            return subclassOf(superClass, t);
        }
        return false;
    }

    /**
     * @return The position where the player should begin on the map when they first enter it.
     */
    public abstract Point getStartingPosition();

    /**
     * Returns the tile at the desired position. Will return null if the
     * specified point is out the map's boundaries.
     * @param x Position of the tile
     * @param y Position of the tile
     * @return Tile at this position, or null if it is out of bounds
     */
    public Tile getTile(int x, int y) {
        if(x < 0 || y < 0 || x >= width || y >= height) {
            return null;
        }
        return terrain[x][y];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    /**
     * Finds a path between the given starting and ending points. Uses an
     * A* method to find the most efficient path.
     * @param start Point at which to start pathfinding
     * @param end Point where pathfinding should end
     * @return A list of points which form a short path between start and end
     */
    public ArrayList<Point> getPath(Point start, Point end) {
        ArrayList<PathfindingNode> openList = new ArrayList<PathfindingNode>();
        ArrayList<PathfindingNode> closedList = new ArrayList<PathfindingNode>();
		byte[][] status = new byte[width][height];
		
        //Add the starting point to the open list
        PathfindingNode starting = new PathfindingNode(start, end, null);
        openList.add(starting);
		status[start.getX()][start.getY()] = 1;

        while(true) {
            //Get the lowest cost node from the open list
            PathfindingNode lowest = openList.get(0);
            for(PathfindingNode node : openList) {
                if(node.getCost() < lowest.getCost()) {
                    lowest = node;
                }
            }
            //Remove it from the open list and add it to the closed list
            openList.remove(lowest);
            closedList.add(lowest);
			status[lowest.x][lowest.y] = 2;
            //If lowest is the final point, break and construct the path
            if(lowest.x == end.getX() && lowest.y == end.getY()) {
                return makePath(closedList.get(closedList.size() - 1));
            }
            
            //For each of the adjacent squares, consider adding it to the open list
            for(int x = lowest.x - 1; x <= lowest.x + 1; x++) {
                for(int y = lowest.y - 1; y <= lowest.y +  1; y++) {
                    //Ignore the centre tile
                    if(x == lowest.x && y == lowest.y){
                        continue;
                    }
                    if(x == end.getX() && y == end.getY()) {
                        PathfindingNode adjNode = new PathfindingNode(new Point(x, y), end, lowest);
                        return makePath(adjNode);
                    }
                    //If it is not passable or is already closed, ignore it.
                    if(getTile(x, y) == null || isTileBlocked(x, y)) {
                        continue;
					}
                    if(status[x][y] == 2) {
                        continue;
                    }
                    //If it is on the open list, compare this path to the existing path
					if(status[x][y] == 1) {
						for(PathfindingNode n : openList) {
							if(n.x == x && n.y == y) {
								if (lowest.gcost + 10 < n.gcost) {
									//If it is better, change n's parent
									n.setParent(lowest);
								}
							}
						}
						continue;
					}
                    //Otherwise, add it to the open list
                    PathfindingNode adjNode = new PathfindingNode(new Point(x, y), end, lowest);
                    openList.add(adjNode);
					status[x][y] = 1;

                }
            }
            //If the list is empty, there is no possible path
            if(openList.isEmpty()) {
                return null;
            }
        }
    }

    public boolean isTileBlocked(int x, int y) {
		if (getTile(x, y).blocksMovement()) {
			return true;
		}
		for (MapObject o : objects) {
			if (o.getX() == x && o.getY() == y && o.blocksMovement()) {
				return true;
			}
		}
		return false;
	}

    /**
     * Recursively constructs a path by working backwards to find the starting
     * point and adding points to this.
     * @param node The final node of the path
     * @return A path starting at the ancestor and tracing its way to node.
     */
    private ArrayList<Point> makePath(PathfindingNode node) {
        Point toAdd = new Point(node.x, node.y);
        if(node.getParent() == null) {
            //Ignores the first point
            return new ArrayList<Point>();
        }
        else {
            ArrayList<Point> path = makePath(node.getParent());
            path.add(toAdd);
            return path;
        }
    }
    
    /**
     * Generates and returns a new map.
     *
     * @param type The type of map to be created
     * @param seed The seed for the map
     * @param player The player responsible for this.
     * @return A generated map.
     */
    public static Map create(MapType type, long seed, Player player) {
      //Generate the new Map
        Map newMap = null;
        if(type == MapType.DUNGEON) {
            newMap = new DungeonMap(seed);
        } 
        else if(type == MapType.CAVE) {
            newMap = new CaveMap(seed); // TODO Nextseed
        } else if(type == MapType.WORLD) {
            newMap = new WorldMap(seed);
        }
        newMap.generate(null);
        //Miscellaneous things to do
        //newMap.objects.add(player);
        
        return newMap;
    }


    
    public int getChallengeLevel() {
        ArrayList<Monster> monsters = getObjects(Monster.class);
        int challenge = 0;
        for(Monster monster : monsters) {
            challenge += monster.getLevel() * monster.getCharacterClass().getDifficultyModifier();
        }
        return challenge;
    }
    
    /**
     * Gets the given property as an int from the Properties.
     * Returns 0 if the property does not exist or is not a
     * string.
     * @param string Name of the property
     * @param param Dictionary of parameters
     * @return The value of the property
     */
    protected static int getProp(String string, HashMap<String, Integer> param) {
		try {
			return param.get(string);
		}catch(Exception e) {
			return 0;
		}
		
	}
	
	
	public void printMap() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < height; i++) {
			String line = "";
			for (int j = 0; j < width; j++) {
				line += getTile(j, i).getCharacter();
			}
			builder.append(line + "\n");
		}
		System.out.println(builder);
	}

	protected void fillArea(int startx, int starty, int width, int height, Tile tile) {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				terrain[startx + i][starty + j] = tile;
			}
		}
	}


	public String getTileDescriptor(Player p, int x, int y, boolean showFull) {
		String tileText = "";
		if(showFull) {
			tileText = getTile(x, y).getDescription() + ". ";
		}
		final ArrayList<MapObject> objects = getObjects(x, y);

		boolean objectAdded = false;
		String objText = "";
		if (showFull) {
			objText = "Things here: ";
		}

		if (objects.size() != 0) {

			for (int i = 0; i < objects.size(); i++) {
				MapObject obj = objects.get(i);
				if (obj instanceof Entity && p.getVisibilityMap().getVisibility(x, y) == Visibility.VISITED) {
					continue; //Ignore entities who we can't actually see.
				}
				if (obj instanceof Player) {
					continue;
				}
				objText += obj.getDescription(showFull, p);
				objectAdded = true;
				if (i < objects.size() - 1) { //Comma when there is more than one object
					objText += ", ";
				}
			}
		}
		if(objText.endsWith(", ")) {
			objText = objText.substring(0, objText.length() - 2);
		}
		if(objectAdded) {
			tileText += objText;
		}
		return tileText;
	}
}
