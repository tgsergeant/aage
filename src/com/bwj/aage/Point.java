package com.bwj.aage;

import java.io.Serializable;

/**
 * A convenience class which represents the position of an object on a 2D plane.
 * @author Tim
 *
 */
public class Point implements Serializable {

	private int x;
	
	private int y;
	
	/**
	 * Creates a new Dimension located at (0, 0)
	 */
	public Point() {
		setX(0);
		setY(0);
	}
	/**
	 * Creates a new Dimension with the given coordinates
	 * @param x The X coordinate of the dimension
	 * @param y The y coordinate of the dimension
	 */
	public Point(int x, int y) {
		this.setX(x);
		this.setY(y);
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getX() {
		return x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getY() {
		return y;
	}

    /**
     * Determines the direction of 'other' from *this* point
     * @param other Point to test
     * @return Direction of other
     */
    public Direction getRelativeDirection(Point other) {
        int deltaX = other.getX() - getX();
        int deltaY = other.getY() - getY();

        double a = Math.atan2(deltaY, deltaX) * 180 / Math.PI;
        if (a <= 22.5 && a > -22.5) {
            return Direction.EAST;
        }
        if (a <= 67.5 && a > 22.5) {
            return Direction.NORTHEAST;
        }
        if (a <= 112.5 && a > 67.5) {
            return Direction.NORTH;
        }
        if (a <= 157.5 && a > 112.5) {
            return Direction.NORTHWEST;
        }
        if (a > 157.5) {
            return Direction.WEST;
        }
        if (a >= -67.5 && a < -22.5) {
            return Direction.SOUTHEAST;
        }
        if (a >= -112.5 && a < -67.5) {
            return Direction.SOUTH;
        }
        if (a >= -157.5 && a < -112.5) {
            return Direction.SOUTHWEST;
        }
        if (a < -157.5) {
            return Direction.WEST;
        }

        return null;
    }
}
