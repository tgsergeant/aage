package com.bwj.aage;

import java.io.Serializable;


public class VisibilityMap implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4767687579894800985L;

	public Visibility[][] visibilityMap;

	private Map referencedMap;

	public VisibilityMap(Map m) {
		referencedMap = m;
		visibilityMap = new Visibility[m.getWidth()][m.getHeight()];
		for(int i = 0; i < m.getWidth(); i++) {
			for(int j=0; j<m.getHeight(); j++) {
				visibilityMap[i][j] = Visibility.INVISBLE; //Debugging
			}
		}
	}

	public Visibility getVisibility(int x, int y) {
		if(x < 0 || y < 0 || x >= referencedMap.getWidth() || y >= referencedMap.getHeight())
			return Visibility.INVISBLE;
		return visibilityMap[x][y];
	}

	public void setVisibility(int x, int y, Visibility v) {
		visibilityMap[x][y] = v;
	}


	/**
	 * Recursive function for the calculation of FOV. Uses shadowcasting
	 * @param cx X-coordinate of the entity
	 * @param cy Y-coordinate of the entity
	 * @param row The number of rows above the entity that the algorithm is scanning
	 * @param start The starting slope of the area to be scanned
	 * @param end The ending slope of the area to be scanned.
	 * @param radius The distance away from (cx, cy) to be scanned
	 * @param xx Multiplier used to translate coordinates between octants
	 * @param xy Multiplier used to translate coordinates between octants
	 * @param yx Multiplier used to translate coordinates between octants
	 * @param yy Multiplier used to translate coordinates between octants
	 */
	private void castLight(int cx, int cy, int row, double start, double end, int radius,
						   int xx, int xy, int yx, int yy) {
		if (start < end) {
			return;
		}
		for(int j = row; j < row + radius + 1; j++) {
			int dx = -j-1;
			int dy = -j;
			boolean blocked = false;
			double newStart = 0;
			while (dx <= 0) {
				dx += 1;
				//Translate dx and dy into map coordinates
				int x = cx + dx * xx + dy * xy;
				int y = cy + dx * yx + dy * yy;
				//Calculate the slope of the left and right extremities of the square being considered
				double lSlope = (dx - 0.5) / (dy + 0.5);
				double rSlope = (dx + 0.5) / (dy - 0.5);

				if(start < rSlope) {
					continue;
				}
				else if(end > lSlope) {
					break;
				}
				else {

					//Light the square if it is within the search radius
					if(dx * dx + dy * dy < radius * radius) {
						if(x >= 0 && y >= 0 && x < referencedMap.getWidth() && y < referencedMap.getHeight())
							visibilityMap[x][y] = Visibility.VISIBLE;
					}
					if(blocked) {
						//Scanning a row of blocked squares, we want to find a break
						if(blocked(x,y)) {
							newStart = rSlope;
						}
						else {
							blocked = false;
							start = newStart;
						}
					}
					else {
						if(blocked(x,y) && j < radius) {
							//Start a child scan
							blocked = true;
							castLight(cx, cy, j+1, start, lSlope, radius, xx, xy, yx, yy);
							newStart = rSlope;
						}
					}
				}
			}
			if (blocked) {
				break;
			}
		}

	}

	/**
	 * Tests to see whether a given point is blocked within the current map
	 * @param x Position to test
	 * @param y position to test
	 * @return True if the tile is blocked
	 */
	private boolean blocked(int x, int y) {
		if(x >= 0 && y >= 0 && x < referencedMap.getWidth() && y < referencedMap.getHeight() && referencedMap.terrain[x][y].blocksSight()) {
			return true;
		}
		for (MapObject object : referencedMap.getObjects()) {
			if (object.getX() == x && object.getY() == y && object.blocksSight()) {
				return true;
			}
		}
		return false;
	}

	/** Multiplexer used to translate octant coordinates into real coordinates */
	int[][] mult = {{ 1, 0, 0, -1, -1, 0, 0, 1 },
			{ 0, 1, -1, 0, 0, -1, 1, 0 },
			{ 0, 1, 1, 0, 0, -1, -1, 0 },
			{ 1, 0, 0, 1, -1, 0, 0, -1 } };

	/**
	 * Updates the visibility map. First changes all visible elements to visited. Then calls
	 * castLight to calculate which elements are currently visible.
	 * @param x Position to centre the update around
	 * @param y Position to centre the update around
	 * @param radius Radius of the update
	 */
	public void update(int x, int y, int radius) {
		// TODO Auto-generated method stub
		//Update visibility map
		for(int i = 0; i < referencedMap.getWidth(); i++) {
			for(int j = 0; j < referencedMap.getHeight(); j++) {
				if(visibilityMap[i][j] == Visibility.VISIBLE) {
					visibilityMap[i][j] = Visibility.VISITED;
				}
			}
		}
		visibilityMap[x][y] = Visibility.VISIBLE;
		for(int oct = 0; oct < 8; oct++) {
			castLight(x, y, 1, 1.0, 0.0, radius, mult[0][oct], mult[1][oct], mult[2][oct], mult[3][oct]);
		}
	}


}
