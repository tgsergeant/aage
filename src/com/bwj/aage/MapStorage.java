package com.bwj.aage;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Provides static methods for storing maps to disk and retrieving them from disk.
 */
public class MapStorage {

    /**
     * Creates a save folder with the given name
     * @param name Player name, creates saves/name/player.dat
     * @return False if the file could not be created.
     */
    public static boolean createSaveFile(String name) {
        File saveDir = new File("saves");
        if (!saveDir.exists()) {
            saveDir.mkdir();
        }
        File dir = new File("saves/" + name + "/");
        if (!dir.exists()) {
            dir.mkdir();
        }
        File saveFile = new File("saves/" + name + "/player.dat");
        try {
            return saveFile.createNewFile();
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Deletes all files associated with the given save game.
     * Ie, all files in the folder /saves/name/
     * @param name Name of the savegame to delete
     */
    public static void deleteSaveGame(String name) {
        File saveFile = new File("saves/" + name + "/");
        String[] list = saveFile.list();
        for(String file : list) {
            File toDel = new File(saveFile, file);
            toDel.delete();
        }
        saveFile.delete();
    }

    /**
     * Saves the given map to the disk using the file name given.
     * Stores the map in saves/name/seed.dat/seed (seed.dat is a zip
     * file with a single entry: seed)
     * @param name Name of the save folder to save to
     * @param map Map data to save
     */
    public static void saveMap(String name, StoredMap map) {
        try {
            ZipOutputStream zos = new ZipOutputStream(new FileOutputStream("saves/" + name + "/" + map.getMap().getSeed() + ".dat"));
            ZipEntry entry = new ZipEntry(Long.toString(map.getMap().getSeed()));
            zos.putNextEntry(entry);

            ObjectOutputStream oos = new ObjectOutputStream(zos);
            oos.writeObject(map);
            oos.close();
            zos.close();
            //System.out.println("Stored map: " + map.getMap().getSeed());
        } catch (FileNotFoundException e) {
            Util.reportError(e);
        } catch (IOException e) {
            Util.reportError(e);
        }


    }

    /**
     * Loads the map located at saves/name/seed.dat
     * @param name Name of the save folder to use
     * @param seed Seed to be loaded.
     * @return The map retrieved from this location
     */
    public static StoredMap loadMap(String name, long seed) {
        try {
            //System.out.println("Loading map: " + seed);
            ZipInputStream zis = new ZipInputStream(new FileInputStream("saves/" + name + "/" + seed + ".dat"));
            ZipEntry entry = zis.getNextEntry();

            if(entry == null) {
                System.err.println("Missing map entry: " + seed);
                return null;
            }
            ObjectInputStream ois = new ObjectInputStream(zis);
            StoredMap map = (StoredMap) ois.readObject();
            ois.close();
            zis.close();
            return map;
        } catch (FileNotFoundException e) {
            Util.reportError(e);
        } catch (IOException e) {
            Util.reportError(e);
        } catch (ClassNotFoundException e) {
            Util.reportError(e);
        }
        return null;
    }

    /**
     * Determines whether the given map exists on the disk
     * @param name Name of the save folder
     * @param seed Seed of the map to search for
     * @return True if this map file exists
     */
    public static boolean hasMap(String name, long seed) {
        File mapFile = new File("saves/" + name + "/" + seed + ".dat");
        return mapFile.exists();
    }
}
