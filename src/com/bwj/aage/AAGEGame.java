package com.bwj.aage;

import com.bwj.aage.config.ConfigManager;
import com.bwj.aage.screen.GameScreen;
import com.bwj.aage.screen.MenuScreen;
import com.bwj.aage.util.MarkovNameGenerator;
import net.slashie.libjcsi.wswing.WSwingConsoleInterface;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The main class for AAGE. Initialises the game window, handles input, updates and drawing.
 * @author Tim
 *
 */
public class AAGEGame {

	/** The player. One instance is used throughout the entire game cycle */
	private Player player;

	/** The width, in tiles, of the screen. */
	public static final int GAMEWIDTH = AAGEConfig.getWidth();
	/** The height, in tiles, of the screen */
	public static final int GAMEHEIGHT = AAGEConfig.getHeight();
	/** The maximum int that can be used for a seed */
	public static final long SEEDRANGE = Long.MAX_VALUE;

	/** The screen which is currently active (ie, being updated and rendered). */
	private GameScreen activeScreen;

	/**
	 * The main function. Starts a instance of AAGE.
	 * @param args Command line arguments (ignored)
	 */
	public static void main(String[] args) {
		AAGEGame game = new AAGEGame();
		game.run();
		System.exit(0);
	}

	/**
	 * Constructs a new instance by initialising a blank player and setting up the menu screen.
	 */
	public AAGEGame() {
		setPlayer(null);
		//Load config
		ConfigManager.load();
		MarkovNameGenerator.initialiseNames();
		((WSwingConsoleInterface) WindowManager.getWindow()).addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (player != null) {
					//getPlayer().save();
					// TODO Find a better way to autosave
				}
			}
		});
	}

	/**
	 * Runs the game by:
	 * <ol>
	 * <li>Awaiting a keystroke from the player
	 * <li>Updating all entities (including the player!) in the map according to their initiative
	 * <li>Redrawing the map and surrounding interface.
	 * </ol>
	 */
	private void run() {
		activeScreen = new MenuScreen(this);
		//player.visibilityMap.update();
		//getPlayer().getMap().redraw(window, getPlayer());
		WindowManager.getWindow().refresh();
		boolean needsquit = false;
		while (!needsquit) {
			//try {
				activeScreen.redraw();
				WindowManager.getWindow().refresh();
				needsquit = activeScreen.update();
			//} catch (Throwable e) {
			//	Util.reportError(e); // Not convinced about whether this is a good idea.
			//}
		}
	}


	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * Changes the active screen to a new screen.
	 * @param gameScreen New screen to be displayed
	 */
	public void changeScreen(GameScreen gameScreen) {
		WindowManager.getWindow().cls();
		activeScreen = gameScreen;

	}

	/**
	 * Displays a new window on top of the current window being displayed.
	 * This achieves a sort of 'modality' for dialog boxes. This window
	 * will continue to be displayed until toShow.update() returns false.
	 * @param toShow New window to show on top of the screen.
	 */
	public void showScreen(GameScreen toShow) {
		WindowManager.getWindow().cls();
		boolean needsquit = false;
		while (!needsquit) {
			try {
				toShow.redraw();
				WindowManager.getWindow().refresh();
				needsquit = toShow.update();
			} catch (Throwable e) {
				Util.reportError(e);
			}
		}
		WindowManager.getWindow().cls();
	}

	public GameScreen getScreen() {
		return activeScreen;
	}
}
