package com.bwj.aage;

import com.bwj.aage.screen.AAGEScreenshotListener;
import com.bwj.aage.screen.components.KeyListComponent;
import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.ConsoleSystemInterface;
import net.slashie.libjcsi.wswing.WSwingConsoleInterface;

import java.util.Properties;

public class WindowManager {
    
    private static WSwingConsoleInterface instance = null;

	private static AAGEScreenshotListener screenshotListener = null;
    
    protected WindowManager() {
        
    }
    
    public static ConsoleSystemInterface getWindow() {
        if(instance == null) {
            Properties prop = AAGEConfig.getProperties();
            instance = new WSwingConsoleInterface("AAGE", prop);
			screenshotListener = new AAGEScreenshotListener();
			instance.setScreenshotListener(screenshotListener, CharKey.F1);
        }
        return instance;
    }

    public static void printCentered(int y, String toPrint) {
        instance.print((AAGEConfig.getWidth() - toPrint.length()) / 2, y, toPrint);
    }

	
	public static KeyListComponent createKeyList() {
		return new KeyListComponent(instance, AAGEConfig.getHeight() - 2, AAGEConfig.getWidth());
	}


	public static void disposeAndCreate() {
		instance.dispose();
		instance = new WSwingConsoleInterface("AAGE", AAGEConfig.getProperties());
	}
}
