package com.bwj.aage.entity;

import com.bwj.aage.*;
import com.bwj.aage.config.CharacterClass;
import com.bwj.aage.config.Race;
import net.slashie.libjcsi.CharKey;

import java.util.ArrayList;

public class Monster extends AIEntity {


	/**
     * Enum representing the different stages of the AI.
     */
    enum MonsterAIState {
        IDLE, WALKING, ATTACKING
    }

    MonsterAIState currentState = MonsterAIState.IDLE;

	private transient Entity aimingTowards;
    
    public Entity getTarget() {
        return aimingTowards;
    }

    public Monster(int x, int y, Tile t, Map m, Race r, CharacterClass cl, String name) {
        super(x, y, t, m, r, cl, name);

    }

	/**
     * Retaliates to being attacked. If this monster is not currently attacking,
     * it will start attacking the given entity.
     * @param entity Entity which has attacked us
     */
    @Override
    protected void notifyAttackedBy(Entity entity) {
        if (currentState != MonsterAIState.ATTACKING) {
            currentState = MonsterAIState.ATTACKING;
            setTarget(entity);
        }
    }

    @Override
    public String toString() {
        return "the " + getName();
    }

    /**
     * Update the monster according to standard AI. This will walk randomly through known areas until
     * it spots an enemy, at which point it will home in and attack.
     */
    @Override
    public void update() {
    	
        if(currentPath != null && currentPath.size() == 0) {
            currentPath = null;
        }
        switch(currentState) {
        case IDLE:
            //Check for hostiles.
            if(findHostile()) {

            }
            else {
                int chance = (int) (Math.random() * 100);
                if(chance < 15) {
					walkToRandom();
					currentState = MonsterAIState.WALKING;
				}
            }
            break;
        case WALKING:
            //If we're at the end of the path, change to idle.
            if(currentPath == null) {
                targetPoint = null;
                currentState = MonsterAIState.IDLE;
            }
            //Check for hostiles.
            else if(findHostile()) {

            }
            //Otherwise, continue moving
            else {
                moveToNext();
            }
            break;
        case ATTACKING:
            //If the target creature is dead, change to idle
            if(aimingTowards == null || aimingTowards.isDead() || !aimingTowards.getMap().equals(currentMap)) {
                targetPoint = null;
                currentPath = null;
                currentState = MonsterAIState.IDLE;
                break;
            }
            
            if(getVisibilityMap().getVisibility(aimingTowards.getX(), aimingTowards.getY()) != Visibility.VISIBLE) {
                currentState = MonsterAIState.WALKING;
            }
			setTargetPoint(new Point(aimingTowards.getX(), aimingTowards.getY()));

            if(currentPath == null) {
                currentState = MonsterAIState.WALKING; //No path to the target could be found

            }
            else if(Math.abs(aimingTowards.getX() - this.x) <= 1 && Math.abs(aimingTowards.getY() - this.y) <= 1) {
                //Next to the target
                attack(aimingTowards);
            }
            else {
                // Use int modifier as a basis for occasionally missing steps in the path
                // Monsters with int > 40 should skip 5% of steps. More stupid monsters
                // will skip a lot more
                int intMod = getAttributeSet().getAttribute(Attribute.INTELLIGENCE).getModifier();
                intMod = intMod > 2 ? 2 : intMod;
                if(Math.random() * 20 + intMod >= 2)
                    moveToNext();
            }
        }
		super.update();
        //return false;
    }

	/**
     * Searches through the list of entities in the map to find one this monster
     * is hostile towards. It sets the AI to attacking this monster
     * @return True if an enemy entity could be found
     */
    private boolean findHostile() {
        ArrayList<MapObject> objs = currentMap.getObjects();
        for(MapObject obj : objs) {
            if(obj instanceof Entity) {
                Entity e = (Entity) obj;
                if(e.equals(this)) {
                    continue;
                }
                if(getVisibilityMap().getVisibility(e.getX(), e.getY()) == Visibility.VISIBLE) {
                    if(getRace().hostileTowards(e.getRace().getIdent()) || ((e instanceof Player) && getRace().hostileTowards("player"))) {
                        setTarget(e);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void setTarget(Entity e) {
        targetPoint = new Point(e.getX(), e.getY());
        currentPath = currentMap.getPath(new Point(x, y), targetPoint);
        aimingTowards = e;
        currentState = MonsterAIState.ATTACKING;
    }

	@Override
	public boolean canInteract(int keyCode, Player player) {
		return keyCode == CharKey.a || keyCode == -1;
	}

	@Override
	public void interact(int keyCode, Player player) {
		player.attack(this);
	}


	@Override
	public String getDescription(boolean showFull, Player p) {
		String toReturn = name;
		if(showFull) {
			toReturn += ". ";
			if(getHealthValue() < getHealth().getDefaultValue() * 0.5) {
				toReturn += "`1It is heavily wounded.`0 ";
			}
			else if(getHealthValue() < getHealth().getDefaultValue()) {
				toReturn += "It is slightly wounded. ";
			}
			if (getLevel() > p.getLevel() + 2) {
				toReturn += "It looks `1much`0 stronger than you.";
			}
			else if(getLevel() > p.getLevel()) {
				toReturn += "It looks stronger than you.";
			}
			else if(getLevel() < p.getLevel()) {
				toReturn += "It looks weaker than you.";
			} else {
				toReturn += "It looks like a good match for you.";
			}

		}
		return toReturn;
	}
}
