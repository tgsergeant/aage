package com.bwj.aage.entity;

import java.io.Serializable;

/**
 *
 */
public class StatusCondition implements Serializable {
	
	private int attribute;
	private int delta;
	private int totalDuration;
	private int usedDuration;
	private String name;

	public StatusCondition(int attribute, int delta, int totalDuration, String name) {
		this.attribute = attribute;
		this.delta = delta;
		this.totalDuration = totalDuration;
		usedDuration = 0;
		this.name = name;
	}

	public void incrementDuration() {
		usedDuration++;
	}

	public boolean isDurationElapsed() {
		return usedDuration >= totalDuration;
	}

	public int getAttribute() {
		return attribute;
	}

	public int getDelta() {
		return delta;
	}

	public String getName() {
		return name;
	}

	public int getTimeRemaining() {
		return totalDuration - usedDuration;
	}
}
