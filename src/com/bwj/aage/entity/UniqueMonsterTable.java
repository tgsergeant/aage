package com.bwj.aage.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Tim Sergeant
 */
public class UniqueMonsterTable implements Serializable {

	public static UniqueMonsterTable table = null;

	private ArrayList<String> uniqueIds = new ArrayList<String>();

	private UniqueMonsterTable() {

	}

	public void addId(String id) {
		uniqueIds.add(id);
	}

	public boolean hasId(String id) {
		return uniqueIds.contains(id);
	}

	public static UniqueMonsterTable getTable() {
		if (table == null) {
			table = new UniqueMonsterTable();
		}
		return table;
	}

	public static void setTable(UniqueMonsterTable tab) {
		table = tab;
	}

	public static void clearTable() {
		table = new UniqueMonsterTable();
	}

	public int getSize() {
		return uniqueIds.size();
	}
}
