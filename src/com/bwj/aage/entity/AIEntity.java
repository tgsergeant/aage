package com.bwj.aage.entity;

import com.bwj.aage.*;
import com.bwj.aage.config.CharacterClass;
import com.bwj.aage.config.Race;

import java.util.ArrayList;

/**
 *
 */
public abstract class AIEntity extends Entity {
	protected ArrayList<Point> currentPath = null;
	protected Point targetPoint;

	public AIEntity(int x, int y, Tile t, Map m, Race r, CharacterClass cl, String name) {
		super(x, y, t, m, r, cl, name);
	}

	@Override
	protected void notifyLevelUp(int level) {
		while(getExperience() >= ExperienceTable.getExperience((getLevel() + 1))) {
			levelUp();
		}
	}

	@Override
	protected int selectAttributeToIncrease(boolean primary) {
		if (primary) {
			return (Integer)Util.randomElementOf(getCharacterClass().getPrimaries());
		} else {
			return (int) (Math.random() * 6);
		}
	}

	/**
	 * Moves the monster to the next point on its path.
	 */
	protected void moveToNext() {
		if(currentPath.size() == 0) {
			currentPath = null;
			return;
		}
		Point moveTo = currentPath.get(0);

		boolean moved = move(moveTo.getX() - x, moveTo.getY() - y);
		//System.out.println(moved + ", " + moveTo.getX() + ", " + moveTo.getY());
		if(moved) {
			currentPath.remove(0);
		}
	}

	public Point getTargetPoint() {
		return targetPoint;
	}

	public void setTargetPoint(Point targetPoint) {
		this.targetPoint = targetPoint;
		currentPath = getMap().getPath(getPositionAsPoint(), targetPoint);
	}

	protected void walkToRandom() {
		//Select a tile and start moving towards it.
		while(true) {
			int x = (int) (Math.random() * currentMap.getWidth());
			int y = (int) (Math.random() * currentMap.getHeight());
			
			if(getVisibilityMap().getVisibility(x, y) != Visibility.INVISBLE && !currentMap.getTile(x, y).blocksMovement()) {
				setTargetPoint(new Point(x, y));
				break;
			}
		}
	}

	@Override
	public String toString() {
		return getName();
	}
}
