package com.bwj.aage.entity;

import com.bwj.aage.Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class StatusConditionList implements Serializable {

	private List<StatusCondition> conditions = new ArrayList<StatusCondition>();

	public void addCondition(StatusCondition condition) {
		conditions.add(condition);
	}

	public void update(Entity entity) {
		ArrayList<StatusCondition> toRemove = new ArrayList<StatusCondition>();
		for (StatusCondition status : conditions) {
			status.incrementDuration();
			if (status.isDurationElapsed()) {
				toRemove.add(status);
				entity.getAttributeSet().getAttribute(status.getAttribute()).modifyValue(-status.getDelta());
				entity.notifyConditionRemoved(status);
			}
		}
		conditions.removeAll(toRemove);
	}

	public List<StatusCondition> getConditions() {
		return conditions;
	}
}
