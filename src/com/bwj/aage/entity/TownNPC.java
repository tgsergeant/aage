package com.bwj.aage.entity;

import com.bwj.aage.*;
import com.bwj.aage.config.CharacterClass;
import com.bwj.aage.config.Race;
import com.bwj.aage.map.House;
import net.slashie.libjcsi.CharKey;

import java.util.Random;

/**
 * A friendly NPC which inhabits a town. Each NPC has a house they call home, and prefer
 * to spend their time there. They can also randomly visit other locations within the
 * village.
 */
public class TownNPC extends AIEntity {

	enum AIState {
		AT_HOME, WALKING, OUTSIDE
	}
	private House home;

	private Random r = new Random();

	private AIState currentState = AIState.AT_HOME;
	
	public TownNPC(int x, int y, House house, Map m, Race r, CharacterClass cl, String name) {
		super(x, y,	new Tile(r.getTileChar(), r.getTileColor()), m, r, cl, name);
		this.home = house;
	}

	@Override
	protected void notifyAttackedBy(Entity entity) {
		// TODO Retaliate
	}

	@Override
	public void update() {
		super.update();
		switch(currentState) {
			case AT_HOME:
				//Occasionally move to a new location.
				if (r.nextInt(40) == 1) {
					walkToRandom();
					currentState = AIState.WALKING;
				}
				else if (r.nextInt(10) == 1) {
					moveToHome();
					currentState = AIState.WALKING;
				}
				break;
			case WALKING:
				if(currentPath == null || currentPath.size() == 0) {
					targetPoint = null;
					currentPath = null;
					if (home.contains(getPositionAsPoint())) {
						currentState = AIState.AT_HOME;
					} else {
						currentState = AIState.OUTSIDE;
					}
				} else {
					moveToNext();
				}
				break;
			case OUTSIDE:
				if (r.nextInt(20) == 1) {
					moveToHome();
					currentState = AIState.WALKING;
				}
				if (r.nextInt(10) == 1) {
					walkToRandom();
					currentState = AIState.WALKING;
				}
		}
	}

	private void moveToHome() {
		setTargetPoint(home.getRandomPointInside());
	}

	@Override
	public void interact(int keyCode, Player player) {
		if (keyCode == -1) {
			MessageManager.addMessage(getDescription(false, player) + " is in the way.");
		}
		else if (keyCode == CharKey.a) {
			player.attack(this);
		}
		else if (keyCode == CharKey.h) {
			MessageManager.addMessage("You switch places with " + getName());
			int deltax = player.getX() - getX();
			int deltay = player.getY() - getY();
			player.setX(player.getX() - deltax);
			player.setY(player.getY() - deltay);

			setX(getX() + deltax);
			setY(getY() + deltay);
		}
	}

	@Override
	public boolean canInteract(int keyCode, Player player) {
		return keyCode == CharKey.a || keyCode == CharKey.h || keyCode == -1;
	}
}
