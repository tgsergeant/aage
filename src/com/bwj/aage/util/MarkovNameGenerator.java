package com.bwj.aage.util;

import com.bwj.aage.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Generator for random names using Markov chains. We load a large dataset from a resource file.
 * The data is stored as short strings as the key to a HashMap, while a list of suffixes is
 * stored as the value.
 * When it's time to generate a name, we take the list of suffixes for the previous string,
 * and select a random one, append it to the string and repeat.
 *
 * Probabilities are implicit, which is nice.
 *
 * Algorithm adapted from http://roguebasin.roguelikedevelopment.org/index.php/Markov_chains_name_generator_in_Python
 *
 * Name data obtained from http://www.census.gov/genealogy/names/names_files.html
 * and http://inventwithpython.com/blog/2011/09/28/list-of-street-names-list-of-last-names/
 */
public class MarkovNameGenerator {
	
	public static MarkovNameGenerator femaleNameGen;
	public static MarkovNameGenerator maleNameGen;
	public static MarkovNameGenerator lastNameGen;
	public static MarkovNameGenerator placeGen;
	public static MarkovNameGenerator kingdomGen;

	/** Length of the chain (lookback) to use */
	private int chainLength;

	/** Dictionary loaded from the resource file */
	private HashMap<String, ArrayList<Character>> dictionary = new HashMap<String, ArrayList<Character>>();

	public static void initialiseNames() {
		long startTime = System.currentTimeMillis();

		femaleNameGen = MarkovNameGenerator.load("text/female.txt", 2);
		System.out.println("Loaded female names");
		maleNameGen = MarkovNameGenerator.load("text/male.txt", 2);
		System.out.println("Loaded male names");
		lastNameGen = MarkovNameGenerator.load("text/lastnames.txt", 2);
		System.out.println("Loaded surnames");
		placeGen =  MarkovNameGenerator.load("text/places.txt", 3);
		System.out.println("Loaded place names");
		kingdomGen = MarkovNameGenerator.load("text/kingdoms.txt", 2);
		System.out.println("Loaded kingdom names");

		System.out.println("Loading names took: " + (System.currentTimeMillis() - startTime) + "ms");
	}

	/**
	 * Creates a new generator with the given chainlength
	 * @param chainLength Lookback to use
	 */
	public MarkovNameGenerator(int chainLength) {
		this.chainLength = chainLength;
	}

	/**
	 * Adds suffix to the dictionary as a possibility for the given prefix
	 * @param prefix Chainlengthed prefix to use
	 * @param suffix Suffix to add
	 */
	public void addKey(String prefix, char suffix) {
		if (!dictionary.containsKey(prefix)) {
			dictionary.put(prefix, new ArrayList<Character>(3));
		}
		dictionary.get(prefix).add(suffix);

	}

	/**
	 * Returns a randomly selected suffix for the given prefix
	 * @param prefix Prefix currently used in the name
	 * @param r Random generator to use
	 * @return Suffix which fits with the given prefix
	 */
	public Character getSuffix(String prefix, Random r) {
		if (!dictionary.containsKey(prefix)) {
			return null;
		} else {
			ArrayList<Character> chains = dictionary.get(prefix);
			return chains.get(r.nextInt(chains.size()));
		}
	}

	/**
	 * Loads the data from the given file into a name generator. The data is
	 * loaded from /resources/ + filename.
	 * @param filename Filename to load from
	 * @param chainLength Chainlength of the name generator
	 * @return A name generator filled with the file's data
	 */
	public static MarkovNameGenerator load(String filename, int chainLength) {
		MarkovNameGenerator generator = new MarkovNameGenerator(chainLength);

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(MarkovNameGenerator.class.getResourceAsStream("/resources/" + filename)));

			String line;
			while ((line = reader.readLine()) != null) {
				//Parse line
				if (line.length() > 0) {
					//line = line.toLowerCase();
					String name = Util.createWhitespace(chainLength) + line;
					for (int i = 0; i < line.length(); i++) {
						generator.addKey(name.substring(i, i + chainLength), name.charAt(i + chainLength));
					}
					generator.addKey(name.substring(line.length(), line.length() + chainLength), '\n');
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return generator;
	}

	/**
	 * Creates a random name from the generator.
	 * @param r Random number generator to use
	 * @return A random name
	 */
	public String getName(Random r, int length) {
		String prefix = Util.createWhitespace(chainLength);
		StringBuilder name = new StringBuilder();
		Character suffix = null;
		while (name.length() <= length) {
			suffix = getSuffix(prefix, r);
			if (suffix == '\n') {
				break;
			}
			name.append(suffix);
			prefix = prefix.substring(1) + suffix;
		}
		return name.toString();
	}
	
	public String getName(Random r) {
		return getName(r, 9);
	}

	

	public static void main(String[] args) {

		MarkovNameGenerator gen = MarkovNameGenerator.load("text/kingdoms.txt", 2);
		Random r = new Random();
		for (int i = 0; i < 100; i++) {
			System.out.println(gen.getName(r, 11));
		}
	}
}
