package com.bwj.aage.util;

import com.bwj.aage.Util;

import java.io.*;

/**
 *
 */
public class TextCutter {
	public static void main(String[] args) {
		try {
			BufferedReader buf = new BufferedReader(new FileReader("src/resources/text/kingdomsout.txt"));
			PrintWriter out = new PrintWriter("src/resources/text/kingdomsout.txt");

			String line;
			while ((line = buf.readLine()) != null) {
				//String[] split = line.split(";");
				String name = line.toLowerCase();
				name = Util.capitalize(name);
				System.out.println(name);
				out.println(name);

			}
			out.flush();
			System.out.println("Done!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
