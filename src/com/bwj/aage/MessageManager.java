package com.bwj.aage;

import java.util.ArrayList;

/**
 * Collates messages from across other classes and stores them
 * so they can be displayed on the screen.
 */
public class MessageManager {
    
    private static ArrayList<Message> messageList = new ArrayList<Message>();
    
    protected MessageManager() {
    }

    
    public static void addMessage(String toAdd) {
        messageList.add(new Message(toAdd, -1, -1, 1));
    }
    
    public static void addMessage(String toAdd, int x, int y) {
        messageList.add(new Message(toAdd, x, y, 1));
    }


	public static void addMessage(String message, int duration) {
		messageList.add(new Message(message, -1, -1, duration));
	}

    public static ArrayList<Message> getMessages() {
        return messageList;
    }
    
    public static ArrayList<Message> getMessages(int x, int y, int radius) {
        ArrayList<Message> results = new ArrayList<Message>();
        for (Message m : messageList) {
            if (Math.hypot(m.getXsource() - x, m.getYsource() - y) <= radius
                    || (m.getXsource() == -1 && m.getYsource() == -1)) {
                results.add(m);
            }
        }
        return results;
    }

    /**
     * Reduces the time left on all messages, and remove those which have expired.
     */
    public static void clearMessages() {
		ArrayList<Message> toDelete = new ArrayList<Message>();
		for (Message m : messageList) {
			if (m.getDuration() == 1) {
				toDelete.add(m);
			}
			else {
				m.setDuration(m.getDuration() - 1);
			}
		}
		messageList.removeAll(toDelete);
	}


}
