package com.bwj.aage.config;

import com.bwj.aage.config.types.ConfigType;
import com.bwj.aage.config.types.FlagType;
import com.bwj.aage.player.SaveHeader;

import java.io.PrintWriter;
import java.io.Serializable;
import java.util.HashMap;

/**
 * A single element of the configuration. An element has a id, a type and a list
 * of attributes identitfied as Key-Value pairs.
 */
public abstract class ConfigElement implements Serializable {

	private final String id;

	private HashMap<String, ConfigType> attributes = new HashMap<String, ConfigType>();

	public ConfigElement(String id) {
		this.id = id.toLowerCase();
	}


	public String getId() {
		return id;
	}

	public void addAtribute(String id, ConfigType value) {
		attributes.put(id.toLowerCase(), value);
		//System.out.println("Added attribute: " + id + ", " + value);
	}

	/**
	 * Creates a ConfigElement from the given token list. The current position
	 * of the list is assumed to be at the very start of the element - that is,
	 * the next element is the colon. This will load all flags from the element
	 * and return with the current position of the Tokenizer set to the closing
	 * brace of the element.
	 * @param tokens Tokens of the current file.
	 * @return The element that was parsed from tokens.
	 */
	public static ConfigElement loadElement(Tokenizer tokens) throws ConfigParseException {
		String name = tokens.getCurrent();

		//Check that the next element is a colon
		if(!tokens.next().equals(":")) {
			throw new ConfigParseException(":", tokens.getCurrent(), tokens.getContext());
		}

		//Load the type
		String type = tokens.next();
		ConfigElement elem = createElement(type, name);
		if(elem == null) {
			throw new ConfigParseException("Could not create element of type " + type);
		}

		//Next token is a open brace
		if (!tokens.next().equals("{")) {
			throw new ConfigParseException("{", tokens.getCurrent(), tokens.getContext());
		}

		//Parse until we hit a closed brace
		String tok;
		while (!(tok = tokens.next()).equals("}")) {
			String attributeName = tokens.getCurrent();
			String seperator = tokens.next();

			String attEnd; //The final token on this 'line'
			ConfigType value;
			if (seperator.equals("=")) {
				value = ConfigType.parseValue(tokens);

				attEnd = tokens.next();
			} else {
				value = new FlagType();
				attEnd = seperator;
			}

			elem.addAtribute(attributeName, value);

			if(attEnd.equals("}")) {
				break;
			} else if(!attEnd.equals(",")) {
				throw new ConfigParseException(",", attEnd, tokens.getContext());
			}

		}
		return elem;
	}

	/**
	 * Creates a new element of the given type with the given ID. Used to abstract
	 * the if/else if chain from the rest of the code.
	 * @param type Type to create
	 * @param id ID of the element
	 * @return An element of this type
	 */
	private static ConfigElement createElement(String type, String id) {
		if (type.equalsIgnoreCase("monster")) {
			return new MonsterTemplate(id);
		} else if (type.equalsIgnoreCase("race")) {
			return new Race(id);
		} else if (type.equalsIgnoreCase("class")) {
			return new CharacterClass(id);
		} else if (type.equalsIgnoreCase("savegame")) {
			return new SaveHeader(id);
		} else {
			return null;
		}
	}

	/**
	 * Validates that the element has all the required attributes with the rquired types.
	 * This changes depending on the type of element.
	 * @return True if the element was validated successfully.
	 */
	public abstract boolean validate() throws ConfigValidationException;

	/**
	 * A method which can be optionally overwritten which 'compiles' the element,
	 * allowing the data provided by the underlying model to be converted to
	 * a more convenient or faster representation.
	 * Useful for converting AttributeSets, etc.
	 *
	 * Compile is guaranteed to only be called on valid elements
	 */
	public void compile() throws ConfigValidationException {

	}

	public ConfigType getAttributeValue(String name) {
		return attributes.get(name);
	}

	public boolean hasAttribute(String name) {
		return attributes.containsKey(name.toLowerCase());
	}

	public void assertType(String name, Class type) throws ConfigValidationException {
		try {
			if (!getAttributeValue(name).getClass().equals(type)) {
				throw new ConfigValidationException(id, name, type.toString(), getAttributeValue(name).getClass().toString());
			}
		} catch (NullPointerException e) {
			throw new ConfigValidationException(id, name);
		}
	}

	/**
	 * @return The type of the element, used for saving the element to a file.
	 */
	public abstract String getElementType();

	/**
	 * Save this element to a file.
	 * Prints: The header line,
	 * Each attribute (as defined by its type)
	 * The final '}'
	 * @param out Writer to output to.
	 */
	public void save(PrintWriter out) {
		out.println(getId() + " : " + getElementType() + " {");
		for (String attKey : attributes.keySet()) {
			ConfigType attribute = attributes.get(attKey);
			out.print("\t" + attKey);
			if (!(attribute instanceof FlagType)) {
				out.print(" = " + attribute.toString());
			}
			out.println(",");
		}
		out.println("}");
	}
}
