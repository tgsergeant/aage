package com.bwj.aage.config;

/**
 *
 */
public class ConfigValidationException extends Exception {
	public ConfigValidationException(String id, String attName, String expected, String received) {
		super("Incorrect type in " + id +  " at " + attName + ". Expected " + expected + ", received " + received);
	}

	public ConfigValidationException(String id, String name) {
		super("Missing required attribute " + name + " in element " + id);
	}
}
