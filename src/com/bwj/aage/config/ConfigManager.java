package com.bwj.aage.config;

import com.bwj.aage.Util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Loads the configuration of Races, classes and monsters from XML files and stores it
 * for later use
 */
public class ConfigManager {

    /** The races that have been loaded */
    private static HashMap<String, Race> races = new HashMap<String, Race>();
    /** The classes that have been loaded */
    private static HashMap<String, CharacterClass> classes = new HashMap<String, CharacterClass>();

    private static ArrayList<MonsterTemplate> monsters = new ArrayList<MonsterTemplate>();


    public static void load() {
		try {
			ConfigDocument doc = ConfigDocument.load("/resources/root.acf");
			for (ConfigElement element : doc.getElements()) {
				if (element instanceof Race) {
					races.put(element.getId(), (Race) element);
				}
				else if (element instanceof CharacterClass) {
					classes.put(element.getId(), (CharacterClass) element);
				}
				else if (element instanceof MonsterTemplate) {
					monsters.add((MonsterTemplate) element);
				}
			}
		} catch (Exception e) {
			Util.reportError(e);
		}
	}


    /**
     * Finds an XML Monster at the given level, which can then be used to create a proper Monster.
     * We first filter the list of monsters to those which meet the level requirement,
     * and then use each monster types rarity modifier as its 'weight' within the array, to choose
     * a weighted monster type
     *
	 * @param level The level of monster desired
	 * @param filterRace
	 * @return A weighted random selection of monster for that level.
     */
    public static MonsterTemplate findMonster(int level, Race filterRace) {
        //Filter the monster list for those at the desired level
        ArrayList<MonsterTemplate> filteredMonsters = new ArrayList<MonsterTemplate>();
        int totalRarity = 0;
        for(MonsterTemplate monster : monsters) {
            if (level <= monster.getMaxLevel() && level >= monster.getMinLevel()
					&& (filterRace == null || monster.getRace().equals(filterRace))) {
                filteredMonsters.add(monster);
                totalRarity += monster.getRarityModifier();
            }
        }
        if(filteredMonsters.size() == 0) {
            return null;
        }
        //Choose one index at random from the filtered list
        double index = Math.random() * totalRarity;
        int runningRarity = 0; //Running total of rarity
        for (MonsterTemplate monster : filteredMonsters) {
            runningRarity += monster.getRarityModifier();
            if (index <= runningRarity) {
                return monster;
            }
        }
        return null;
    }

    public static Race getRace(String ident) {
        return races.get(ident.toLowerCase());
    }
    
    public static CharacterClass getClass(String ident) {
        return classes.get(ident.toLowerCase());
    }

    /**
     *
     * @return A list of all the player races in the currently loaded race set
	 * @param tag
     */
    public static ArrayList<Race> getRaceByTag(String tag) {
        ArrayList<Race> found = new ArrayList<Race>();
        for (String key : races.keySet()) {
            Race r = races.get(key);
            if (r.hasAttribute(tag)) {
                found.add(r);
            }
        }

        return found;
    }

    public static ArrayList<CharacterClass> getClassesByTag(String tag) {
        ArrayList<CharacterClass> found = new ArrayList<CharacterClass>();
        for (String key : classes.keySet()) {
            CharacterClass c = classes.get(key);
            if (c.hasAttribute(tag)) {
                found.add(c);
            }
        }

        return found;
    }
}
