package com.bwj.aage.config.types;

import com.bwj.aage.config.ConfigParseException;
import com.bwj.aage.config.Tokenizer;

import java.util.ArrayList;
import java.util.List;

/**
 * A list of ConfigTypes, all of which should have the same type.
 * These are represented using square brackets: [item1, item2, item3].
 * Nested lists are also possible, but note that the child lists can be made
 * up of different types.
 * For example, [ [1,2], ["a", "b"] ] is syntactically valid.
 */
public class ListType extends ConfigType {

	private List<ConfigType> list;

	public ListType() {
		list = new ArrayList<ConfigType>(3);
	}

	public void addValue(ConfigType val) {
		list.add(val);
	}

	@Override
	public Object getValue() {
		return list;
	}

	@Override
	public String toString() {
		String built = "[";
		for (int i = 0; i < list.size(); i++) {
			 built += list.get(i).toString();
			if (i < list.size() - 1) {
				built += ", ";
			}
		}
		built += "]";
		return built;
	}

	public static ListType parse(Tokenizer tokens) throws ConfigParseException {
		ListType list = new ListType();
		Class listClass = null;
		String tok;
		while (true) {
			ConfigType nextValue = ConfigType.parseValue(tokens);
			if (listClass == null) {
				listClass = nextValue.getClass();
			} else if (!nextValue.getClass().equals(listClass)) {
				throw new ConfigParseException(listClass.toString(), nextValue.getClass().toString(), tokens.getContext());
			}
			list.addValue(nextValue);

			//Check for comma
			tok = tokens.next();
			if (tok.equals("]")) {
				break;
			} else if (!tok.equals(",")) {
				throw new ConfigParseException(",", tok, tokens.getContext());
			}
		}

		return list;
	}
}
