package com.bwj.aage.config.types;

/**
 * A Truth value, represented as a case-insensitive True or False.
 */
public class BooleanType extends ConfigType {

	public static final BooleanType TRUE = new BooleanType(true);

	public static final BooleanType FALSE = new BooleanType(false);
	private final boolean bool;

	public BooleanType(boolean bool) {
		this.bool = bool;
	}

	@Override
	public Object getValue() {
		return bool;
	}

	@Override
	public String toString() {
		return String.valueOf(bool);
	}
}
