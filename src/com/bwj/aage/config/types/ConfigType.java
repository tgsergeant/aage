package com.bwj.aage.config.types;

import com.bwj.aage.config.ConfigParseException;
import com.bwj.aage.config.Tokenizer;

import java.io.Serializable;

/**
 * A wrapper around a range of different data types allowing all of them to be used
 * as values for config attributes. Provides a method of extracting the value from
 * a type, as well as creating a type from a token list.
 */
public abstract class ConfigType implements Serializable {

	/**
	 * @return The value stored within this object
	 */
	public abstract Object getValue();

	/**
	 * Parses the value at the *next* position of the Tokenizer, returning it as
	 * a subclass of ConfigType.
	 * @param tokens Tokens to parse
	 * @return The next value present in the token list
	 * @throws ConfigParseException Thrown if a syntax error is encountered.
	 */
	public static ConfigType parseValue(Tokenizer tokens) throws ConfigParseException {
		String start = tokens.next();
		if (start.equals("\"")) {
			return StringType.parse(tokens);
		}
		else if (start.equalsIgnoreCase("true")) {
			return BooleanType.TRUE;
		}
		else if (start.equalsIgnoreCase("false")) {
			return BooleanType.FALSE;
		}
		else if (start.equals("(")) {
			return RangeType.parse(tokens);
		}
		else if (start.equals("#")) {
			return ColorType.parse(tokens);
		} else if (start.equals("[")) {
			return ListType.parse(tokens);
		} else {
			return IntType.parse(tokens);
		}

	}

	public abstract String toString();


}
