package com.bwj.aage.config.types;

import com.bwj.aage.config.ConfigParseException;
import com.bwj.aage.config.Tokenizer;

/**
 * A type storing a single integer - no floating point values are currently
 * possible in ACF
 */
public class IntType extends ConfigType {

	private final int value;

	public IntType(int value) {
		this.value = value;
	}

	@Override
	public Object getValue() {
		return value;
	}

	public static IntType parse(Tokenizer tokens) throws ConfigParseException {
		try {
			return new IntType(Integer.parseInt(tokens.getCurrent()));
		} catch (NumberFormatException e) {
			throw new ConfigParseException("\\d+", tokens.getCurrent(), tokens.getContext());
		}
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}
}
