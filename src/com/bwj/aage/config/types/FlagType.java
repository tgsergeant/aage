package com.bwj.aage.config.types;

/**
 * A type with no value - it is used to flag whether a certain property
 * is present or not.
 * This is kind of redundant when considering BooleanType, but provides
 * nice syntax.
 */
public class FlagType extends ConfigType {
	@Override
	public Object getValue() {
		return null;
	}

	@Override
	public String toString() {
		return "Set";
	}
}
