package com.bwj.aage.config.types;

import com.bwj.aage.config.ConfigParseException;
import com.bwj.aage.config.Tokenizer;
import net.slashie.util.Util;

/**
 * A type representing an inclusive range between two integer values.
 * A range is represented as (low, high).
 */
public class RangeType extends ConfigType{

	private final int min, max;

	public RangeType(int min, int max) {
		this.min = min;
		this.max = max;
	}


	/**
	 * @return A random number between min and max, inclusive.
	 */
	@Override
	public Object getValue() {
		return Util.rand(min, max);
	}

	public static RangeType parse(Tokenizer tokens) throws ConfigParseException {
		try {
			int low = Integer.parseInt(tokens.next());
			if (!tokens.next().equals(",")) {
				throw new ConfigParseException(",", tokens.getCurrent(), tokens.getContext());
			}
			int high = Integer.parseInt(tokens.next());
			if (!tokens.next().equals(")")) {
				throw new ConfigParseException(")", tokens.getCurrent(), tokens.getContext());
			}

			return new RangeType(low, high);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			throw new ConfigParseException("Could not parse integer from " + tokens.getCurrent() + "at " + tokens.getContext());
		}
	}

	@Override
	public String toString() {
		return "(" + min + ", " + max + ")";
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}
}
