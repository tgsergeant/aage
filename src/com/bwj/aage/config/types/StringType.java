package com.bwj.aage.config.types;

import com.bwj.aage.config.ConfigParseException;
import com.bwj.aage.config.Tokenizer;

/**
 * A string value, which starts and ends with double quotes. There are no escape sequences
 * to allow quotes within the string.
 */
public class StringType extends ConfigType {

	private String value;

	public StringType(String value) {
		this.value = value;
	}

	@Override
	public Object getValue() {
		return value;
	}


	public static StringType parse(Tokenizer tokens) throws ConfigParseException {
		if (!"\"".equals(tokens.getCurrent())) {
			throw new ConfigParseException("\"", tokens.getCurrent(), tokens.getContext());
		}
		String parsed = "";
		String tok;
		while (!(tok = tokens.next()).equals("\"") ) {
			parsed += tok;
		}
		return new StringType(parsed);
	}

	@Override
	public String toString() {
		return "\"" + value + "\"";
	}
}
