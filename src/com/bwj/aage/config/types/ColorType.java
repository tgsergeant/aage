package com.bwj.aage.config.types;

import com.bwj.aage.config.ConfigParseException;
import com.bwj.aage.config.Tokenizer;
import net.slashie.libjcsi.CSIColor;

/**
 * A type representing a RGB colour, written in hex as
 * #RRGGBB. This is parsed into a CSIColor.
 */
public class ColorType extends ConfigType {

	public final CSIColor color;

	public ColorType(CSIColor color) {
		this.color = color;
	}

	@Override
	public Object getValue() {
		return color;
	}

	@Override
	public String toString() {
		return "#" + Integer.toString(color.getColor(), 16);
	}

	public static ColorType parse(Tokenizer tokens) throws ConfigParseException {
		try {
			int code = Integer.parseInt(tokens.next(), 16);
			return new ColorType(new CSIColor(code));
		} catch (NumberFormatException e) {
			throw new ConfigParseException("RRGGBB", tokens.getCurrent(), tokens.getContext());
		}
	}
}
