package com.bwj.aage.config;

import com.bwj.aage.AttributeSet;
import com.bwj.aage.config.types.ConfigType;
import com.bwj.aage.config.types.IntType;
import com.bwj.aage.config.types.ListType;
import com.bwj.aage.config.types.StringType;
import net.slashie.libjcsi.CSIColor;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The race of an entity. Determines their starting attributes, enemies,
 * and the symbol used to represent them on the map.
 */
public class Race extends ConfigElement implements Serializable {


	public Race(String id) {
		super(id);
	}

	public AttributeSet getAttributeSet() {
        return attributeSet;
    }

    public void setAttributeSet(AttributeSet attributeSet) {
        this.attributeSet = attributeSet;
    }

	public boolean isPlayerRace() {
		return hasAttribute("player");
	}

    /**
     * The attributes of this race. Any entity with this race has these
     * as base attributes.
     */
    private AttributeSet attributeSet = new AttributeSet(0);
    /**
     *
     */
    private static final long serialVersionUID = -7069083592397441115L;
    /** The list of other races that the AI will attack on sight */
    protected ArrayList<String> hostileRaces = new ArrayList<String>();

    public char getTileChar() {
		if (hasAttribute("character")) {
			return getAttributeValue("character").getValue().toString().charAt(0);
		}
		else {
			return '@';
		}
	}


    /**
     * Determines whether we are hostile towards the given race
     * @param id Identifier of another race
     * @return True if this race is hostile towards the given race
     */
    public boolean hostileTowards(String id) {
        for(String name : hostileRaces) {
            if(name.equals(id))
                return true;
        }
        return false;
    }

    public String toString() {
        return getName();
    }

    public String getName() {
        return getAttributeValue("name").getValue().toString();
    }

    public String getIdent() {
        return getId();
    }

	@Override
	public boolean validate() throws ConfigValidationException {
		assertType("name", StringType.class);
		assertType("agility", IntType.class);
		assertType("charisma", IntType.class);
		assertType("endurance", IntType.class);
		assertType("intelligence", IntType.class);
		assertType("perception", IntType.class);
		assertType("strength", IntType.class);

		if (hasAttribute("player")) {
			assertType("description", StringType.class);
		}
		if (hasAttribute("enemies")) {
			assertType("enemies", ListType.class);
		}
		return true;
	}

	@Override
	public void compile() throws ConfigValidationException {
		super.compile();
		//Compile attributes
		for (int i = 0; i < 6; i++) {
			final String name = AttributeSet.ATTRIBUTENAMES[i].toLowerCase();
			attributeSet.getAttribute(i).setDefaultValue((Integer) getAttributeValue(name).getValue());
			attributeSet.getAttribute(i).setValue((Integer) getAttributeValue(name).getValue());
		}

		//Compile enemy list
		if (hasAttribute("enemies")) {
			ArrayList list = (ArrayList) getAttributeValue("enemies").getValue();
			for (Object o : list) {
				try {
					String enemy = (String) ((ConfigType)o).getValue();
					hostileRaces.add(enemy);

				} catch (ClassCastException e) {
					throw new ConfigValidationException(getId(), "enemies", StringType.class.toString(), o.getClass().toString());
				}
			}
		}
	}

	@Override
	public String getElementType() {
		return "Race";
	}

	public String getDescription() {
		return getAttributeValue("description").getValue().toString();
	}

	public CSIColor getTileColor() {
		if (hasAttribute("color")) {
			return (CSIColor) getAttributeValue("color").getValue();
		}
		else {
			return CSIColor.WHITE;
		}
	}
}
