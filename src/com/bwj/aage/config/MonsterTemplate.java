package com.bwj.aage.config;

import com.bwj.aage.Util;
import com.bwj.aage.config.types.RangeType;
import com.bwj.aage.config.types.StringType;
import net.slashie.libjcsi.CSIColor;

/**
 * Represents the details of a monster which has been loaded from the monsters.config file.
 * This class contains all of the details required by MonsterSpawner in order to create a
 * new monster.
 */
public class MonsterTemplate extends ConfigElement {

	/**
	 * Creates a new monster template with the given name
	 *
	 * @param name Name of the monster
	 */
	public MonsterTemplate(String name) {
		super(name);
	}

	@Override
	public boolean validate() throws ConfigValidationException {
		assertType("name", StringType.class);
		assertType("race", StringType.class);
		assertType("class", StringType.class);
		return true;
	}

	@Override
	public String getElementType() {
		return "Monster";
	}

	public int getRarity() {
		return (Integer) getAttributeValue("rarity").getValue();
	}
	public Race getRace() {
		return ConfigManager.getRace(getAttributeValue("race").getValue().toString());
	}

	public CharacterClass getCharacterClass() {
		return ConfigManager.getClass(getAttributeValue("class").getValue().toString());
	}

	public int getMinLevel() {
		return ((RangeType) getAttributeValue("level")).getMin();
	}

	public int getMaxLevel() {
		return ((RangeType) getAttributeValue("level")).getMax();
	}

	/**
	 * Retrieves the color for this monster. We first try Monster.color, if this fails, we try Race.color.
	 * If this fails, we return white.
	 * @return The color for this monster.
	 */
	public CSIColor getColor() {
		if (hasAttribute("color")) {
			return (CSIColor) getAttributeValue("color").getValue();
		}
		else if (getRace().hasAttribute("color")) {
			return (CSIColor) getRace().getAttributeValue("color").getValue();
		}
		else {
			return CSIColor.WHITE;
		}

	}

	public String getName() {
		return Util.capitalize((String) getAttributeValue("name").getValue());
	}


	/**
	 * Determines how often this monster should spawn relative to a monster with rarity 5.
	 * A rarity 1 monster will spawn 64 times more often than a rarity 5 monster.
	 * @return The number of times this monster will spawn compared to a rarity 5 monster.
	 */
	public int getRarityModifier() {
		switch (getRarity()) {
			case 1: return 64;
			case 2: return 32;
			case 3: return 10;
			case 4: return 6;
			default: return 1;
		}
	}
}
