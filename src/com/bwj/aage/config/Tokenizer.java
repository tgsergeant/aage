package com.bwj.aage.config;

import com.bwj.aage.config.types.StringType;

import java.util.ArrayList;

/**
 * Takes a string and acts as a lexer, breaking it up into tokens.
 * The result can then be iterated through as the document is parsed.
 */
public class Tokenizer extends ArrayList<Tokenizer.Token> {

	public static final int CONTEXT_SIZE = 4;

	/**
	 * Constructs a list of tokens from the given string.
	 * @param str String to tokenize
	 */
	public Tokenizer(String str) {
		super(20);
		String currentToken = "";
		boolean isString = false;
		int currentLine = 0;
		for (char c : str.toCharArray()) {
			if (isWhitespace(c)) {
				if (c == '\n') {
					currentLine ++;
				}
				if (isString) {
					currentToken += c;
				} else if (!currentToken.equals("")) {
					add(new Token(currentToken, currentLine));
					currentToken = "";
				}
			}
			else if (isSpecialCharacter(c)) {
				if (!currentToken.equals("")) {
					add(new Token(currentToken, currentLine));
					currentToken = "";
				}
				add(new Token(String.valueOf(c), currentLine));
				if (c == '"') {
					isString = !isString;
				}
			} else {
				currentToken += c;
			}
		}
		if(!currentToken.equals("")) {
			add(new Token(currentToken, currentLine));
		}
	}

	private int position = -1;

	/**
	 * Interprets the next token as the start of a string,
	 * constructs a StringType from it, and returns the parsed
	 * value
	 * @return A string, which is contained in the document within double quotes
	 * @throws ConfigParseException Thrown if no valid string could be found
	 */
	public String parseString() throws ConfigParseException {
		position++;
		return StringType.parse(this).getValue().toString();
	}

	/**
	 * @return The token at the current position
	 */
	public String getCurrent() {
		return get(position).getTok();
	}

	/**
	 * Increments the position and returns the token there.
	 * @return The next token, or null if we are at the end.
	 */
	public String next() {
		position++;

		if (position < size()) {
			return get(position).getTok();
		}
		return null;
	}

	/**
	 * Gets the token delta elements away from the current token.
	 * @param delta Position relative to the current token to return
	 * @return Token at that position, or null if out of bounds
	 */
	public String getRelative(int delta) {
		final int pos = position + delta;
		if(pos >= 0 && pos < size()) {
			return get(pos).getTok();
		}
		return null;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}


	private static boolean isSpecialCharacter(char c) {
		return (c == '(' || c == ')' || c == '"' || c == '=' || c == ',' ||
				c == ':' || c == '{' || c == '}' || c == '#' || c == '[' || c == ']');
	}

	private static boolean isWhitespace(char c) {
		return (c == ' ' || c == '\n' || c == '\t' || c == '\r');
	}

	public boolean hasNext() {
		return position < size() - 1;
	}

	/**
	 * Creates a string representing the context of the current token, by printing n tokens to either
	 * side of the current position.
	 * @return A string containing those words.
	 */
	public String getContext() {
		int line = get(position).getLine();
		String context = "line " + line + ": '";

		for (Token tok : this) {
			if (tok.getLine() == line) {
				context += tok.getTok() + " ";

			}
		}
		context += "'";
		return context;
	}

	/**
	 * A string representing a single token within the document. We associate a
	 * line number with the string to produce nicer syntax errors.
	 */
	public class Token {
		private String tok;
		private int line;

		/**
		 * Constructs a new Token
		 * @param tok String of the token
		 * @param line Line number of this token
		 */
		public Token(String tok, int line) {
			this.tok = tok;
			this.line = line;
		}

		public String getTok() {
			return tok;
		}

		public int getLine() {
			return line;
		}
	}
}
