package com.bwj.aage.config;

/**
 *
 */
public class ConfigParseException extends Exception {

	public ConfigParseException(String message) {
		super(message);
	}

	public ConfigParseException(String expected, String received, String context) {
		super("Syntax error: Expecting '" + expected + "', received '" + received + "' at " + context);
	}
}
