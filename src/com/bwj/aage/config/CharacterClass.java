package com.bwj.aage.config;

import com.bwj.aage.AttributeSet;
import com.bwj.aage.config.types.IntType;
import com.bwj.aage.config.types.ListType;
import com.bwj.aage.config.types.StringType;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the 'profession' or specialisation of an entity. Provides modifiers to the entity's
 * attributes, determines how they level up, and what skills they have
 */
public class CharacterClass extends ConfigElement {

    private AttributeSet attributeModifiers = new AttributeSet(0);

    private ArrayList<Integer> primaries = new ArrayList<Integer>();

	public CharacterClass(String id) {
		super(id);
	}

	public void addPrimaryAttribute(int id) {
        primaries.add(id);
    }

    public boolean isPrimaryAttribute(int id) {
        return primaries.contains(id);
    }
    
    public ArrayList<Integer> getPrimaries() {
        return primaries;
    }

    public String getName() {
        return getAttributeValue("name").getValue().toString();
    }

    public String getDescription() {
        return getAttributeValue("description").getValue().toString();
    }

    public AttributeSet getAttributeModifiers() {
        return attributeModifiers;
    }

    /**
     * Different class types multiply entity difficulty by different amounts
     * @return The modifier for this class
     */
    public int getDifficultyModifier() {
		if (hasAttribute("normal")) {
			return 3;
		} else if (hasAttribute("commander")) {
			return 7;
		} else if (hasAttribute("elite")) {
			return 15;
		} else {
			return 1; //Minion or empty
		}
	}

	@Override
	public boolean validate() throws ConfigValidationException {

		//We need names and primaries
		assertType("name", StringType.class);
		assertType("primaries", ListType.class);

		if (hasAttribute("player")) {
			assertType("description", StringType.class);
		}

		for (int i = 0; i < 6; i++) {
			String attName = AttributeSet.ATTRIBUTENAMES[i].toLowerCase();
			if (hasAttribute(attName)) {
				assertType(attName, IntType.class);
			}
		}

		return true;
	}

	@Override
	public void compile() throws ConfigValidationException {
		//We first compile our primary attributes.
		List primList = (List) getAttributeValue("primaries").getValue();
		for (Object o : primList) {
            String attString = ((StringType) o).getValue().toString();
            int index = AttributeSet.getAttributeIndex(attString);
            if (index != -1) {
                addPrimaryAttribute(index);
            }
        }

		//Next, create our attribute modifiers
		for (int i = 0; i < 6; i++) {
			String attname = AttributeSet.ATTRIBUTENAMES[i].toLowerCase();
			if (hasAttribute(attname)) {
				Integer value = (Integer) getAttributeValue(attname).getValue();
				attributeModifiers.getAttribute(i).setDefaultValue(value);
				attributeModifiers.getAttribute(i).setValue(value);
			}
		}
	}

	@Override
	public String getElementType() {
		return "Class";
	}
}
