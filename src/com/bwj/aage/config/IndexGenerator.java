package com.bwj.aage.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Convenience utility class to generate index files, listing each of the files in the given
 * folders. This allows all of the required files to be found when they are loaded from the
 * classpath in the game itself.
 */
public class IndexGenerator {
    
    public static void main(String[] args) {
        generateIndex("/resources/races/");
        generateIndex("/resources/classes/");
    }
    
    private static void generateIndex(String folderName) {
        File dir = new File("src" + folderName);
        String[] files = dir.list();

		try {
			File outputFile = new File("src" + folderName + "index.acf");
			PrintWriter out = new PrintWriter(outputFile);
			for (String fileName : files) {
				if (fileName.endsWith("acf") && !fileName.startsWith("index")) {
					out.println("import \"" + folderName + fileName + "\"");
				}
			}
			out.close();
			System.out.println("Finished " + folderName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
