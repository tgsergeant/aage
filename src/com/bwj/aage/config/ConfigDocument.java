package com.bwj.aage.config;

import java.io.*;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * A document used for configuration, written in a special format for AAGE. Designed to be
 * extendable with little to no effort.
 */
public class ConfigDocument {

	private ArrayList<ConfigElement> elements = new ArrayList<ConfigElement>(30);


	public static ConfigDocument load(String filename) throws ConfigParseException, ConfigValidationException {
		final InputStream inputStream = ConfigDocument.class.getResourceAsStream(filename);
		return load(inputStream);
	}
	/**
	 * Loads an acf file from the given location, relative to the root of the classpath.
	 * A formal grammar defining the acf format is given in grammar.txt
	 * @param inputStream Stream to load from
	 * @return A document loaded from that location
	 * @throws ConfigParseException Thrown whenever a syntax error occurs
	 */
	public static ConfigDocument load(InputStream inputStream) throws ConfigParseException, ConfigValidationException {


		if (inputStream == null) {
			throw new ConfigParseException("Could not read file");
		}

		BufferedInputStream stream = new BufferedInputStream(inputStream);
		Scanner scanner = new Scanner(stream);
		scanner.useDelimiter("\\A");
		String str;
		try {
			str = scanner.next();
		} catch (NoSuchElementException e) {
			str = "";
		}

		Tokenizer tokens = new Tokenizer(str);

		ConfigDocument document = new ConfigDocument();

		while (tokens.hasNext()) {
			String current = tokens.next();
			//Import another file
			if (current.equalsIgnoreCase("import")) {
				String toImport = tokens.parseString();
				System.out.println("Importing " + toImport);
				ConfigDocument doc = load(toImport);
				document.elements.addAll(doc.getElements());
			}
			//Parse an element
			else if (":".equals(tokens.getRelative(1))) {
				System.out.println("Adding element: " + current);
				ConfigElement elem = ConfigElement.loadElement(tokens);

				if (elem.validate()) {
					elem.compile();
					document.addElement(elem);
				}

			}

		}

		return document;
	}

	/**
	 * Saves this document to the given file/whatever
	 * @param stream Output stream to write to
	 * @throws FileNotFoundException Thrown if the file doesn't exist
	 */
	public void save(OutputStream stream) throws FileNotFoundException {
		PrintWriter out = new PrintWriter(stream);
		for (ConfigElement elem : elements) {
			out.println();
			elem.save(out);
		}
		out.close();
	}

	/**
	 * Adds the given element to the document
	 * @param elem Element to add
	 */
	public void addElement(ConfigElement elem) {
		elements.add(elem);
	}
	public ArrayList<ConfigElement> getElements() {
		return elements;
	}

	public static void main(String[] args) {
		try {
			ConfigDocument doc = load("/resources/root.acf");
			System.out.println("Loaded " + doc.getElements().size() + " elements.");
			//Save to a file
			doc.save(new FileOutputStream("compiled.acf"));
			System.out.println("Saved to compiled.acf");
		} catch (ConfigParseException e) {
			e.printStackTrace();
		} catch (ConfigValidationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
