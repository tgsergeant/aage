package com.bwj.aage;

import com.bwj.aage.config.*;
import com.bwj.aage.entity.StatusCondition;
import com.bwj.aage.entity.UniqueMonsterTable;
import com.bwj.aage.map.InhabitedMap;
import com.bwj.aage.player.KeyboardTurnHandler;
import com.bwj.aage.player.SaveHeader;
import com.bwj.aage.player.TurnHandler;
import com.bwj.aage.screen.GameOverScreen;
import com.bwj.aage.screen.LevelUpScreen;
import net.slashie.libjcsi.CSIColor;
import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.ConsoleSystemInterface;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Player extends Entity implements Serializable {

	transient private AAGEGame game;
	/**
	 *
	 */
	private static final long serialVersionUID = -5744746904030105512L;
	public long seedToChange;
	private boolean readyForLevelUp = false;

	private transient boolean showVerboseDescriptions = AAGEConfig.getStringProperty("verbose").equalsIgnoreCase("true");

	private transient TurnHandler turnHandler = null;

	@Override
	public String toString() {
		return name;
	}

	public Player(AAGEGame g, String name, Map map, int x, int y, Race r, CharacterClass cl) {
		super(x, y, new Tile('@', CSIColor.BROWNER), map, r, cl, name);
		game = g;

		currentMap.objects.add(this);
	}

	public void setGame(AAGEGame game) {
		this.game = game;
	}
	/**
	 * Replaces the current map with a different map. If the map has been visited
	 * before, it will be restored, complete with location and visibilityMap.
	 * Otherwise, a new map will be generated.
	 * @param seed The map seed to move to
	 */
	public void requestMapChange(long seed) {
		seedToChange = seed;
	}
	public void changeMap(long seed) {
		//Store the current map
		if(currentMap != null && currentMap.getSeed() != 0) {
			currentMap.getObjects().remove(this); // We don't want to store the player with the map
			StoredMap toStore = new StoredMap(currentMap, getVisibilityMap(), x, y);
			//mapStorage.put(currentMap.getSeed(), toStore);
			MapStorage.saveMap(getName(), toStore);
		}
		WindowManager.getWindow().cls();
		StoredMap newMap = MapStorage.loadMap(getName(), seed);
		if(newMap != null) {
			currentMap = newMap.getMap();
			currentMap.getObjects().add(this);
			setVisibilityMap(newMap.getVisibility());
			x = newMap.getX();
			y = newMap.getY();
			//mapStorage.remove(seed);
		}
		else {
			System.err.println("Could not find new map in storage: " + seed);
		}
	}

	/**
	 * Selects an attribute to level up by showing a level up screen. Once the call has returned
	 * from showScreen, we can safely get the selected value from the screen object
	 * @param primary Whether a primary attribute needs to be selected
	 * @return Attribute which was selected by the player.
	 */
	@Override
	protected int selectAttributeToIncrease(boolean primary) {
		LevelUpScreen screen = new LevelUpScreen(game, primary);
		game.showScreen(screen);
		return screen.getSelectedValue();
	}

	@Override
	protected void notifyAttackedBy(Entity entity) {
		getTurnHandler().notifyAttackedBy(entity);
	}
	@Override
	public void addExperience(int toAdd) {
		super.addExperience(toAdd);
		MessageManager.addMessage("You have gained " + toAdd + " experience.");
	}

	/**
	 * Prepares a map for storage by creating a StoredMap object from it,
	 * and then passes it to MapStorage to be saved to disk.
	 * @param map The map to be stored.
	 */
	public void storeMap(Map map) {
		VisibilityMap genVisibilityMap = new VisibilityMap(map);
		//Set start point
		Point startPoint = map.getStartingPosition();
		int genx = startPoint.getX();
		int geny = startPoint.getY();
		//Initialise visibility
		super.update();
		genVisibilityMap.update(genx, geny, getVisibilityRadius());
		//Store it
		StoredMap toStore = new StoredMap(map, genVisibilityMap, genx, geny);
		MapStorage.saveMap(getName(), toStore);
	}

	@Override
	public boolean move(int deltax, int deltay) {
		//Check if we are moving off the map
		Tile tileTo = currentMap.getTile(x + deltax, y + deltay);
		if (tileTo == null) {
			//Try and change map
			if (getMap() instanceof InhabitedMap) {
				requestMapChange(((InhabitedMap) getMap()).getParentSeed());
				return true;
			}
		}
		boolean moved = super.move(deltax, deltay);
		if (moved) {
			String tileText = getMap().getTileDescriptor(this, x, y, showVerboseDescriptions);
			if (!"".equals(tileText)) {
				MessageManager.addMessage(tileText);
			}
		}
		return moved;
	}

	/**
	 * Updates the player character by waiting for a keystroke and then
	 * performing the desired action.
	 */
	public void update() {
		boolean turnTaken = false;
		while(!turnTaken) {

			turnTaken = getTurnHandler().takeTurn(this);
			if(!turnTaken) {
				//Prevent the game from redrawing unless necessary.
				game.getScreen().redraw();
				WindowManager.getWindow().refresh();
			}

		}
		super.update();
	}

//	@Override
//	public int getVisibilityRadius() {
//		return super.getVisibilityRadius() + 10;
//	}


	@Override
	public void changeHealth(int delta) {
		super.changeHealth(delta);
		if(isDead()) {
			game.changeScreen(new GameOverScreen(game));
		}
	}

	@Override
	protected void notifyLevelUp(int level) {
		MessageManager.addMessage("You have levelled up to level " + level + "! Press L to apply the increase!");
		readyForLevelUp = true;
	}

	public boolean hasLevelUp() {
		return readyForLevelUp;
	}

	public boolean hasMap(long seed) {
		return MapStorage.hasMap(getName(), seed);
	}

	public StoredMap getMap(int seed) {
		return MapStorage.loadMap(getName(), seed);
	}


	public void save() {
		try {

			FileOutputStream fileOut = new FileOutputStream("saves/" + getName() + "/player.dat");
			ZipOutputStream zip = new ZipOutputStream(fileOut);
			zip.putNextEntry(new ZipEntry(getName()));
			ObjectOutputStream out = new ObjectOutputStream(zip);

			out.writeObject(this);
			out.flush();
			//Write unique monsters
			zip.putNextEntry(new ZipEntry("Uniques.dat"));
			out = new ObjectOutputStream(zip);
			out.writeObject(UniqueMonsterTable.getTable());
			out.flush();

			//Write header
			zip.putNextEntry(new ZipEntry("header.acf"));
			ConfigDocument doc = new ConfigDocument();
			doc.addElement(new SaveHeader(this));
			doc.save(zip);

			out.close();
			zip.close();
			fileOut.close();
			System.out.println("Saved file");
		} catch (IOException e) {
			Util.reportError(e);
		}

	}

	public static Player load(String name) {
		try {
			FileInputStream fileIn = new FileInputStream("saves/" + name + "/player.dat");
			ZipInputStream zip = new ZipInputStream(fileIn);
			zip.getNextEntry();
			ObjectInputStream objIn = new ObjectInputStream(zip);

			Player p = (Player) objIn.readObject();
			p.postLoad();
			//objIn.close();

			zip.getNextEntry();
			objIn = new ObjectInputStream(zip);
			UniqueMonsterTable table = (UniqueMonsterTable) objIn.readObject();
			UniqueMonsterTable.setTable(table);
			objIn.close();


			zip.close();
			fileIn.close();

			return p;
		} catch (IOException e) {
			//Util.reportError(e);
		} catch (ClassNotFoundException e) {
			//Util.reportError(e);
		}
		return null;


	}

	public static SaveHeader loadHeader(String playerName) {
		try {
			FileInputStream fileIn = new FileInputStream("saves/" + playerName + "/player.dat");
			ZipInputStream zip = new ZipInputStream(fileIn);
			ZipEntry entry = zip.getNextEntry();
			while (entry != null && !entry.getName().equals("header.acf")) {
				entry = zip.getNextEntry();

			}
			if(entry == null) {
				return null;
			}

			ConfigDocument doc = ConfigDocument.load(zip);
			if (doc.getElements().isEmpty()) {
				System.out.println("No header information found while loading " + playerName);
				return null;
			}

			return (SaveHeader) doc.getElements().get(0);

		} catch (IOException e) {
			//Util.reportError(e);
		} catch (ConfigValidationException e) {
			e.printStackTrace();
		} catch (ConfigParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Method to perform any actions which need to be done immediately after the game is
	 * loaded. This can be used to populate fields which are not saved with the player,
	 * for whatever reason.
	 */
	private void postLoad() {
		showVerboseDescriptions = AAGEConfig.getStringProperty("verbose").equalsIgnoreCase("true");
	}

	/**
	 * For simplicities' sake, a list of the locations of the numpad numbers on the selection HUD.
	 */
	public static final Point[] locs = new Point[] {
			new Point(1, 5), new Point(4, 5), new Point(7, 5),
			new Point(1, 3), new Point(4, 3), new Point(7, 3),
			new Point(1, 1), new Point(4, 1), new Point(7, 1)
	};
	/**
	 * Searches for an object adjecent to the player which is able to interact with
	 * the specified key. Prompts the user to select the desired object (if there is more
	 * than one available) and performs the interaction.
	 * @param keyCode Code representing the interaction to be performed.
	 * @param radius Radius to search away from the player. Currently only supports 0 and 1.
	 */
	public void doInteract(int keyCode, int radius) {
		//First, search for all objects in the radius that can be interacted with. Keep hold of one per direction
		MapObject[] found = new MapObject[9];
		int foundCount = 0;
		for (MapObject obj : currentMap.getObjects()) {
			if (Math.abs(obj.getX() - x) <= radius && Math.abs(obj.getY() - y) <= radius && obj.canInteract(keyCode, this)) {
				int deltax = obj.getX() - x;
				int deltay = obj.getY() - y;
				int dirNum = DIRECTIONS[1+deltay][1+deltax]; //Sneaky way to determine direction
				found[dirNum-1] = obj;
				foundCount++;
			}

		}
		int chosenIndex = -1;
		if(foundCount == 0) {
			return; //Give up if none were found
		}
		else if(foundCount > 1) {
			//Prompt for a choice from the user
			game.getScreen().redraw();
			final ConsoleSystemInterface window = WindowManager.getWindow();
			window.print(0, 0, "Choose a direction to " + getInteractionName(keyCode) + ":");
			window.print(1, 2, " \\ | / ");
			window.print(1, 3, " - + - ");
			window.print(1, 4, " / | \\ ");
			for (int i = 0; i < 9; i++) {
				//Print direction number if an object was found in that direction.
				if(found[i] != null) {
					window.print(locs[i].getX(), locs[i].getY(), Integer.toString(i+1), CSIColor.CYAN);
				}
			}
			window.refresh();
			while (chosenIndex == -1) {
				//Get input, determine the direction.
				int code = window.inkey().code;
				chosenIndex = directionFromKeyCode(code) - 1;
			}
		}
		else {
			for (int i = 0; i < 9; i++) {
				if (found[i] != null) {
					chosenIndex = i;
					break;
				}
			}
		}
		//Interact with the specified object.
		final MapObject foundObj = found[chosenIndex];
		if (foundObj != null) {
			foundObj.interact(keyCode, this);
		}
	}

	/**
	 * Translates a keycode into the numberpad direction it represents. Allows for
	 * input from the numberpad as well as from the arrow keys.
	 * @param code CharKey code from user input
	 * @return Numberpad direction (1-9) of that code, or 0 if none was found.
	 */
	public static int directionFromKeyCode(int code) {
		switch (code) {
			case CharKey.N1:
				return 1;
			case CharKey.N2:
			case CharKey.DARROW:
				return 2;
			case CharKey.N3:
				return 3;
			case CharKey.N4:
			case CharKey.LARROW:
				return 4;
			case CharKey.N5:
				return 5;
			case CharKey.N6:
			case CharKey.RARROW:
				return 6;
			case CharKey.N7:
				return 7;
			case CharKey.N8:
			case CharKey.UARROW:
				return 8;
			case CharKey.N9:
				return 9;
		}
		return 0;
	}

	public boolean directInteract(int x, int y) {
		for (MapObject obj : currentMap.getObjects()) {
			if (obj.x == x && obj.y == y && obj.canInteract(-1, this)) {
				obj.interact(-1, this);
				return true;
			}
		}
		return false;
	}

	/** Array used to determine numpad direction from a deltax and deltay value:
	 *  dir = DIRECTIONS[1+deltay][1+deltax]
	 */
	public static final int[][] DIRECTIONS = new int[][]
			{
					{7, 8, 9},
					{4, 5, 6},
					{1, 2, 3}
			};

	/**
	 * Retrieves a name for the given interaction key
	 * @param keycode Key required
	 * @return A name for that interaction, or null if it is not an interaction key.
	 */
	public static String getInteractionName(int keycode) {
		switch (keycode) {
			case CharKey.a:
				return "attack";
			case CharKey.c :
				return "close";
			case CharKey.o:
				return "open";
			case CharKey.h:
				return "shift";

		}
		return "interact";
	}

	public TurnHandler getTurnHandler() {
		if(turnHandler == null) {
			return new KeyboardTurnHandler(game);
		}
		return turnHandler;
	}

	public void setTurnHandler(TurnHandler turnHandler) {
		this.turnHandler = turnHandler;
	}

	@Override
	public void notifyConditionRemoved(StatusCondition status) {
		MessageManager.addMessage(status.getName() + " has worn off.");
	}

	public boolean isReadyForLevelUp() {
		return readyForLevelUp;
	}

	public void setReadyForLevelUp(boolean readyForLevelUp) {
		this.readyForLevelUp = readyForLevelUp;
	}
}
