package com.bwj.aage.player;

import com.bwj.aage.*;

/**
 * @author Tim Sergeant
 */
public class AutowalkTurnHandler extends TurnHandler implements Runnable {

	private Direction direction = null;
	private Thread walk;

	public AutowalkTurnHandler(AAGEGame game, int dir) {
		super(game);
		walk = new Thread(this, "Autowalk handler");
		walk.start();

		switch (dir) {
			case 1: direction = Direction.SOUTHWEST; break;
			case 2: direction = Direction.SOUTH; break;
			case 3: direction = Direction.SOUTHEAST; break;
			case 4: direction = Direction.WEST; break;
			case 6: direction = Direction.EAST; break;
			case 7: direction = Direction.NORTHWEST; break;
			case 8: direction = Direction.NORTH; break;
			case 9: direction = Direction.NORTHEAST; break;

		}
	}
	@Override
	public boolean takeTurn(Player player) {
		if (direction != null) {
			boolean moved = player.move(direction);
			if(!moved) {
				endWalk();
			}
		}

		try {
			Thread.sleep(110);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public void notifyAttackedBy(Entity entity) {
		endWalk();
	}

	@Override
	public void run() {
		WindowManager.getWindow().inkey();
		endWalk();
	}

	private void endWalk() {
		getPlayer().setTurnHandler(new KeyboardTurnHandler(getGame()));
		walk.interrupt();
	}
}
