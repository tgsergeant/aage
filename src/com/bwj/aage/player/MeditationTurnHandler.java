package com.bwj.aage.player;

import com.bwj.aage.*;
import com.bwj.aage.entity.StatusCondition;

/**
 *
 */
public class MeditationTurnHandler extends TurnHandler implements Runnable {
	
	private int duration = 0;
	private boolean meditationEnded = false;
	private Thread thread;

	public MeditationTurnHandler(AAGEGame game) {
		super(game);

		thread = new Thread(this, "Meditation handler");
		thread.start();
		MessageManager.addMessage("You are meditating. Press any key to snap back to reality.", 5);
	}
	@Override
	public boolean takeTurn(Player player) {

		player.changeHealth(1);
		if (player.getHealthValue() == player.getHealth().getDefaultValue()) {
			MessageManager.addMessage("You snap out of your trance.");
			endMeditation();
		}
		duration++;
		try {
			Thread.sleep(400);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public void notifyAttackedBy(Entity entity) {
		endMeditation();
	}

	@Override
	public void run() {
		WindowManager.getWindow().inkey();
		endMeditation();
	}

	private void endMeditation() {
		if(meditationEnded) {
			return; // Prevent occasional instances where debuffs would be added twice.
		}
		meditationEnded = true;
		if (duration > 0) {
			MessageManager.addMessage("You feel a great sense of peace with the world.");
			addMeditationDebuff(Attribute.AGILITY);
			addMeditationDebuff(Attribute.STRENGTH);
			addMeditationDebuff(Attribute.PERCEPTION);
		}
		getPlayer().setTurnHandler(new KeyboardTurnHandler(getGame()));
		thread.interrupt();
	}

	private void addMeditationDebuff(int attribute) {
		getPlayer().addStatusCondition(new StatusCondition(attribute,
				-getPlayer().getAttributeSet().getAttribute(attribute).getDefaultValue() / 5,
				3 * duration, "Meditation " + AttributeSet.ATTRIBUTENAMES[attribute].toLowerCase() + " debuff"));
	}
}
