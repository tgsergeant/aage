package com.bwj.aage.player;

import com.bwj.aage.*;
import com.bwj.aage.screen.LookScreen;
import com.bwj.aage.screen.StatisticsScreen;
import com.bwj.aage.screen.TextScreen;
import net.slashie.libjcsi.CharKey;

/**
 * @author Tim Sergeant
 */
public class KeyboardTurnHandler extends TurnHandler {


	public KeyboardTurnHandler(AAGEGame game) {
		super(game);
	}

	@Override
	public boolean takeTurn(Player player) {
		int key;
		boolean turnTaken = false;
		while (true) {
			key = WindowManager.getWindow().inkey().code;
			if (key != CharKey.NONE) {
				break;
			}
		}

		//Retrieves the direction from the key
		Point deltaPoint = Util.getDeltaFromKey(key);
		int deltax = deltaPoint.getX();
		int deltay = deltaPoint.getY();

		turnTaken = player.move(deltax, deltay);
		if (!turnTaken) {
			turnTaken = player.directInteract(player.getX() + deltax, player.getY() + deltay);
		}
		switch (key) {
			case CharKey.MORETHAN:
			case CharKey.LESSTHAN:
				player.doInteract(key, 0);
				turnTaken = true;
				break;
			case CharKey.q:
			case CharKey.Q:
				player.save();
				System.exit(0);
				break;
			case CharKey.L:
				while (player.getExperience() >= ExperienceTable.getExperience((player.getLevel() + 1))) {
					player.levelUp();
				}
				player.setReadyForLevelUp(false);
				break;
			case CharKey.d:
				player.changeHealth(-10);
				turnTaken = true;
				break;
//            case CharKey.x:
//                addExperience(10);
//                break;
			case CharKey.QUESTION:
				TextScreen help = new TextScreen(getGame(), "help", "AAGE Help");
				getGame().showScreen(help);
				break;
			case CharKey.s:
				getGame().showScreen(new StatisticsScreen(getGame()));
				break;
			case CharKey.l:
				getGame().showScreen(new LookScreen(getGame()));
				break;
			case CharKey.o:
			case CharKey.c:
			case CharKey.a:
			case CharKey.h:
				player.doInteract(key, 1);
				turnTaken = true;
				break;
			case CharKey.m:
				player.setTurnHandler(new MeditationTurnHandler(getGame()));
				break;
			case CharKey.w:
				WindowManager.getWindow().print(0, 0, "Choose direction to walk in: [1-9]");
				WindowManager.getWindow().refresh();
				int code = WindowManager.getWindow().inkey().code;
				int direction = Player.directionFromKeyCode(code);
				if(direction != 0) {
					player.setTurnHandler(new AutowalkTurnHandler(getGame(), direction));
				}
				break;
			case CharKey.SPACE:
				turnTaken = true;
				break;
		}

		return turnTaken;
	}

	@Override
	public void notifyAttackedBy(Entity entity) {
	}
}
