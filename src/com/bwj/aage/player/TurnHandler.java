package com.bwj.aage.player;

import com.bwj.aage.AAGEGame;
import com.bwj.aage.Entity;
import com.bwj.aage.Player;

/**
 * A TurnHandler allows the system to take player moves automatically, or with a reduced set of options depending
 * on the players situation.
 */
public abstract class TurnHandler {

	private AAGEGame game;


	public TurnHandler(AAGEGame game) {
		this.game = game;
	}
	
	public abstract boolean takeTurn(Player player);

	public abstract void notifyAttackedBy(Entity entity);

	public AAGEGame getGame() {
		return game;
	}

	public Player getPlayer() {
		return game.getPlayer();
	}
}
