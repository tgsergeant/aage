package com.bwj.aage.player;

import com.bwj.aage.Player;
import com.bwj.aage.config.ConfigElement;
import com.bwj.aage.config.ConfigValidationException;
import com.bwj.aage.config.types.IntType;
import com.bwj.aage.config.types.StringType;

/**
 * @author Tim Sergeant
 */
public class SaveHeader extends ConfigElement {

	public SaveHeader(String id) {
		super(id);
	}
	public SaveHeader(Player player) {
		super(player.getName());
		addAtribute("level", new IntType(player.getLevel()));
		addAtribute("class", new StringType(player.getCharacterClass().getName()));
		addAtribute("race", new StringType(player.getRace().getName()));
		addAtribute("location", new StringType(player.getMap().getName()));
		addAtribute("name", new StringType(player.getName()));
	}

	@Override
	public boolean validate() throws ConfigValidationException {
		return true;
	}

	@Override
	public String getElementType() {
		return "Savegame";
	}
}
