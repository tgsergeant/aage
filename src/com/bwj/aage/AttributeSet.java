package com.bwj.aage;

import java.io.Serializable;

/**
 * A set of attributes describing a character, class or race
 */
public class AttributeSet implements Serializable {

    /**
     * Names of each of the attributes
     */
    public static final String[] ATTRIBUTENAMES = {
            "Agility", "Charisma", "Endurance", "Intelligence", "Perception", "Strength"
    };
    private Attribute[] attributes = new Attribute[6];

    /**
     * Creates a new AttributeSet, with all values set to the given default
     * @param start Default attribute value
     */
    public AttributeSet(int start) {
        for(int i = 0; i < 6; i++) {
            attributes[i] = new Attribute(start, ATTRIBUTENAMES[i]);
        }
    }

    public AttributeSet(int[] start) {
        for (int i = 0; i < 6; i++) {
            attributes[i] = new Attribute(start[i], ATTRIBUTENAMES[i]);
        }
    }

    public Attribute getAttribute(int id) {
        return attributes[id];
    }

    /**
     *
     * @param id Attribute to find
     * @return Value of the requested attribute
     */
    public int getAttributeValue(int id) {
        return attributes[id].getValue();
    }


    /**
     * Adds all of the other attributes to this AttributeSet
     * @param other AttributeSet to merge
     */
    public void mergeWith(AttributeSet other) {
        for(int i = 0; i < 6; i++) {
            attributes[i].modifyDefault(other.getAttributeValue(i));
            
        }
    }
    
    public String toString() {
        String built = "";
        for(int i = 0; i < 6; i++) {
            built += getAttributeValue(i) + ", ";
        }
        return built;
    }

    /**
     * Translates the given attribute name into the attribute index
     * @param name Name to search
     * @return Index of the attribute, or -1 if not found.
     */
    public static int getAttributeIndex(String name) {
        for(int i = 0; i < 6; i++) {
            if (ATTRIBUTENAMES[i].equalsIgnoreCase(name)) {
                return i;
            }
        }
        return -1;
    }
}