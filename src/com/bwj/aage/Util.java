package com.bwj.aage;

import net.slashie.libjcsi.CharKey;

import java.util.List;
import java.util.Random;

/**
 *
 */
public class Util extends net.slashie.util.Util {

	public static int randomInRange(int min, int max, Random r) {
		return r.nextInt(max - min) + min;
	}


	public static String addWhitespace(String original, int totalLength) {
		StringBuilder whitespace = new StringBuilder();
		for (int i = original.length(); i < totalLength; i++) {
			whitespace.append(" ");
		}
		return original + whitespace;
	}

	public static double log2(double in) {
		return Math.log(in) / Math.log(2);
	}


	private static ErrorReportDialog dialog;

	/**
	 * Shows a (unique) error reporting dialog which allows the user to email
	 * and report errors.
	 * @param e The error to report
	 */
	public static void reportError(Throwable e) {
		e.printStackTrace();
		if (dialog == null || !dialog.isShowing()) {
			dialog = new ErrorReportDialog();
			dialog.setError(e);
			dialog.setModal(false);
			dialog.pack();
			dialog.setResizable(false);
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);
		}

	}

	/**
	 * Translates a key code into the direction in the x/y plane that it represents.
	 * Works for arrow keys and number pad.
	 * @param keyCode Keycode to use
	 * @return Direction for that keycode
	 */
	public static Point getDeltaFromKey(int keyCode) {
		int deltax = 0;
		int deltay = 0;

		switch(keyCode) {
			case CharKey.DARROW:
			case CharKey.N2:
				deltay = 1;
				break;
			case CharKey.UARROW:
			case CharKey.N8:
				deltay = -1;
				break;
			case CharKey.LARROW:
			case CharKey.N4:
				deltax = -1;
				break;
			case CharKey.RARROW:
			case CharKey.N6:
				deltax = 1;
				break;
			case CharKey.N7:
				deltax = -1;
				deltay = -1;
				break;
			case CharKey.N9:
				deltax = 1;
				deltay = -1;
				break;
			case CharKey.N3:
				deltax = 1;
				deltay = 1;
				break;
			case CharKey.N1:
				deltax = -1;
				deltay = 1;
				break;
		}

		return new Point(deltax, deltay);
	}

	public static String createWhitespace(int length) {
		String whitespace = "";
		for (int i = 0; i < length; i++) {
			whitespace += " ";
		}
		return whitespace;
	}

	public static String capitalize(String str) {
		return str.substring(0,1).toUpperCase() + str.substring(1);
	}

	public static <T> T selectFromList(List<T> list, Random r) {
		int index = r.nextInt(list.size());
		return list.get(index);
	}
}
