package com.bwj.aage.screen;

import com.bwj.aage.*;
import com.bwj.aage.screen.components.KeyListComponent;
import net.slashie.libjcsi.CharKey;

/**
 * A screen which allows the player to examine the visible map area to see what objects are nearby.
 * Used as a showScreen when the player presses l, the LookScreen provides a cursor which can be
 * moved to any location on the map.
 */
public class LookScreen extends MapViewScreen {

	/** Position of the cursor */
	private int cursorx;
	private int cursory;

	KeyListComponent listComponent;

	/**
	 * Creates a new LookScreen
	 * @param game The game being played
	 */
	public LookScreen(AAGEGame game) {
		super(game);
		cursorx = game.getPlayer().getX();
		cursory = game.getPlayer().getY();
		updateBox(game.getPlayer());

		listComponent = WindowManager.createKeyList();
		listComponent.addHint("Exit", new CharKey(CharKey.z));
	}

	/**
	 * Delegates drawing to MapViewScreen, and draws a cursor on top of this
	 */
	@Override
	public void redraw() {
		super.redraw();
		WindowManager.getWindow().print(cursorx - getViewX(), cursory - getViewY() + 2, "_");
		listComponent.draw();
	}

	/**
	 * Updates the screen by looking for arrow key input from the player. Moves the cursor accordingly.
	 * If z or escape are pressed, the screen is exited.
	 * @return True when the screen needs to exit
	 */
	@Override
	public boolean update() {
		final Player player = game.getPlayer();
		while (true) {
			int code = WindowManager.getWindow().inkey().code;
			if (code == CharKey.ESC || code == CharKey.z) {
				return true;
			}
			Point deltaPoint = Util.getDeltaFromKey(code);
			if (deltaPoint.getX() != 0 || deltaPoint.getY() != 0) { //Determine if this was a movement
				cursorx += deltaPoint.getX();
				cursory += deltaPoint.getY();
				if (cursorx < 0) {
					cursorx = 0;
				}
				if (cursory < 0) {
					cursory = 0;
				}
				Map map = player.getMap();
				if (cursorx >= map.getWidth()) {
					cursorx = map.getWidth() - 1;
				}
				if (cursory >= map.getHeight()) {
					cursory = map.getHeight() - 1;
				}
				break;
			}

		}
		centerViewAround(cursorx, cursory, player.getMap());
		updateBox(player);


		return false;
	}

	/**
	 * Updates the TextInformBox at the top of the screen to contain information about the current
	 * location. Includes the description for the tile, and a description of any present enemies.
	 * @param player Player to update for
	 */
	// TODO Need to include monster difficulty and wounds
	private void updateBox(Player player) {
		//Get the description to put into the inform box
		if(player.getVisibilityMap().getVisibility(cursorx, cursory) != Visibility.INVISBLE) {
			String boxText = player.getMap().getTileDescriptor(player, cursorx, cursory, true);
			getInformBox().setText(boxText);
		} else {
			getInformBox().clear(); //Clear when we move the cursor to somewhere we can't see.
		}
	}


}
