package com.bwj.aage.screen;

import com.bwj.aage.*;
import com.bwj.aage.entity.StatusCondition;
import com.bwj.aage.screen.components.KeyListComponent;
import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.ConsoleSystemInterface;

/**
 *
 */
public class StatisticsScreen extends GameScreen {

	KeyListComponent keys = WindowManager.createKeyList();
	
	public StatisticsScreen(AAGEGame game) {
		super(game);
		keys.addHint("Back", new CharKey(CharKey.z));

	}

	@Override
	public boolean update() {
		int code = WindowManager.getWindow().inkey().code;
		if(code == CharKey.z) {
			return true;
		}
		return false;
	}

	@Override
	public void redraw() {
		ConsoleSystemInterface console = WindowManager.getWindow();
		Player player = game.getPlayer();
		WindowManager.printCentered(4, "Statistics for " + player.getName());

		console.print(5, 8, "Race: " + player.getRace().getName());
		console.print(5, 9, "Class: " + player.getCharacterClass().getName());
		console.print(5, 10, "Level: " + player.getLevel());
		console.print(5, 11, "Experience: " + player.getExperience() + "/" + ExperienceTable.getExperience(player.getLevel() + 1));

		console.print(1, 13, Attribute.getAttributeTableHeader());

		for (int i = 0; i < 6; i++) {
			console.print(5, 14 + i, player.getAttributeSet().getAttribute(i).toTableString());
		}

		console.print(48, 13, "Status condition             Effect    Turns left");
		int yOffset = 0;
		for (StatusCondition condition : player.getConditionList().getConditions()) {
			console.print(48, 14 + yOffset, getStatusTableString(condition));
			yOffset ++;
		} 
		keys.draw();
	}

	private String getStatusTableString(StatusCondition condition) {
		String tableString = condition.getName();
		if (tableString.length() > 27) {
			tableString = tableString.substring(0, 24) + "...  ";
		}
		else {
			tableString = Util.addWhitespace(tableString, 29);
		}
		final int delta = condition.getDelta();
		if(delta < 0) {
			tableString += "`1";
		} else {
			tableString += "`8";
		}
		tableString += Util.addWhitespace(Integer.toString(delta), 3)
				+ "`0 " + AttributeSet.ATTRIBUTENAMES[condition.getAttribute()]
				.substring(0, 3).toUpperCase();
		tableString += "       " + condition.getTimeRemaining();

		return tableString;
	}
}
