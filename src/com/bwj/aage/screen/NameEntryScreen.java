package com.bwj.aage.screen;

import com.bwj.aage.AAGEConfig;
import com.bwj.aage.AAGEGame;
import com.bwj.aage.MapStorage;
import com.bwj.aage.WindowManager;
import com.bwj.aage.screen.components.KeyListComponent;
import com.bwj.aage.screen.components.TextEntryComponent;
import net.slashie.libjcsi.CharKey;

import java.io.File;

/**
 *
 */
public class NameEntryScreen extends GameScreen {

	private TextEntryComponent textEntry;

    private KeyListComponent keys = new KeyListComponent(WindowManager.getWindow(), AAGEConfig.getHeight() - 2, AAGEConfig.getWidth());

	public NameEntryScreen(AAGEGame game) {
        super(game);
        keys.addHint("Confirm", new CharKey(CharKey.ENTER));
        keys.addHint("Back", new CharKey(CharKey.ESC));
		textEntry = new TextEntryComponent(WindowManager.getWindow(), AAGEConfig.getWidth() / 2 - 16, 7, 32);
    }

    /**
     * Creates a new name entry screen with an existing name
     * @param game Game to use
     * @param name Current name
     */
    public NameEntryScreen(AAGEGame game, String name) {
        this(game);
        textEntry.setText(name);
    }

    @Override
    public boolean update() {
        CharKey key = WindowManager.getWindow().inkey();

		if (key.code == CharKey.ESC) {
            game.changeScreen(new MenuScreen(game));
        } else {
			boolean done = textEntry.update(key);
			if(done) {
				String currentName = textEntry.getText();
				if (currentName.length() == 0) {
					return false; // Ignore empty names
				}
				if (saveExists(currentName)) {
					MapStorage.deleteSaveGame(currentName); //Delete existing games
				}
				game.changeScreen(new RaceSelectionScreen(game, currentName));

			}
		}

        return false;
    }

    @Override
    public void redraw() {
        String toPrint = "Enter a name for your character:";

        WindowManager.getWindow().cls();
        WindowManager.printCentered(5, toPrint);
		textEntry.draw();

		String currentName = textEntry.getText();

        if (currentName.length() > 0 && saveExists(currentName)) {
            toPrint = "A save game already exists with that name. Pressing enter will delete it";
            WindowManager.printCentered(9, toPrint);
        }
        keys.draw();
    }

    /**
     *
     * @param name Name to check
     * @return Whether a save game with that name exists
     */
    private boolean saveExists(String name) {
        File saveFile = new File("saves/" + name + "/");
        return saveFile.exists();
    }
}
