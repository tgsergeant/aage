package com.bwj.aage.screen;


import com.bwj.aage.AAGEConfig;
import com.bwj.aage.AAGEGame;
import com.bwj.aage.Util;
import com.bwj.aage.WindowManager;
import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.textcomponents.TextBox;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fills the screen with text loaded from a file. Pressing any key will cause the screen to revert
 * to the previous screen.
 */
// TODO Support for larger files: Scrolling and the like.
public class TextScreen extends GameScreen {

    private TextBox text;

    /**
     * Creates a new text screen for displaying text from a resource file.
     * @param game Game for the screen to display on
     * @param filename File name, located in /resources/text/filename.txt
     * @param title Title to display for the screen
     */
    public TextScreen(AAGEGame game, String filename, String title) {
        super(game);
        text = new TextBox(WindowManager.getWindow());
        text.setWidth(AAGEGame.GAMEWIDTH - 4);
        text.setHeight(AAGEGame.GAMEHEIGHT - 5);
        text.setPosition(2, 2);
        text.setBorder(true);
        text.setTitle(title);
        try {
            InputStreamReader reader = new InputStreamReader(getClass().getResourceAsStream("/resources/text/" + filename + ".txt"));
            BufferedReader buff = new BufferedReader(reader);

            String readText = "";
            String newLine;
            while((newLine = buff.readLine()) != null) {
                readText += newLine;
            }
            text.setText(readText);
        } catch (FileNotFoundException e) {
            Util.reportError(e);
            text.setText("I couldn't find the file that is supposed to show up here.");
        } catch (IOException e) {
            Util.reportError(e);
        }
    }
    @Override
    public boolean update() {
        int key = WindowManager.getWindow().inkey().code;

		if (key == CharKey.z) {
			return true;
		}
        return false;
    }

    @Override
    public void redraw() {
        text.draw();
		WindowManager.getWindow().print(2, AAGEConfig.getHeight() - 2, "[z] Return");
    }
}
