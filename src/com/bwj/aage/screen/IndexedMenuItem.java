package com.bwj.aage.screen;


import net.slashie.libjcsi.textcomponents.MenuItem;

/**
 *
 */
public class IndexedMenuItem implements MenuItem {

    public IndexedMenuItem(String name, int index) {
        this.name = name;
        this.index = index;
    }

    private String name;
    
    private int index;
    @Override
    public char getMenuChar() {
        return 0;
    }

    @Override
    public int getMenuColor() {
        return 0;
    }

    @Override
    public String getMenuDescription() {
        return name;
    }
    
    public int getIndex() {
        return index;
    }
}
