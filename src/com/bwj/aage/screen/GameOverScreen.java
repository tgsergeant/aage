package com.bwj.aage.screen;

import com.bwj.aage.AAGEConfig;
import com.bwj.aage.AAGEGame;
import com.bwj.aage.MapStorage;
import com.bwj.aage.WindowManager;
import net.slashie.libjcsi.CSIColor;

public class GameOverScreen extends GameScreen {

    public GameOverScreen(AAGEGame game) {
        super(game);
        //game.getPlayer() = new Player();
    }

    @Override
    public boolean update() {
        WindowManager.getWindow().inkey();
        MapStorage.deleteSaveGame(game.getPlayer().getName());
        game.changeScreen(new MenuScreen(game));

        return false;
    }

    @Override
    public void redraw() {
        String toPrint = "You have died. Press any key to return to the menu.";
        WindowManager.getWindow().print((AAGEConfig.getWidth() - toPrint.length()) / 2, AAGEConfig.getHeight() / 2 - 5, toPrint, CSIColor.WHITE);


    }

}
