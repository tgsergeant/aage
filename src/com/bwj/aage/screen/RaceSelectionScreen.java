package com.bwj.aage.screen;

import com.bwj.aage.AAGEConfig;
import com.bwj.aage.AAGEGame;
import com.bwj.aage.WindowManager;
import com.bwj.aage.config.ConfigManager;
import com.bwj.aage.config.Race;
import com.bwj.aage.screen.components.DescriptionSelectionBox;

import java.util.ArrayList;

/**
 *
 */
public class RaceSelectionScreen extends GameScreen {

    String playerName;
    DescriptionSelectionBox box;

    public RaceSelectionScreen(AAGEGame game, String currentName) {
        super(game);
        playerName = currentName;

        //Initialise the race list
        box = new DescriptionSelectionBox(WindowManager.getWindow(), "Choose a race for your character");
        box.setPosition(5, 7);
        box.setHeight(AAGEConfig.getHeight() - 10);
        box.setWidth(AAGEConfig.getWidth() - 10);

        ArrayList<Race> races = ConfigManager.getRaceByTag("player");
        ArrayList<DescriptionSelectionBox.DescriptionMenuItem> items = new ArrayList<DescriptionSelectionBox.DescriptionMenuItem>();
        for (Race r : races) {
            items.add(box.new DescriptionMenuItem(r.getName(), r.getIdent(), r.getDescription()));

        }
        box.setMenuItems(items);
    }

    @Override
    public boolean update() {
        String raceIdent = box.getSelection();
        if(raceIdent == null) {
            game.changeScreen(new NameEntryScreen(game, playerName));
        }
        else {
            game.changeScreen(new ClassSelectionScreen(game, ConfigManager.getRace(raceIdent), playerName));
        }

        return false;
    }

    @Override
    public void redraw() {
    }
}
