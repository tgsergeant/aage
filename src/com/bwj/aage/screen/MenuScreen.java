package com.bwj.aage.screen;

import com.bwj.aage.AAGEConfig;
import com.bwj.aage.AAGEGame;
import com.bwj.aage.WindowManager;
import net.slashie.libjcsi.textcomponents.MenuBox;

import java.util.ArrayList;

public class MenuScreen extends GameScreen {
    
    MenuBox menu;
    public MenuScreen(AAGEGame game) {
        super(game);
        menu = new MenuBox(WindowManager.getWindow());
        ArrayList<GenericMenuItem> items = new ArrayList<GenericMenuItem>();
        items.add(new GenericMenuItem("New game", "new"));
        items.add(new GenericMenuItem("Load Game", "load"));
        items.add(new GenericMenuItem("About AAGE", "about"));
		items.add(new GenericMenuItem("Options", "options"));
		items.add(new GenericMenuItem("Quit", "quit"));

        menu.setMenuItems(items);
        menu.setHeight(5);
        menu.setWidth(20);
        menu.setPosition((AAGEConfig.getWidth() / 2)-7, AAGEConfig.getHeight() / 2 - 3);
    }

    @Override
    public boolean update() {
		WindowManager.printCentered(AAGEConfig.getHeight() / 2 - 5, "Welcome to AAGE. Please make a selection");
		WindowManager.printCentered(AAGEConfig.getHeight() / 2 + 3, "Programmed by Tim Sergeant, 2011-2012");

        GenericMenuItem item = (GenericMenuItem) menu.getSelection();
        if(item == null) {
            return false;
        }
        if(item.getIdent().equals("quit")) {
            return true;
        }
        else if(item.getIdent().equals("new")) {
            game.changeScreen(new NameEntryScreen(game));
        }

        else if(item.getIdent().equals("load")) {
            game.changeScreen(new LoadGameScreen(game));
        }
		else if (item.getIdent().equals("options")) {
			game.changeScreen(new OptionsScreen(game));
		} else if (item.getIdent().equals("about")) {
			game.showScreen(new TextScreen(game, "about", "About AAGE: Another Ampersand Game Engine"));
		}

        return false;
    }

    @Override
    public void redraw() {
        //menu.draw();
        //text.draw();
    }

}
