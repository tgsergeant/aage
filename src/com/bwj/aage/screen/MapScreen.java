package com.bwj.aage.screen;

import com.bwj.aage.*;
import com.bwj.aage.map.UndergroundMap;
import net.slashie.libjcsi.ConsoleSystemInterface;

/**
 * A map screen displays the player on the map with a HUD above and below the map.
 * @author Tim
 *
 */
public class MapScreen  extends MapViewScreen{

	private Map oldMap;

    public MapScreen(AAGEGame game) {
        super(game);


    }

    @Override
    public boolean update() {
        Player p = game.getPlayer();
        p.getMap().update();
        //Change map if it has been requested
        if(p.seedToChange != 0) {
            p.changeMap(p.seedToChange);
            p.seedToChange = 0;
        }
		
		if(p.getMap() != oldMap) {
			resetViewPort(p.getX(), p.getY(), p.getMap());
			oldMap = p.getMap();
		}
		centerViewAround(p.getX(), p.getY(), p.getMap());

		updateMessages(p);

        return false;
    }

	public void updateMessages(Player p) {
		getInformBox().clear();
		for (Message message : MessageManager.getMessages(p.getX(), p.getY(), 3)) {
			getInformBox().addText(message.getMessage());
		}
		MessageManager.clearMessages();
	}

	@Override
	public void redraw() {
		super.redraw();
		drawHUD(WindowManager.getWindow(), game.getPlayer());
	}

	protected void drawHUD(ConsoleSystemInterface window, Player player) {
		//Draw the HUD
		String levelProgress = player.hasLevelUp() ? "[L] Level up" : player.getLevelProgress() + "%";
		String firstLine = "L:" + player.getLevel() + ", " + levelProgress + " H: " + player.getHealth().toStringColor();
		int depth = 0;
		if(player.getMap() instanceof UndergroundMap) {
			depth = ((UndergroundMap) player.getMap()).getDepth();
		}
		String secondLine = player.getMap().getName() + " " + (depth == 0 ? "" : "D: " + depth) ;
		window.print(0, AAGEGame.GAMEHEIGHT - 2, firstLine);
		window.print(0, AAGEGame.GAMEHEIGHT - 1, secondLine);


	}
}
