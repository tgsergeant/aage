package com.bwj.aage.screen;

import com.bwj.aage.*;
import com.bwj.aage.config.CharacterClass;
import com.bwj.aage.config.Race;
import com.bwj.aage.entity.UniqueMonsterTable;
import com.bwj.aage.screen.components.IntegerEntryComponent;
import com.bwj.aage.screen.components.KeyListComponent;
import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.textcomponents.MenuBox;

import java.util.ArrayList;

/**
 *
 */
public class CharacterConfirmationScreen extends GameScreen {

    AttributeSet attributeSet = new AttributeSet(0);
    Race chosenRace;
    CharacterClass chosenClass;
    String chosenName;
    
    int pointsToSpend = 10;

	private IntegerEntryComponent seedEntry;
    
    MenuBox box;
    
    long seed = 0;

    private KeyListComponent keys = new KeyListComponent(WindowManager.getWindow(), AAGEConfig.getHeight() - 2, AAGEConfig.getWidth());
    
    public CharacterConfirmationScreen(AAGEGame game, String name, Race r, CharacterClass cl) {
        
        super(game);
        attributeSet.mergeWith(r.getAttributeSet());
        attributeSet.mergeWith(cl.getAttributeModifiers());
        
        chosenClass = cl;
        chosenRace = r;
        chosenName = name;

        box = new MenuBox(WindowManager.getWindow());
        box.setPosition(5, 14);
        box.setWidth(50);
        box.setHeight(7);
        setMenuItems();

        keys.addHint("Restart", new CharKey(CharKey.ESC));
        keys.addHint("Reset point spending", new CharKey(CharKey.R));
        keys.addHint("Add seed", new CharKey(CharKey.S));

    }

    private void setMenuItems() {
        ArrayList<IndexedMenuItem> items = new ArrayList<IndexedMenuItem>();
        for (int i = 0; i < 6; i++) {
            String attName = attributeSet.getAttribute(i).toTableString();
            IndexedMenuItem item = new IndexedMenuItem(attName, i);
            items.add(item);
        }
        box.setMenuItems(items);
    }

    @Override
    public boolean update() {
        CharKey inKey = WindowManager.getWindow().inkey();
        if (inKey.code == CharKey.ESC) { //Restart character creation
            game.changeScreen(new NameEntryScreen(game, chosenName));
        }
        else if (inKey.code == CharKey.r || inKey.code == CharKey.R) { //Reset point spending
            pointsToSpend = 10;
            attributeSet = new AttributeSet(0);
            attributeSet.mergeWith(chosenRace.getAttributeSet());
            attributeSet.mergeWith(chosenClass.getAttributeModifiers());
            keys.removeHint(CharKey.ENTER);

        }
        else if (inKey.code == CharKey.s || inKey.code == CharKey.S) {
            //Add a seed
			seedEntry = new IntegerEntryComponent(WindowManager.getWindow(), 11, 10, 20);
			if(seed != 0) {
				seedEntry.setLong(seed);
			}
			while(true) {
				WindowManager.getWindow().print(5, 10, "Seed:                ");
				seedEntry.draw();
				WindowManager.getWindow().refresh();
				CharKey key = WindowManager.getWindow().inkey();
				boolean finished = seedEntry.update(key);
				if (finished || key.code == CharKey.ESC) {
					break;
				}
			}
            try {
                seed = seedEntry.getLong();
            }
            catch (NumberFormatException e) {
                //Do nothing
            }
        } else if (pointsToSpend > 0) { //Spend a point
            IndexedMenuItem item = (IndexedMenuItem) box.getSelection(inKey);
            if (item != null) {
                attributeSet.getAttribute(item.getIndex()).modifyDefault(1);
                pointsToSpend--;
                //Determine whether to show the enter hint
                if (pointsToSpend == 0) {
                    keys.addHint("Finish character creation", new CharKey(CharKey.ENTER));
                }
            }
        } else if (pointsToSpend == 0 && inKey.code == CharKey.ENTER) {
            //Create character
            //Create game!
            if(seed == 0) {
                seed = (long) Math.floor(Math.random() * AAGEGame.SEEDRANGE);
            }
            MapStorage.createSaveFile(chosenName);
            Map overworld = Map.create(MapType.WORLD, seed, game.getPlayer());
            Point startingPoint = overworld.getStartingPosition();
            Player p = new Player(game, chosenName, overworld, startingPoint.getX(), startingPoint.getY(), chosenRace, chosenClass);
            p.setAttributeSet(attributeSet);
            p.getHealth().setDefaultValue(15 + attributeSet.getAttributeValue(Attribute.ENDURANCE) / 2);
            p.getHealth().setValue(15 + attributeSet.getAttributeValue(Attribute.ENDURANCE) / 2); // TODO This is kinda messy
            game.setPlayer(p);

			UniqueMonsterTable.clearTable();

            //Initialise map screen and add help message
            MapScreen screen = new MapScreen(game);
            MessageManager.addMessage("Press ? at any time to see the controls", 5);
            screen.updateMessages(game.getPlayer());
            game.changeScreen(screen);
        }
        setMenuItems();

        return false;
    }

    @Override
    public void redraw() {
        WindowManager.getWindow().cls();
        WindowManager.getWindow().print(5, 7, "Name: " + chosenName);
        WindowManager.getWindow().print(5, 8, "Race: " + chosenRace.getName());
        WindowManager.getWindow().print(5, 9, "Class: " + chosenClass.getName());
        String toPrint = "Press S to add a seed";
        if(seed != 0) {
            toPrint = "Seed: " + seed;
        }
        WindowManager.getWindow().print(5, 10, toPrint);

        WindowManager.getWindow().print(5, 12, "Points to spend: " + pointsToSpend);
        WindowManager.getWindow().print(5, 13, Attribute.getAttributeTableHeader());

        WindowManager.printCentered(5, "Finalise your statistics.");


        box.draw();
        keys.draw();
    }
}
