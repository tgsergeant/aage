package com.bwj.aage.screen;

import com.bwj.aage.AAGEConfig;
import com.bwj.aage.AAGEGame;
import com.bwj.aage.Util;
import com.bwj.aage.WindowManager;
import com.bwj.aage.screen.components.IntegerEntryComponent;
import com.bwj.aage.screen.components.KeyListComponent;
import com.bwj.aage.screen.components.TextEntryComponent;
import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.textcomponents.MenuBox;
import net.slashie.libjcsi.textcomponents.MenuItem;

import java.util.ArrayList;

/**
 * @author Tim Sergeant
 */
public class OptionsScreen extends GameScreen {

	KeyListComponent keys;
	MenuBox menu;

	boolean changedCritical = false;

	public OptionsScreen(AAGEGame game) {
		super(game);
		keys = WindowManager.createKeyList();
		keys.addHint("Back", CharKey.ESC);
		menu = new MenuBox(WindowManager.getWindow());
		menu.setPosition(3, 7);
		menu.setWidth(60);
		menu.setHeight(4);

		ArrayList<OptionMenuItem> items = new ArrayList<OptionMenuItem>(5);
		items.add(new OptionMenuItem(0, "xDim", "Screen Width", OptionType.INTEGER));
		items.add(new OptionMenuItem(1, "yDim", "Screen Height", OptionType.INTEGER));
		items.add(new OptionMenuItem(2, "fontSize", "Font Size", OptionType.INTEGER));
		items.add(new OptionMenuItem(3, "verbose", "Show verbose tile descriptions", OptionType.BOOLEAN));
		menu.setMenuItems(items);
	}

	@Override
	public boolean update() {
		CharKey inKey = WindowManager.getWindow().inkey();

		if (inKey.code == CharKey.ESC) {
			//Exit the screen! Save the config and change the screen size if necessary
			AAGEConfig.saveProperties();
			if (changedCritical) {
				WindowManager.disposeAndCreate();
			}
			game.changeScreen(new MenuScreen(game));
		}
		OptionMenuItem item = (OptionMenuItem) menu.getSelection(inKey);
		if (item != null) {

			//Handle toggles
			if (item.type == OptionType.BOOLEAN) {
				AAGEConfig.toggleBooleanProperty(item.optionID);
			}
			else {
				//Create entry field
				TextEntryComponent text = new TextEntryComponent(WindowManager.getWindow(), 47, 7 + item.index, 10);
				if (item.type == OptionType.INTEGER) {
					text = new IntegerEntryComponent(WindowManager.getWindow(), 47, 7 + item.index, 10);
				}
				text.setText(item.value);


				keys.addHint("Accept", CharKey.ENTER);
				redraw();

				// I'm sorry, future me, for what I'm about to do.
				while (true) {
					//Loop, redrawing the screen each time and updating the text entry. Break when finished.
					WindowManager.getWindow().print(47, 7+item.index, "          ");
					text.draw();
					WindowManager.getWindow().refresh();

					CharKey key = WindowManager.getWindow().inkey();

					boolean finished = text.update(key);
					if (finished || key.code == CharKey.ESC) {
						AAGEConfig.getProperties().setProperty(item.optionID, text.getText());
						if (item.optionID.equals("xDim") || item.optionID.equals("yDim")
								|| item.optionID.equals("fontSize")) {
							changedCritical = true;
						}
						break;
					}
				}
				keys.removeHint(CharKey.ENTER);
			}
			item.refreshValue();
		}

		return false;
	}

	@Override
	public void redraw() {
		WindowManager.getWindow().cls();
		WindowManager.getWindow().print(3, 5, "Select an option to change:");
		keys.draw();
		menu.draw();
	}

	class OptionMenuItem implements MenuItem {

		private int index;
		private final String optionID;
		private final String display;
		private final OptionType type;

		private String value;

		public OptionMenuItem(int index, String optionID, String display, OptionType type) {
			this.index = index;

			this.optionID = optionID;
			this.display = display;
			this.type = type;

			refreshValue();
		}

		private void refreshValue() {
			value = AAGEConfig.getStringProperty(optionID);
		}

		@Override
		public char getMenuChar() {
			return 0;
		}

		@Override
		public int getMenuColor() {
			return 0;
		}

		@Override
		public String getMenuDescription() {
			return Util.addWhitespace(display, 40) + value;
		}

		public int getIndex() {
			return index;
		}
	}

	public enum OptionType {
		INTEGER, STRING, BOOLEAN
	}

}
