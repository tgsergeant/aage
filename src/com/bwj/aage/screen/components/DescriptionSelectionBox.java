package com.bwj.aage.screen.components;

import com.bwj.aage.WindowManager;
import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.ConsoleSystemInterface;
import net.slashie.libjcsi.textcomponents.MenuBox;
import net.slashie.libjcsi.textcomponents.MenuItem;
import net.slashie.libjcsi.textcomponents.TextBox;
import net.slashie.libjcsi.textcomponents.TextComponent;

import java.util.List;

/**
 * A selection box which displays a description of the selected item before it
 * is confirmed by the user. Initially, a list of all the possible items is displayed
 * to the user. When an item is selected, the description for that item is displayed.
 * The user can then press enter to confirm their selection, or escape to return to
 * the menu.
 */
public class DescriptionSelectionBox extends TextComponent {

    /**
     * A GenericMenuItem with a description.
     */
    public class DescriptionMenuItem implements MenuItem {
        private String name;
        private String ident;
        private String description;

        public DescriptionMenuItem(String name, String ident, String description) {
            this.name = name;
            this.ident = ident;
            this.description = description;
        }

        @Override
        public char getMenuChar() {
            return 0;
        }

        @Override
        public int getMenuColor() {
            return 0;
        }

        @Override
        public String getMenuDescription() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getIdent() {
            return ident;
        }
    }

    /** Used to make a selection from the list */
    private MenuBox menuBox;
    /** Used to show the description and confirm the selection */
    private TextBox textBox;

    /** Currently selected item. */
    private String selectedIdent = null;
    /** Title to display above the menu */
    private String title;

    private KeyListComponent keys = WindowManager.createKeyList();

    @Override
    public void setWidth(int value) {
        super.setWidth(value);
        menuBox.setWidth(value);
        textBox.setWidth(value);
    }

    @Override
    public void setHeight(int value) {
        super.setHeight(value);
        menuBox.setHeight(value - 2);
        textBox.setHeight(value - 2);
    }

    @Override
    public void setPosition(int x, int y) {
        super.setPosition(x, y);
        menuBox.setPosition(x, y+2);
        textBox.setPosition(x, y+2);
    }

    /**
     * Creates a new TextComponent within the specified interface.
     *
     * @param si interface to be created within
     * @param title The title of the selection box
     */
    public DescriptionSelectionBox(ConsoleSystemInterface si, String title) {
        super(si);
        this.title = title;
        menuBox = new MenuBox(si);
        textBox = new TextBox(si);
        
        keys.addHint("Back", new CharKey(CharKey.ESC));
    }

    /**
     * @param items List of menu items which can be selected
     */
    public void setMenuItems(List<DescriptionMenuItem> items) {
        menuBox.setMenuItems(items);
    }

    /**
     * Gets a selection from the user. Switches back and forth through descriptions and the menu
     * until the user confirms a selection.
     * @return The identifier of the selected item, or null if none was selected
     */
    public String getSelection() {
        while (true) {
            si.cls();
            draw();
            si.refresh();
            CharKey inKey = si.inkey();
            if (selectedIdent == null) {
                if (inKey.code == CharKey.ESC) {
                    return null;
                } else {
                    DescriptionMenuItem item = (DescriptionMenuItem) menuBox.getSelection(inKey);
                    if(item != null) {
                        selectedIdent = item.getIdent();
                        textBox.setText(item.getDescription());
                        keys.addHint("Confirm", new CharKey(CharKey.ENTER));
                    }
                }
            } else {
                if (inKey.code == CharKey.ESC) {
                    selectedIdent = null;
                    keys.removeHint(CharKey.ENTER);
                } else if (inKey.code == CharKey.ENTER) {
                    return selectedIdent;
                }
            }
        }
    }

    @Override
    public void draw() {
        String toPrint = title;
        if (selectedIdent == null) {
            menuBox.draw();
        }
        else {
            textBox.draw();
            toPrint = "Confirm your selection";
        }
        
        int x = (width - toPrint.length()) / 2 + this.position.x;
        si.print(x, this.position.y, toPrint);
        keys.draw();
    }
}
