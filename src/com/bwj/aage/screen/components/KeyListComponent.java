package com.bwj.aage.screen.components;

import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.ConsoleSystemInterface;
import net.slashie.libjcsi.textcomponents.TextComponent;

import java.util.ArrayList;

/**
 *
 */
public class KeyListComponent extends TextComponent {
    
    public class KeyHint {
        private final String action;

        public String getAction() {
            return action;
        }

        public CharKey getKey() {
            return key;
        }

        private final CharKey key;

        public KeyHint(String action, CharKey key) {
            this.action = action;
            this.key = key;
        }
        
        public String toString() {
            return "[" + key.toString() + "] " + action;
        }
    }

    ArrayList<KeyHint> hints = new ArrayList<KeyHint>();

    /**
     * Creates a new TextComponent within the specified interface.
     *
     * @param si interface to be created within
     */
    public KeyListComponent(ConsoleSystemInterface si, int y, int width) {
        super(si);
        setHeight(1);
        setWidth(width - 4);
        setPosition(2, y);
    }
    
    public void addHint(String name, CharKey action) {
        hints.add(new KeyHint(name, action));
    }

	public void addHint(String name, int code) {
		addHint(name, new CharKey(code));
	}

    public void removeHint(int action) {
        for (int i = 0; i < hints.size(); i++) {
            if (hints.get(i).getKey().code == action) {
                hints.remove(i);
				System.out.println("Removed hint: " + i);
                break;
            }
        }
    }

    @Override
    public void draw() {
        String hintString = "";
        for (KeyHint hint : hints) {
            hintString += hint + " ";
        }
		if (hintString.length() > width) {
			hintString = hintString.substring(0, width);
		}
        si.print(position.x, position.y, hintString);
    }
}
