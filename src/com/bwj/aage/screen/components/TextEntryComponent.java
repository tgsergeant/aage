package com.bwj.aage.screen.components;

import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.ConsoleSystemInterface;
import net.slashie.libjcsi.textcomponents.TextComponent;

/**
 * @author Tim Sergeant
 */
public class TextEntryComponent extends TextComponent {

	private String currentText = "";

	/**
	 * Creates a new TextComponent within the specified interface.
	 *
	 * @param si interface to be created within
	 */
	public TextEntryComponent(ConsoleSystemInterface si, int x, int y, int width) {
		super(si);
		setPosition(x, y);
		setWidth(width);
		setHeight(1);
	}

	public boolean update(CharKey inkey) {
		int keyCode = inkey.code;
		if (inkey.isAlphaNumeric() && currentText.length() < width) {
			currentText += inkey.toString();
		}
		else if (keyCode == CharKey.ENTER) {
			return true;
		}
		else if(keyCode == CharKey.BACKSPACE) {
			if (currentText.length() > 0) {
				currentText = currentText.substring(0, currentText.length() - 1);
			}
		}

		return false;
	}

	@Override
	public void draw() {
		si.print(position.x, position.y, currentText);
		if (currentText.length() < width) {
			si.print(position.x + currentText.length(), position.y, "_");
		}
	}

	public String getText() {
		return currentText;
	}

	public void setText(String currentText) {
		this.currentText = currentText;
	}
}
