package com.bwj.aage.screen.components;

import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.ConsoleSystemInterface;

/**
 * @author Tim Sergeant
 */
public class IntegerEntryComponent extends TextEntryComponent {
	/**
	 * Creates a new TextComponent within the specified interface.
	 *
	 * @param si interface to be created within
	 */
	public IntegerEntryComponent(ConsoleSystemInterface si, int x, int y, int width) {
		super(si, x, y, width);
	}

	@Override
	public boolean update(CharKey inkey) {
		if((inkey.code >= CharKey.N0 && inkey.code < CharKey.N9) ||
				(inkey.code >= CharKey.T0 && inkey.code < CharKey.T9) ||
				inkey.code == CharKey.ENTER || inkey.code == CharKey.BACKSPACE) {
			return super.update(inkey);
		}
		return false;
	}

	public int getInt() {
		return Integer.parseInt(getText());
	}

	public long getLong() {
		return Long.parseLong(getText());
	}

	public void setInt(int value) {
		setText(String.valueOf(value));
	}

	public void setLong(long value) {
		setText(String.valueOf(value));
	}
}
