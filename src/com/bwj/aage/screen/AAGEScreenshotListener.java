package com.bwj.aage.screen;

import com.bwj.aage.MessageManager;
import net.slashie.libjcsi.wswing.ScreenshotListener;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Tim Sergeant
 */
public class AAGEScreenshotListener implements ScreenshotListener {

	@Override
	public void handleScreenshot(BufferedImage screenshot) {
		File dir = new File("screens/");
		if (!dir.exists()) {
			dir.mkdir();
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss") ;
		final File output = new File("screens/" + dateFormat.format(new Date()) + ".png");
		try {
			ImageIO.write(screenshot, "png", output);
			MessageManager.addMessage("Saved screenshot to " + output.getName());
		} catch (IOException e) {
			MessageManager.addMessage("Failed to save screenshot =(");
		}

	}
}
