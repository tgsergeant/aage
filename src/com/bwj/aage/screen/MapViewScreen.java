package com.bwj.aage.screen;

import com.bwj.aage.*;
import net.slashie.libjcsi.ConsoleSystemInterface;
import net.slashie.libjcsi.textcomponents.TextInformBox;

/**
 * A base screen for anything which displays the current map. Provides methods to place a certain position
 * within the bounds of the screen, and redrawing which takes into account the current visibility map of the player.
 */
public abstract class MapViewScreen extends GameScreen {

	static final int SCROLLDISTANCE = 8;
	private TextInformBox box;
	private int screenwidth;
	private int screenheight;
	private int viewx = 0;
	private int viewy = 0;

	public MapViewScreen(AAGEGame game) {
		super(game);
		screenheight = AAGEConfig.getHeight() - 4;
		box = new TextInformBox(WindowManager.getWindow());
		screenwidth = AAGEConfig.getWidth();

		final Player player = game.getPlayer();
		resetViewPort(player.getX(), player.getY(), player.getMap());

		box.setPosition(0, 0);
		box.setWidth(screenwidth - 1);
		box.setHeight(2);
	}

	@Override
	public void redraw() {
		//game.getPlayer().getMap().redraw(game.getPlayer());
		//Redraw terrain
		final ConsoleSystemInterface window = WindowManager.getWindow();
		window.cls();
		Player player = game.getPlayer();
		Map m = player.getMap();
		for(int x = 0; x < screenwidth; x++) {
			for(int y = 0; y < screenheight; y++) {
				Tile thisTile = m.getTile(x + viewx, y + viewy);
				if(thisTile == null)
					continue;
				if(thisTile.equals(Tile.BlankTile))
					continue;
				if(player.getVisibilityMap().getVisibility(x + viewx, y + viewy) == Visibility.VISIBLE) {
					window.print(x, y + 2, thisTile.getCharacter(), thisTile.getColor()); //Must be shifted down 2 to allow room for gui
				} else if(player.getVisibilityMap().getVisibility(x + viewx, y + viewy) == Visibility.VISITED) {
					window.print(x, y + 2, thisTile.getCharacter(), thisTile.getVisitedColor());
				}
			}
		}
		//Redraw objects
		for(MapObject obj : m.getObjects()) {
			if(viewx <= obj.getX() && obj.getX() < (viewx + screenwidth) && viewy <= obj.getY() && obj.getY() < viewy + screenheight) {
				if(player.getVisibilityMap().getVisibility(obj.getX(), obj.getY()) == Visibility.VISIBLE) {
					window.print(obj.getX() - viewx, obj.getY() - viewy + 2, obj.getTile().getCharacter(), (obj.getTile().getColor()));
				}
				else if(!(obj instanceof Entity) && player.getVisibilityMap().getVisibility(obj.getX(), obj.getY()) == Visibility.VISITED) {
					window.print(obj.getX() - viewx, obj.getY() - viewy + 2, obj.getTile().getCharacter(), (obj.getTile().getVisitedColor()));
				}
			}
		}
		box.draw();
	}

	/**
	 * Shifts the camera position a single unit if required, in order to keep the given coordinates within the
	 * bounds of the map
	 * @param x Coordinate to keep in the map
	 * @param y Coordinate to keep in the map
	 * @param map Map displayed on the screen
	 */
	protected void centerViewAround(int x, int y, Map map) {
		//Update viewx and viewy depending on the player's position

		if(x < viewx + SCROLLDISTANCE && y > SCROLLDISTANCE - 1) {
			viewx --;
		}
		if(y < viewy + SCROLLDISTANCE && y > SCROLLDISTANCE - 1) {
			viewy --;
		}
		if(y > viewy + screenheight - SCROLLDISTANCE && y < map.getHeight() - SCROLLDISTANCE + 1) {
			viewy ++;
		}
		if(x > viewx + screenwidth - SCROLLDISTANCE && x < map.getWidth() - SCROLLDISTANCE + 1) {
			viewx ++;
		}
	}

	/**
	 * Used to reset the view whenever map changes. On smaller maps, this ensures that the map
	 * is centered within the screen. On larger maps, it centers the view around the given position.
	 * @param x Position to center around
	 * @param y Position to center around
	 * @param map Map to display
	 */
	protected void resetViewPort(int x, int y, Map map) {
        if(map.getWidth() <= screenwidth && map.getHeight() <= screenheight) {
            viewx = -(screenwidth - map.getWidth()) / 2;
            viewy = -(screenheight - map.getHeight()) / 2;
        } else {
            viewx = x - screenwidth / 2;
            viewy = y - screenheight / 2;
            if(viewx < 0) {
                viewx = 0;
            }
            if(viewy < 0) {
                viewy = 0;
            }
            if(viewx + screenwidth > map.getWidth()) {
                viewx = map.getWidth() - screenwidth;
            }
            if(viewy + screenheight > map.getHeight()) {
                viewy = map.getHeight() - screenheight;
            }
        }
    }
	
	protected int getViewX() {
		return viewx;
	}
	
	protected int getViewY() {
		return viewy;
	}

	protected TextInformBox getInformBox() {
		return box;
	}

	public int getScreenWidth() {
		return screenwidth;
	}

	public int getScreenHeight() {
		return screenheight;
	}
}
