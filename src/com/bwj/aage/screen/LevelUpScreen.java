package com.bwj.aage.screen;

import com.bwj.aage.*;
import net.slashie.libjcsi.textcomponents.MenuBox;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class LevelUpScreen extends GameScreen{

    /** Whether or not the screen should only display primary attributes */
    private boolean primary;

    /** The index of the selected attribute. 0 until the user has made a selection */
    private int selectedValue;
    
    private MenuBox box;

    public LevelUpScreen(AAGEGame game, boolean primary) {
        super(game);
        this.primary = primary;
        box = new MenuBox(WindowManager.getWindow());
        box.setPosition((AAGEConfig.getWidth() - 40)/2, 9);
        box.setWidth(40);
        box.setHeight(10);

        List<IndexedMenuItem> items = new ArrayList<IndexedMenuItem>();
        AttributeSet atts = game.getPlayer().getAttributeSet();
        for(int i = 0; i < 6; i++) {
            if (!primary || game.getPlayer().getCharacterClass().isPrimaryAttribute(i)) {
                //Carefully position the strings with whitespace
                String attString = atts.getAttribute(i).toTableString();
                IndexedMenuItem item = new IndexedMenuItem(attString, i);
                items.add(item);
            }
            
        }
        box.setMenuItems(items);
    }

    /**
     * Prompts the user to select an attribute to increase
     * @return True once the selection has been made, to exit the window.
     */
    @Override
    public boolean update() {
        //redraw();
        while (true) {
            IndexedMenuItem item = (IndexedMenuItem) box.getSelection();
            if(item != null) {
                selectedValue = item.getIndex();
                return true;
            }
        }
    }

    @Override
    public void redraw() {
        box.draw();
        WindowManager.getWindow().print((AAGEConfig.getWidth() - 40) / 2, 8, Attribute.getAttributeTableHeader());
        WindowManager.printCentered(6, primary ? "Select a primary attribute to increase" : "Select an attribute to increase");
        WindowManager.printCentered(5, "You have levelled up! You are now level " + game.getPlayer().getLevel());
    }

    public int getSelectedValue() {
        return selectedValue;
    }
}
