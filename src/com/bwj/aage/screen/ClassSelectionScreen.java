package com.bwj.aage.screen;

import com.bwj.aage.AAGEConfig;
import com.bwj.aage.AAGEGame;
import com.bwj.aage.WindowManager;
import com.bwj.aage.config.CharacterClass;
import com.bwj.aage.config.ConfigManager;
import com.bwj.aage.config.Race;
import com.bwj.aage.screen.components.DescriptionSelectionBox;

import java.util.ArrayList;

/**
 *
 */
public class ClassSelectionScreen extends GameScreen {
    Race playerRace;
    String playerName;
    DescriptionSelectionBox box;

    // TODO Ew, copypasta

    public ClassSelectionScreen(AAGEGame game, Race r, String name) {
        super(game);
        playerName = name;
        playerRace = r;

        //Initialise the race list
        box = new DescriptionSelectionBox(WindowManager.getWindow(), "Choose a class for your character:");
        box.setPosition(5, 7);
        box.setHeight(AAGEConfig.getHeight() - 10);
        box.setWidth(AAGEConfig.getWidth() - 10);

        ArrayList<CharacterClass> classes = ConfigManager.getClassesByTag("player");
        ArrayList<DescriptionSelectionBox.DescriptionMenuItem> items = new ArrayList<DescriptionSelectionBox.DescriptionMenuItem>();
        for (CharacterClass c : classes) {
            items.add(box.new DescriptionMenuItem(c.getName(), c.getId(), c.getDescription()));
        }
        box.setMenuItems(items);
    }

    @Override
    public boolean update() {
        String classIdent = box.getSelection();
        if(classIdent == null) {
            game.changeScreen(new RaceSelectionScreen(game, playerName));
        }
        else {
            game.changeScreen(new CharacterConfirmationScreen(game, playerName, playerRace, ConfigManager.getClass(classIdent)));

        }

        return false;
    }

    @Override
    public void redraw() {
    }
}
