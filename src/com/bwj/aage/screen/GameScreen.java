package com.bwj.aage.screen;

import com.bwj.aage.AAGEGame;

/**
 * A GameScreen is a single type of display for the game. For example, a main menu, a map screen
 * or a screen displaying contents from a text file. Only one screen can be shown at any time, although
 * AAGEGame provides 2 simple methods for passing control between screens.
 */
public abstract class GameScreen {

    /**
     * The game which the screen belongs to.
     */
    protected AAGEGame game;

    /**
     * Updates the screen, receiving input from the player and responding
     * appropriately. At any time, the screen can return true to lose
     * control and return to whatever was lower down in the stack.
     * If the screen is the current base 'active screen', and it returns
     * true, the game will exit.
     * @return True when the screen wants to exit.
     */
    public abstract boolean update();

    /**
     * Prompts the screen to draw its contents to the console. Note that
     * while refreshing the screen is handled by AAGEGame, clearing it is
     * not - screens will need to do this themselves if required.
     */
    public abstract void redraw();
    
    public GameScreen(AAGEGame game) {
        this.game = game;
    }

}
