package com.bwj.aage.screen;

import com.bwj.aage.AAGEConfig;
import com.bwj.aage.AAGEGame;
import com.bwj.aage.Player;
import com.bwj.aage.WindowManager;
import com.bwj.aage.player.SaveHeader;
import net.slashie.libjcsi.CSIColor;
import net.slashie.libjcsi.textcomponents.MenuBox;
import net.slashie.libjcsi.textcomponents.TextBox;

import java.io.File;
import java.util.ArrayList;

/**
 * A screen used to present a list of currently saved games.
 * The user is able to select a save name, which loads the game from disk
 * and allows them to resume the game where they left it.
 * @author Tim
 *
 */
public class LoadGameScreen extends GameScreen {

    MenuBox menu;

    TextBox text;

    public LoadGameScreen(AAGEGame game) {
        super(game);
        menu = new MenuBox(WindowManager.getWindow());
        ArrayList<GenericMenuItem> items = new ArrayList<GenericMenuItem>();
        getSaveGames(items);
        menu.setMenuItems(items);
        menu.setHeight(10);
        menu.setWidth(AAGEConfig.getWidth() - 4);
        menu.setPosition(2, AAGEConfig.getHeight()/2 - 3);
    }


    /**
     * Traverses the save game directory to find a list of all currently saved files.
     * @param items A list of menu items which can be displayed on the menu
     */
    private void getSaveGames(ArrayList<GenericMenuItem> items) {
        File savesList = new File("saves");
        if(!savesList.exists()) {
            return;
        }
        String[] children = savesList.list();
        for(String child : children) {
			SaveHeader header = Player.loadHeader(child);
			if (header != null) {
				String name = (String) header.getAttributeValue("name").getValue();
				String level = Integer.toString((Integer) header.getAttributeValue("level").getValue());
				String race = (String) header.getAttributeValue("race").getValue();
				String chClass = (String) header.getAttributeValue("class").getValue();

				String playerText = name + ": A level " + level + " " + race + " " + chClass;
				items.add(new GenericMenuItem(playerText, name));
			}
		}
    }


    /**
     * Updates the screen, receives input from the user and then processes that input.
     * A character key loads the corresponding savegame, ESC or SPACE returns to the main menu.
     */
    @Override
    public boolean update() {
        String toPrint = "Please select the game to load. (ESC to return)";
        WindowManager.getWindow().print((AAGEConfig.getWidth() - toPrint.length()) / 2, AAGEConfig.getHeight() / 2 - 5, toPrint, CSIColor.WHITE);

        GenericMenuItem item = (GenericMenuItem) menu.getSelection();
        if(item == null) {
            game.changeScreen(new MenuScreen(game));
        }
        else {
            Player loaded = Player.load(item.getIdent());
            if (loaded != null) {
                loaded.setGame(game); // The game isn't saved to disk
                game.setPlayer(loaded);
                game.changeScreen(new MapScreen(game));
            }
        }
        return false;
    }

    @Override
    public void redraw() {
        //menu.draw();
        //text.draw();
    }

}
