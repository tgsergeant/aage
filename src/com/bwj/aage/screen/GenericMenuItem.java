package com.bwj.aage.screen;

import net.slashie.libjcsi.textcomponents.MenuItem;

public class GenericMenuItem implements MenuItem {

    private String rowData;
    private String ident;


    public GenericMenuItem(String data, String ident) {
        setRow(data);
        setIdent(ident);
    }

    /**
     *
     * @return String of the item
     */
    public String getMenuDescription() {
        return rowData;
    }

    /**
     * Sets the String to be contained.
     * @param data String to be contained
     */
    public void setRow(String data) {
        rowData = data;
    }

    /**
     *
     * @return char used as index
     */
    public char getMenuChar() {
        return ' ';
    }

    /**
     *
     * @return int representation of color
     */
    public int getMenuColor() {
        return 0;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }
}
