# AAGE: Another Ampersand Game Engine.

## About AAGE

AAGE is about my third attempt to write a roguelike, and is currently my most successful. Current features include 
fully random world generation, a range of races and classes, rudimentary combat and a easy-to-write configuration
format.

## Dependencies

AAGE uses a slightly modified version of libjsci, known as libjsci-aage. This library is released under the LGPL license.
Full source code and further details can be found in libjsci-aage.jar.
The original version of the libjsci can be found at http://slashie.net/libjcsi/.

## Version History

**0.3 (Current Release)**

* Dungeon generation - Rooms and corridors in a more traditional roguelike style
* Added villages, towns and kingdoms
    - Villages are a collection of houses, towns and cities are much larger
    - Each house has a couple of NPC inhabitants who wander around aimlessly
    - Kingdoms have a capital city and a number of smaller cities
    - Randomly generated names for all of the above
* Look screen which detatches view from the player
* Meditation to regenerate health. Adds a temporary debuff due to your inner peace
* Autowalk
* Screenshot function
* Options screen
* New config format which makes content creation even easier
* A handful of new enemies, as well as unique monsters which will only appear once in any given savegame


**0.2**

* New world generation based on Perlin noise
* Far better use of attributes (currently charisma and intelligence are unused)
* New XML based system for race, class and monster loading
* Added character creation:
    - 6 player races:
        + Human
        + Elf
        + Dwarf
        + Troll
        + Halfling
        + Orc
    - 3 character classes:
        + Rogue
        + Fighter
        + Mage
* Added 11 new monsters
    - Hobgoblin
    - Goblin Skirmisher
    - Goblin Warchief
    - Orc Scout
    - Orc Fighter
    - Orc Skirmisher
    - Orc Commander
    - Rat
    - Giant rat
    - Wolf
    - Dire wolf
* Improved levelling up to allow extra attribute points to be given
* Improved monster AI to gather around enemies
* Added ability to set a seed for the world from Character creation menu
* Maps are now saved/loaded as they are entered, reducing memory usage.
* Added statistics screen
* Added ability to report errors from within the game

**0.1 (Initial release)**

* Overworld generation
* Multi-level dungeon generation.
* Saving and loading.
* Field of Vision for the player and NPCs.
* A rudimentary combat system.
* A range of playable races and classes
* Numerous enemy types
* Resizeable game window (by modifying config.xml).

## Planned Features

The current release is quite incomplete. Several features are planned for future versions of AAGE:

**0.4**

Performance improvements and code cleanup! In particular,

* Better integration of colour into the HUD
* Faster updates when there are many entities on screen at once (capital cities, I'm looking at you)
* Better villager AI

**Future releases**

* Special caves and dungeons - for example, a goblin cave or rat-infested dungeon
* Items
* Shops
* More detailed combat
* Player skills
* Online highscores/graveyard